/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.element;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;

/**
 * The Definition Of AssociationKey
 *
 * @ClassName: AssociationKey
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationKey implements Cloneable {
    private String sourceElement;
    private String sourceElementDisplay;
    private String targetElement;
    private String targetElementDisplay;
    private String refdataModelName;

    /**
     * 创建关联外键
     */
    public GspAssociationKey() {

    }

    /**
     * 源元素ID，子对象关联字段的元素ID。
     */
    @JsonProperty(CefNames.SOURCE_ELEMENT)
    public final String getSourceElement() {
        return sourceElement;
    }

    public final void setSourceElement(String value) {
        sourceElement = value;
    }

    /**
     * 来源字段显示名称
     */
    @JsonProperty(CefNames.SOURCE_ELEMENT_DISPLAY)
    public final String getSourceElementDisplay() {
        return sourceElementDisplay;
    }

    public final void setSourceElementDisplay(String value) {
        sourceElementDisplay = value;
    }

    /**
     * 目标元素ID，父对象关联字段的元素ID。
     */
    @JsonProperty(CefNames.TARGET_ELEMENT)
    public final String getTargetElement() {
        return targetElement;
    }

    public final void setTargetElement(String value) {
        targetElement = value;
    }

    /**
     * 目的字段显示名称
     */
    @JsonProperty(CefNames.TARGET_ELEMENT_DISPLAY)
    public final String getTargetElementDisplay() {
        return targetElementDisplay;
    }

    public final void setTargetElementDisplay(String value) {
        targetElementDisplay = value;
    }

    /**
     * RefDataModelName,实现需要，要来在添加引用列时取得对应的DataModel
     */
    @JsonProperty(CefNames.REF_DATA_MODEL_NAME)
    public final String getRefDataModelName() {
        return refdataModelName;
    }

    public final void setRefDataModelName(String value) {
        refdataModelName = value;
    }

    /// region ICloneable Members

    /**
     * 克隆
     *
     * @return
     * @throws CloneNotSupportedException
     */
    public final GspAssociationKey clone() {
        Object tempVar;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
        GspAssociationKey newAssKey = (GspAssociationKey)((tempVar instanceof GspAssociationKey) ? tempVar : null);
        newAssKey.sourceElement = this.sourceElement;
        newAssKey.sourceElementDisplay = this.sourceElementDisplay;
        newAssKey.targetElement = this.targetElement;
        newAssKey.targetElementDisplay = this.targetElementDisplay;
        newAssKey.refdataModelName = this.refdataModelName;
        return newAssKey;
    }

    /// endregion
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.element;

/**
 * The Definition Of ForeignKeyConstraintType
 *
 * @ClassName: ForeignKeyConstraintType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ForeignKeyConstraintType {
    /**
     * 禁用
     */
    Forbid(0),
    /**
     * 启用
     */
    Permit(1);

    private static java.util.HashMap<Integer, ForeignKeyConstraintType> mappings;
    private int intValue;

    private ForeignKeyConstraintType(int value) {
        intValue = value;
        ForeignKeyConstraintType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, ForeignKeyConstraintType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ForeignKeyConstraintType>();
        }
        return mappings;
    }

    public static ForeignKeyConstraintType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
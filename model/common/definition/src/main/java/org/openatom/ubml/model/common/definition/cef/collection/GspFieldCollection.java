/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.collection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import org.openatom.ubml.model.common.definition.cef.IFieldCollection;
import org.openatom.ubml.model.common.definition.cef.IGspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.util.DataValidator;

/**
 * The Collection Of FieldDefinition
 *
 * @ClassName: GspFieldCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspFieldCollection extends BaseList<IGspCommonField> implements IFieldCollection {
    private static final long serialVersionUID = 1L;
    @JsonIgnore
    private HashMap<String, IGspCommonField> gspCommonFieldMap;
    // @JsonIgnore
    private IGspCommonDataType parentObject;

    public GspFieldCollection() {
        gspCommonFieldMap = new HashMap<>();
    }

    /**
     * 创建字段集合
     *
     * @param parentObj
     */
    public GspFieldCollection(IGspCommonDataType parentObj) {
        parentObject = parentObj;
        gspCommonFieldMap = new HashMap<>();
    }

    /**
     * 移动字段
     *
     * @param toIndex 目的索引为止
     * @param element 需要移动的字段
     */
    public final void move(int toIndex, IGspCommonField element) {
        if (contains(element)) {
            super.remove(element);
            super.add(toIndex, element);
        }
    }

    public final void remove(String id) {
        IGspCommonField field = getByID(id);
        if (field != null)
            super.remove(field);
    }

    /**
     * 克隆
     *
     * @param absObject
     * @param parentAssociation
     * @return
     */
    public GspFieldCollection clone(IGspCommonDataType absObject, GspAssociation parentAssociation) {
        GspFieldCollection newCollection = new GspFieldCollection(absObject);
        for (IGspCommonField item : this) {
            newCollection.add(item.clone(absObject, parentAssociation));
        }
        return newCollection;
    }

    ///// <summary>
    ///// Index[string]
    ///// </summary>
    public IGspCommonField getItem(String id) {
        for (IGspCommonField item : this) {
            if (id.equals(item.getID())) {
                return item;
            }
        }
        return null;
    }

    protected final IGspCommonField getByID(String id) {
        for (IGspCommonField item : this) {
            if (id.equals(item.getID())) {
                return item;
            }
        }
        return null;
    }

    protected final IGspCommonField getByIndex(int index) {
        return super.get(index);
    }

    /**
     * 所属结点
     */
    public IGspCommonDataType getParentObject() {
        return parentObject;
    }

    public final void setParentObject(IGspCommonDataType value) {
        if (parentObject != value) {
            parentObject = value;
            for (IGspCommonField item : this) {
                item.setBelongObject(value);
            }
        }
    }

    public IGspCommonField getItem(int index) {
        return super.get(index);
    }

    // private void setItem(int index, IGspCommonField value) {
    // super.set(index, value);
    // }

    /**
     * 添加一个新字段
     *
     * @param element
     */
    @Override
    public boolean add(IGspCommonField element) {
        DataValidator.checkForNullReference(element, "element");

        boolean result = super.add(element);
        if (getParentObject() != null) {
            element.setBelongObject(getParentObject());
        }
        gspCommonFieldMap.put(element.getLabelID().toLowerCase(), element);
        return result;
    }

    @Override
    public void add(int index, IGspCommonField element) {
        super.add(index, element);
        gspCommonFieldMap.put(element.getLabelID().toLowerCase(), element);
    }

    @Override
    public boolean addAll(Collection<? extends IGspCommonField> c) {
        c.forEach(element -> {
            gspCommonFieldMap.put(element.getLabelID().toLowerCase(), element);
        });
        return super.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends IGspCommonField> c) {
        c.forEach(element -> {
            gspCommonFieldMap.put(element.getLabelID().toLowerCase(), element);
        });
        return super.addAll(index, c);
    }

    @Override
    public boolean remove(Object element) {
        if (element instanceof IGspCommonField) {
            gspCommonFieldMap.remove(((IGspCommonField)element).getLabelID().toLowerCase());
        }
        return super.remove(element);
    }

    @Override
    public IGspCommonField remove(int index) {
        IGspCommonField result = super.remove(index);
        gspCommonFieldMap.remove(result.getLabelID().toLowerCase());
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> elements) {
        Objects.requireNonNull(elements);
        elements.forEach(element -> {
            if (element instanceof IGspCommonField) {
                gspCommonFieldMap.remove(((IGspCommonField)element).getLabelID().toLowerCase());
            }
        });
        return super.removeAll(elements);
    }

    @Override
    public boolean removeIf(Predicate<? super IGspCommonField> filter) {
        boolean result = super.removeIf(filter);
        final Iterator<IGspCommonField> each = this.iterator();
        while (each.hasNext()) {
            gspCommonFieldMap.remove(((IGspCommonField)each).getLabelID().toLowerCase());
        }
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> elements) {
        boolean result = super.retainAll(elements);
        Objects.requireNonNull(elements);
        elements.forEach(element -> {
            if (element instanceof IGspCommonField) {
                gspCommonFieldMap.remove(((IGspCommonField)element).getLabelID().toLowerCase());
            }
        });
        return result;
    }

    @Override
    public void replaceAll(UnaryOperator<IGspCommonField> operator) {
        super.replaceAll(operator);
        gspCommonFieldMap.clear();
        for (IGspCommonField element : this) {
            gspCommonFieldMap.put(element.getLabelID().toLowerCase(), element);
        }
    }

    @Override
    public IGspCommonField set(int index, IGspCommonField element) {
        IGspCommonField result = super.set(index, element);
        gspCommonFieldMap.put(element.getLabelID().toLowerCase(), element);
        return result;
    }

    @Override
    public void clear() {
        super.clear();
        gspCommonFieldMap.clear();
    }

    @Override
    public IGspCommonField getByLabelId(String labelId) {
        return gspCommonFieldMap.get(labelId.toLowerCase());
    }
}

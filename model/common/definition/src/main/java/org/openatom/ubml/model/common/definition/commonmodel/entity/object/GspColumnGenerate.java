/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.entity.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;

/**
 * The Definition Of Column Generate
 *
 * @ClassName: GspColumnGenerate
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspColumnGenerate implements Cloneable {
    private String elementID = "";
    private String generateType = "";

    /**
     * 创建一个规则
     */
    public GspColumnGenerate() {
    }

    /**
     * 元素ID，此属性应该是ElementID，而不是ColumnID
     */
    @JsonProperty(CommonModelNames.ELEMENT_ID)
    public final String getElementID() {
        return this.elementID;
    }

    public final void setElementID(String value) {
        this.elementID = value;
    }

    /**
     * 生成类型
     */
    @JsonProperty(CommonModelNames.GENERATE_TYPE)
    public final String getGenerateType() {
        return this.generateType;
    }

    public final void setGenerateType(String value) {
        this.generateType = value;
    }

    // region ICloneable Members

    /**
     * 克隆
     *
     * @return
     */
    public final GspColumnGenerate clone() {
        try {
            Object tempVar = super.clone();
            GspColumnGenerate newObj = (GspColumnGenerate)((tempVar instanceof GspColumnGenerate) ? tempVar : null);
            return newObj;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    // endregion
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappingRelation implements Iterable<MappingInfo>, Cloneable {
    /**
     * Key : ChildElement ID
     * Value: UDT字段ID
     */
    private java.util.HashMap<String, String> dic = new java.util.HashMap<String, String>();

    public final void add(String key, String value) {
        dic.put(key, value);
    }

    public final void remove(String key) {
        dic.remove(key);
    }

    public final java.util.ArrayList<String> getKeys() {
        return new ArrayList<String>(dic.keySet());
    }

    public final java.util.ArrayList<String> getValues() {
        return new ArrayList<String>(dic.values());
    }

    // public final java.util.Iterable<MappingInfo> GetEnumerator()
    // {
    // //C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing
    // in Java:
    // for (var item : dic)
    // {
    // MappingInfo tempVar = new MappingInfo();
    // tempVar.setKeyInfo(item.getKey());
    // tempVar.setValueInfo(item.getValue());
    // //C# TO JAVA CONVERTER TODO TASK: Java does not have an equivalent to the C#
    // 'yield' keyword:
    // yield return tempVar;
    // }
    // }

    public final void clear() {
        dic.clear();
    }

    /**
     * 根据键查找值
     *
     * @param key
     * @return
     */
    public final String getMappingInfo(String key) {
        return dic.get(key);
    }

    /**
     * 根据值查找键
     *
     * @param value
     * @return
     */
    public final String getKeyInfoByValueInfo(String value) {
        if (!getValues().contains(value)) {
            return null;
        }
        Set<Entry<String, String>> list = dic.entrySet();
        for (Entry<String, String> item : list) {
            if (item.getValue().equals(value)) {
                return (String)item.getKey();
            }
        }
        return null;
    }

    public final int getCount() {
        return dic.size();
    }

    public final void add(MappingInfo info) {
        dic.put(info.getKeyInfo(), info.getValueInfo());
    }

    public final boolean containsKey(String key) {
        return dic.containsKey(key);
    }

    // public final String getItem(String key) {
    // }

    // public final void setItem(String key, String value) {
    // }

    public final Object clone() {
        MappingRelation mappingRelation = new MappingRelation();
        if (dic.size() > 0) {
            Set<Entry<String, String>> list = dic.entrySet();
            for (Entry<String, String> pair : list) {
                mappingRelation.add(pair.getKey(), pair.getValue());
            }
        }
        return mappingRelation;
    }

    @Override
    public Iterator<MappingInfo> iterator() {
        ArrayList<MappingInfo> list = new ArrayList<>();
        for (Entry<String, String> item : dic.entrySet()) {
            MappingInfo info = new MappingInfo();
            info.setKeyInfo(item.getKey());
            info.setValueInfo(item.getValue());
            list.add(info);
        }
        return list.iterator();
    }
//
//	@Override
//	public Iterable<MappingInfo> iterator() {
//		return null;
//	}

    @Override
    public boolean equals(Object obj) {
        MappingRelation relation = (MappingRelation)obj;
        boolean equals = super.equals(obj);
        if (equals)
            return equals;
        if ((dic == null || dic.size() < 1) && (relation.dic == null || relation.dic.size() < 1))
            return true;
        return false;
    }
}
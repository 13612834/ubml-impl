/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.object;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.common.definition.cef.IGspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.collection.CommonDtmCollection;
import org.openatom.ubml.model.common.definition.cef.collection.CommonValCollection;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.json.element.CefFieldSerializer;
import org.openatom.ubml.model.common.definition.cef.json.operation.CommonDtmSerializer;
import org.openatom.ubml.model.common.definition.cef.json.operation.CommonValSerializer;
import org.openatom.ubml.model.common.definition.cef.operation.CommonDetermination;
import org.openatom.ubml.model.common.definition.cef.operation.CommonValidation;

/**
 * The Json Serializer Of CommonDataType
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspCommonDataTypeSerializer extends JsonSerializer<IGspCommonDataType> {

    private CommonDtmSerializer dtmSerializer;
    private CommonValSerializer valSerializer;

    public GspCommonDataTypeSerializer() {
        dtmSerializer = new CommonDtmSerializer();
        valSerializer = new CommonValSerializer();
    }

    @Override
    public void serialize(IGspCommonDataType dataType, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        SerializerUtils.writeStartObject(jsonGenerator);
        writeBaseObjInfo(dataType, jsonGenerator);
        writeSelfObjInfo(dataType, jsonGenerator);
        SerializerUtils.writeEndObject(jsonGenerator);
    }


    //region BaseProp
    private void writeBaseObjInfo(IGspCommonDataType dataType, JsonGenerator jsonGenerator) {
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.ID, dataType.getID());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.CODE, dataType.getCode());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.NAME, dataType.getName());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.IS_REF, dataType.getIsRef());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.I18N_RESOURCE_INFO_PREFIX, dataType.getI18nResourceInfoPrefix());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.CUSTOMIZATION_INFO, dataType.getCustomizationInfo());
        SerializerUtils.writePropertyName(jsonGenerator, CefNames.CONTAIN_ELEMENTS);
        SerializerUtils.writeArray(jsonGenerator, getFieldSerializer(), dataType.getContainElements());
        //标签
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.BE_LABEL, dataType.getBeLabel());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.BIZ_TAG_IDS, dataType.getBizTagIds());

        writeExtendCdtBaseProperty(dataType, jsonGenerator);

    }

    //endregion

    //region SelfProp
    private void writeSelfObjInfo(IGspCommonDataType dataType, JsonGenerator writer) {
        //不需要了
        writeDtmAfterModify(dataType, writer);
//        writeDtmBeforeSave(dataType,writer);
//        writeDtmAfterCreate(dataType,writer);
//        writeValAfterModify(dataType,writer);
//        writeValB4Save(dataType,writer);
        writeExtendCdtSelfProperty(dataType, writer);

    }

    private void writeDtm(String propName, CommonDtmCollection collection, JsonGenerator writer) {
        if (collection == null || collection.size() < 1)
            return;
        SerializerUtils.writePropertyName(writer, propName);
        SerializerUtils.WriteStartArray(writer);
        for (CommonDetermination commonDetermination : collection)
            dtmSerializer.serialize(commonDetermination, writer, null);
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeDtmAfterModify(IGspCommonDataType dataType, JsonGenerator writer) {
        writeDtm(CefNames.DTM_AFTER_MODIFY, dataType.getDtmAfterModify(), writer);
    }

    private void writeDtmBeforeSave(IGspCommonDataType dataType, JsonGenerator writer) {
        writeDtm(CefNames.DTM_BEFORE_SAVE, dataType.getDtmBeforeSave(), writer);
    }

    private void writeDtmAfterCreate(IGspCommonDataType dataType, JsonGenerator writer) {
        writeDtm(CefNames.DTM_AFTER_CREATE, dataType.getDtmAfterCreate(), writer);
    }

    private void writeValAfterModify(IGspCommonDataType dataType, JsonGenerator writer) {
        writeVal(CefNames.VAL_AFTER_MODIFY, dataType.getValAfterModify(), writer);
    }

    private void writeValB4Save(IGspCommonDataType dataType, JsonGenerator writer) {
        writeVal(CefNames.VAL_BEFORE_SAVE, dataType.getValBeforeSave(), writer);
    }


    private void writeVal(String propName, CommonValCollection collection, JsonGenerator writer) {
        if (collection == null || collection.size() < 1)
            return;
        SerializerUtils.writePropertyName(writer, propName);
        SerializerUtils.WriteStartArray(writer);
        for (CommonValidation commonValidation : collection)
            valSerializer.serialize(commonValidation, writer, null);
        SerializerUtils.WriteEndArray(writer);
    }
    //endregion

    //region 抽象方法
    protected void writeExtendCdtBaseProperty(IGspCommonDataType commonDataType, JsonGenerator jsonGenerator) {

    }

    protected abstract CefFieldSerializer getFieldSerializer();

    protected abstract void writeExtendCdtSelfProperty(IGspCommonDataType info, JsonGenerator jsonGenerator);

    //endregion
}

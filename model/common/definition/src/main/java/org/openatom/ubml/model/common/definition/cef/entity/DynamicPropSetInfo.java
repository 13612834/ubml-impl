/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DynamicPropSetInfo {

    /**
     * 动态属性集合持久化构件
     */
    private MdRefInfo privateDynamicPropRepositoryComp;
    /**
     * 动态属性集合序列化构件
     */
    private MdRefInfo privateDynamicPropSerializerComp;

    @JsonProperty("DynamicPropRepositoryComp")
    public final MdRefInfo getDynamicPropRepositoryComp() {
        return privateDynamicPropRepositoryComp;
    }

    public final void setDynamicPropRepositoryComp(MdRefInfo value) {
        privateDynamicPropRepositoryComp = value;
    }

    @JsonProperty("DynamicPropSerializerComp")
    public final MdRefInfo getDynamicPropSerializerComp() {
        return privateDynamicPropSerializerComp;
    }

    public final void setDynamicPropSerializerComp(MdRefInfo value) {
        privateDynamicPropSerializerComp = value;
    }
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.collection;

import org.openatom.ubml.model.common.definition.cef.element.GspEnumValue;

/**
 * The Collection Of EnumValue
 *
 * @ClassName: GspEnumValueCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspEnumValueCollection extends BaseList<GspEnumValue> implements Cloneable {
    private static final long serialVersionUID = 1L;

    /**
     * 创建枚举值集合
     */
    public GspEnumValueCollection() {
        //
        // TODO: add constructor logic here
        //
    }

    /**
     * add Element
     *
     * @param element
     * @return
     */
    public final void addEnumValue(GspEnumValue element) {
        super.add(element);
    }

    /**
     * remove Element
     *
     * @param element
     */
    public final void remove(GspEnumValue element) {
        if (contains(element)) {
            super.remove(element);
        }
    }

    /**
     * removeAt
     *
     * @param index
     */
    public void removeAt(int index) {
        remove(index);
    }

    /**
     * Insert
     *
     * @param index
     * @param element
     */
    public final void insert(int index, GspEnumValue element) {
        super.insert(index, element);
    }

    /**
     * Index[int]
     *
     */
    // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // ORIGINAL LINE: public new GspEnumValue this[int index] =>
    // (GspEnumValue)((_list[index] instanceof GspEnumValue) ? _list[index] : null);
    // public GspEnumValue this[int index] => (GspEnumValue)((_list.get(index)
    // instanceof GspEnumValue) ? _list.get(index) : null);

    // /// <summary>
    // /// Index[string]
    // /// </summary>
    // public Element this[string id]
    // {
    // get
    // {
    // foreach(Element _elem in _list)
    // {
    // if(id.equals(_elem.ID))
    // return _elem;
    // }
    // return null;
    // }
    // }

    // region ICloneable Members

    /**
     * 克隆
     *
     * @return
     */
    public final GspEnumValueCollection clone() {
        return (GspEnumValueCollection)super.clone();
    }

    // endregion
}
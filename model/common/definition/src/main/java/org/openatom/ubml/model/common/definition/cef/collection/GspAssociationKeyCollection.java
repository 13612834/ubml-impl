/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.collection;

import org.openatom.ubml.model.common.definition.cef.element.GspAssociationKey;

/**
 * The Collection Of AssociationKey
 *
 * @ClassName: GspAssociationKeyCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationKeyCollection extends BaseList<GspAssociationKey> implements Cloneable {
    private static final long serialVersionUID = 1L;

    /**
     * 创建关联关系外键集合
     */
    public GspAssociationKeyCollection() {
    }

    /**
     * add AssociationKey
     *
     * @param associationKey
     */
    public final void addAssociation(GspAssociationKey associationKey) {
        super.add(associationKey);
    }

    /**
     * remove AssociationKey
     *
     * @param associationKey
     */
    public final void remove(GspAssociationKey associationKey) {
        if (contains(associationKey)) {
            super.remove(associationKey);
        }
    }

    /**
     * removeAt
     *
     * @param index
     */
    public void removeAt(int index) {
        remove(index);
    }

    /**
     * Insert
     *
     * @param index
     * @param associationKey
     */
    public final void insert(int index, GspAssociationKey associationKey) {
        super.insert(index, associationKey);
    }

    /**
     * Index
     *
     */
    // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // ORIGINAL LINE: public new GspAssociationKey this[int index] =>
    // (GspAssociationKey)((_list[index] instanceof GspAssociationKey) ?
    // _list[index] : null);
    // public GspAssociationKey this[int index] =>
    // (GspAssociationKey)((_list.get(index) instanceof GspAssociationKey) ?
    // _list.get(index) : null);

    // //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    // //#region ICloneable Members

    // /**
    // 克隆

    // @return
    // */
    // public Object clone()
    // {
    // Object tempVar = Memberwiseclone();
    // GspAssociationKeyCollection newObj = (GspAssociationKeyCollection)((tempVar
    // instanceof GspAssociationKeyCollection) ? tempVar : null);
    // newObj._list = new java.util.arrayList();
    // for (GspAssociationKey item : this)
    // {
    // Object tempVar2 = item.clone();
    // newObj.add((GspAssociationKey)((tempVar2 instanceof GspAssociationKey) ?
    // tempVar2 : null));
    // }
    // return newObj;
    // }

    // //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    // //#endregion
}
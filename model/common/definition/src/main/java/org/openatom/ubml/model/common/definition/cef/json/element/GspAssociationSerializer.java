/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociationKey;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

/**
 * The Json Serializer Of GspAssociation
 *
 * @ClassName: GspAssociationSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationSerializer extends JsonSerializer<GspAssociation> {

    private CefFieldSerializer eleConvertor;

    public GspAssociationSerializer(CefFieldSerializer eleConvertor) {
        this.eleConvertor = eleConvertor;
    }

    public GspAssociationSerializer() {
        this.eleConvertor = new CefFieldSerializer();
    }

    @Override
    public void serialize(GspAssociation value, JsonGenerator writer, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(writer);
        writeBaseProperty(value, writer);
        writeSelfProperty(value, writer);
        SerializerUtils.writeEndObject(writer);
    }


    //region BaseProp
    private void writeBaseProperty(GspAssociation value, JsonGenerator writer) {
        SerializerUtils.writePropertyValue(writer, CefNames.ID, value.getId());
        SerializerUtils.writePropertyValue(writer, CefNames.I18N_RESOURCE_INFO_PREFIX, value.getI18nResourceInfoPrefix());
        this.writeRefElementCollection(value, writer);
        this.writeExtendAssoBaseProperty(value, writer);
    }

    private void writeRefElementCollection(GspAssociation ass, JsonGenerator writer) {
        SerializerUtils.writePropertyName(writer, CefNames.REF_ELEMENT_COLLECTION);
        SerializerUtils.WriteStartArray(writer);
        if (ass.getRefElementCollection().size() > 0) {
            for (IGspCommonField refElement : ass.getRefElementCollection()) {
                this.eleConvertor.serialize(refElement, writer, null);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }

    protected void writeExtendAssoBaseProperty(GspAssociation association, JsonGenerator writer) {
    }
    //endregion

    //region SelfProp
    private void writeSelfProperty(GspAssociation association, JsonGenerator writer) {
        SerializerUtils.writePropertyValue(writer, CefNames.REF_MODEL_ID, association.getRefModelID());
        SerializerUtils.writePropertyValue(writer, CefNames.REF_MODEL_NAME, association.getRefModelName());
        SerializerUtils.writePropertyValue(writer, CefNames.REF_MODEL_CODE, association.getRefModelCode());
        SerializerUtils.writePropertyValue(writer, CefNames.REF_MODEL_PKG_NAME, association.getRefModelPkgName());
        SerializerUtils.writePropertyValue(writer, CefNames.ASSO_REF_OBJECT_ID, association.getRefObjectID());
        SerializerUtils.writePropertyValue(writer, CefNames.ASSO_REF_OBJECT_CODE, association.getRefObjectCode());
        SerializerUtils.writePropertyValue(writer, CefNames.ASSO_REF_OBJECT_NAME, association.getRefObjectName());
        this.writeKeyCollection(writer, association);
        SerializerUtils.writePropertyValue(writer, CefNames.WHERE, association.getWhere());
        SerializerUtils.writePropertyValue(writer, CefNames.ASS_SEND_MESSAGE, association.getAssSendMessage());
        SerializerUtils.writePropertyValue(writer, CefNames.FOREIGN_KEY_CONSTRAINT_TYPE, association.getForeignKeyConstraintType().getValue());
        SerializerUtils.writePropertyValue(writer, CefNames.DELETE_RULE_TYPE, association.getDeleteRuleType().getValue());
        SerializerUtils.writePropertyValue(writer, CefNames.ASSO_MODEL_INFO, association.getAssoModelInfo());
        this.writeExtendAssoSelfProperty(writer, association);
    }

    private void writeKeyCollection(JsonGenerator writer, GspAssociation ass) {
        SerializerUtils.writePropertyName(writer, CefNames.KEY_COLLECTION);
        SerializerUtils.WriteStartArray(writer);
        if (ass.getKeyCollection().size() > 0) {
            for (GspAssociationKey assoKey : ass.getKeyCollection())
                SerializerUtils.writePropertyValue_Object(writer, assoKey);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    protected void writeExtendAssoSelfProperty(JsonGenerator writer, GspAssociation bizElement) {
    }
    //endregion
}

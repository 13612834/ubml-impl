/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.json.Variable.CommonVariableEntitySeriazlier;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonModel;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectSerializer;


/**
 * The Json Serializer Of Common Model
 *
 * @ClassName: CommonModelSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CommonModelSerializer extends JsonSerializer<IGspCommonModel> {
    @Override
    public void serialize(IGspCommonModel model, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

        SerializerUtils.writeStartObject(jsonGenerator);
        writeBaseProperty(model, jsonGenerator);
        writeSelfProperty(jsonGenerator, model);
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    //region BaseSelf

    private void writeBaseProperty(IGspCommonModel model, JsonGenerator writer) {
        writeModelInfo(model, writer);
        writeExtendModelProperty(model, writer);
    }

    private void writeModelInfo(IGspCommonModel model, JsonGenerator jsonGenerator) {
        writeBaseInfo(model, jsonGenerator);
        writeCommonModelMainObjectInfo(model, jsonGenerator);
        writeVariableEntity(model, jsonGenerator);
    }

    private void writeBaseInfo(IGspCommonModel model, JsonGenerator jsonGenerator) {
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.ID, model.getID());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.CODE, model.getCode());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.NAME, model.getName());
        SerializerUtils.writePropertyValue(jsonGenerator, CefNames.I18N_RESOURCE_INFO_PREFIX, model.getI18nResourceInfoPrefix());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.IS_VIRTUAL, model.getIsVirtual());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.ENTITY_TYPE, model.getEntityType());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.GENERATING_ASSEMBLY, model.getDotnetGeneratingAssembly());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.IS_USE_NAMESPACE_CONFIG, model.getIsUseNamespaceConfig());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.VERSION_CONTROL_INFO, model.getVersionContronInfo());
        SerializerUtils.writePropertyValue(jsonGenerator, CommonModelNames.BE_LABEL, model.getBeLabel());

        writeFkConstraints(model, jsonGenerator);
    }

    private void writeFkConstraints(IGspCommonModel model, JsonGenerator jsonGenerator) {
//        if (model.getFkConstraints() == null || model.FkConstraints.Count <= 0)
//            return;
//        XmlElement mhElement = AddElement(parent, CommonModelNames.FkConstraints);
//        foreach (RelationForeignKeyConstraint constraint in model.FkConstraints)
//        {
//            XmlElement constraintElement = AddElement(mhElement, CommonModelNames.FkConstraint);
//            constraintElement.SetAttribute(CommonModelNames.Value, Ecp.Caf.Common.Serializer.Serialize(constraint));
//        }
    }

    private void writeCommonModelMainObjectInfo(IGspCommonModel model, JsonGenerator jsonGenerator) {
        SerializerUtils.writePropertyName(jsonGenerator, CommonModelNames.MAIN_OBJECT);
        //IGspCommonObject
        CmObjectSerializer cmObjectSerializer = getCmObjectSerializer();
        cmObjectSerializer.serialize(model.getMainObject(), jsonGenerator, null);
    }

    private void writeVariableEntity(IGspCommonModel model, JsonGenerator writer) {
        SerializerUtils.writePropertyName(writer, CommonModelNames.VARIABLES);
        (new CommonVariableEntitySeriazlier()).serialize(model.getVariables(), writer, null);
    }
    //endregion

    //region SelfProp
    private void writeSelfProperty(JsonGenerator writer, IGspCommonModel model) {
        //扩展模型属性
        writeExtendModelSelfProperty(model, writer);
    }
    //endregion

    protected abstract void writeExtendModelProperty(IGspCommonModel model, JsonGenerator jsonGenerator);

    protected abstract void writeExtendModelSelfProperty(IGspCommonModel model, JsonGenerator writer);

    protected abstract CmObjectSerializer getCmObjectSerializer();
}

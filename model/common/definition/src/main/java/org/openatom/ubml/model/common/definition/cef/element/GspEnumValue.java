/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.element;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Definition Of EnumValue
 *
 * @ClassName: EnumValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspEnumValue implements Cloneable {

    private String val;

    private String name;

    private int index;

    private String stringIndex;

    private boolean isDefault;

    private String privateI18nResourceInfoPrefix;

    /**
     * 构造方法
     */
    public GspEnumValue() {
        setIsDefaultEnum(false);
    }

    /**
     * 值
     */
    @JsonProperty("Value")
    public final String getValue() {
        return val;
    }

    public final void setValue(String value) {
        val = value;
    }

    /**
     * 名称
     */
    @JsonProperty("Name")
    public final String getName() {
        return name;
    }

    public final void setName(String value) {
        name = value;
    }

    /**
     * 枚举的索引值
     */
    @JsonProperty("Index")
    public final int getIndex() {
        return index;
    }

    public final void setIndex(int value) {
        index = value;
    }

    /**
     * 枚举的索引值-String类型
     */
    @JsonProperty("StringIndex")
    public final String getStringIndex() {
        return stringIndex;
    }

    public void setStringIndex(String value) {
        stringIndex = value;
    }

    /**
     * 该枚举值是否为默认值
     */
    @JsonProperty("IsDefaultEnum")
    public final boolean getIsDefaultEnum() {
        return isDefault;
    }

    public final void setIsDefaultEnum(boolean value) {
        isDefault = value;
    }

    /**
     * 国际化项前缀
     */
    @JsonProperty("I18nResourceInfoPrefix")
    public final String getI18nResourceInfoPrefix() {
        return privateI18nResourceInfoPrefix;
    }

    public final void setI18nResourceInfoPrefix(String value) {
        privateI18nResourceInfoPrefix = value;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    //#region ICloneable Members

    /**
     * 克隆
     *
     * @return
     */
    public final GspEnumValue clone() {
        GspEnumValue newEnumValue = new GspEnumValue();
        newEnumValue.val = this.val;
        newEnumValue.name = this.name;
        newEnumValue.index = this.index;
        newEnumValue.stringIndex = this.stringIndex;
        newEnumValue.setIsDefaultEnum(this.isDefault);
        newEnumValue.setI18nResourceInfoPrefix(this.getI18nResourceInfoPrefix());
        return newEnumValue;
    }

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    //#endregion
}
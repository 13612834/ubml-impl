/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.entity.object;

import org.openatom.ubml.model.common.definition.cef.collection.BaseList;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonObject;

/**
 * The Definition Of Unique Constraint
 *
 * @ClassName: GspUniqueConstraint
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspUniqueConstraint implements Cloneable {
    private String id;
    private String code;
    private String name;
    private ConstraintType type = ConstraintType.Unique;
    private String constraintMessage;

    private BaseList<String> elementList;
    /**
     * 国际化项前缀
     */
    private String privateI18nResourceInfoPrefix;

    public GspUniqueConstraint() {
        setType(ConstraintType.Unique);
    }

    /**
     * 唯一标识
     */
    public final String getId() {
        return this.id;
    }

    public final void setId(String value) {
        this.id = value;
    }

    /**
     * 编码
     */
    public final String getCode() {
        return code;
    }

    public final void setCode(String value) {
        code = value;
    }

    /**
     * 名称
     */
    public final String getName() {
        return this.name;
    }

    public final void setName(String value) {
        this.name = value;
    }

    /**
     * 约束类型
     */
    public final ConstraintType getType() {
        return this.type;
    }

    public final void setType(ConstraintType value) {
        this.type = value;
    } // 暂时只提供此种约束类型

    /**
     * 约束消息
     */
    public final String getConstraintMessage() {
        return constraintMessage;
    }

    public final void setConstraintMessage(String value) {
        constraintMessage = value;
    }

    /**
     * 约束包含的字段列表
     */
    public final BaseList<String> getElementList() {
        return this.elementList;
    }

    public final void setElementList(BaseList<String> value) {
        this.elementList = value;
    }

    public final String getI18nResourceInfoPrefix() {
        return privateI18nResourceInfoPrefix;
    }

    public final void setI18nResourceInfoPrefix(String value) {
        privateI18nResourceInfoPrefix = value;
    }

    // region ICloneable Members

    /**
     * 克隆
     *
     * @return
     */
    public final GspUniqueConstraint clone() {
        Object tempVar;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
        GspUniqueConstraint newConstraint = (GspUniqueConstraint)((tempVar instanceof GspUniqueConstraint) ? tempVar
                : null);
        newConstraint.id = this.id;
        newConstraint.code = this.getCode();
        newConstraint.name = this.name;
        newConstraint.type = this.type;
        newConstraint.setConstraintMessage(this.getConstraintMessage());
        newConstraint.elementList = this.elementList;
        newConstraint.setI18nResourceInfoPrefix(this.getI18nResourceInfoPrefix());
        return newConstraint;
    }

    public final GspUniqueConstraint clone(IGspCommonObject absObject) {
        GspUniqueConstraint newObj = clone();
        if (elementList != null) {
            // elementList中元素是对象中的元素，需要从对象中查找，不能单纯的克隆
            // newObj.elementList = this.elementList.clone(absObject, null);
        }
        return newObj;
    }
    // endregion
}
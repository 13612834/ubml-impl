/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.collection;

import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * The BaseList Definition,All List Use This Class
 *
 * @ClassName: CommonFieldRuleNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BaseList<T> extends java.util.ArrayList<T> implements java.util.List<T>, Cloneable {

    private static final long serialVersionUID = 1L;

    /**
     * 操作的数组
     */
//    protected ArrayList<T> _list = new ArrayList<T>();

    /**
     * 构造一个新的列表
     */
    public BaseList() {
    }

    // region IList 成员

    /**
     * 获取、设置指定位置的项目
     */
    public T getItem(int index) {
        if (index > -1 && index < this.getCount()) {
            return super.get(index);
        }
        return null;
    }

    public ArrayList<T> getAllItems(Predicate<T> predicate) {
        ArrayList<T> result = new ArrayList<T>();
        for (T item : this) {
            if (predicate.test(item)) {
                result.add(item);
            }
        }
        return result;
    }

    public T getItem(Predicate<T> predicate) {
        ArrayList<T> result = getAllItems(predicate);
        if (result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public void setItem(int index, T value) {
        set(index, value);
    }

    /**
     * 移出指定位置的项目
     *
     * @param index
     */
    public void removeAt(int index) {
        super.remove(index);
    }

    /**
     * 在指定的位置插入项目
     *
     * @param index 插入位置
     * @param value 新项目
     */
    public void insert(int index, T value) {
        super.add(index, value);
    }

    /**
     * 移除指定的项目
     *
     * @param value
     */
    public void removeItem(Object value) {
        for (int i = 0; i < super.size(); i++) {
            if (super.get(i) == value) {
                super.remove(i);
                break;
            }
        }
    }

    /**
     * 是否包含指定的项目
     *
     * @param value
     * @return
     */
    public boolean contains(Object value) {
        return super.contains(value);
    }

//	/**
//	 * 清除所有的内容
//	 *
//	 */
//	public void clear() {
//		_list.clear();
//	}

    /**
     * 返回指定项目的索引
     *
     * @param value 当前列表的任意一项
     * @return 位置
     */
//	public int indexOf(Object value) {
//		for (int i = 0; i < _list.size(); i++) {
//			if (_list.get(i) == value) {
//				return i;
//			}
//		}
//		return -1;
//	}

    /**
     * 添加新项目
     *
     * @param value 新项目
     * @return 添加到的位置
     */
//	public void add(T value) {
//		_list.add(value);
//	}

    /**
     * 是否固定尺寸
     */
    // public boolean IsFixedSize => _list.IsFixedSize;

    // endregion

    // region ICollection 成员
    public final boolean getIsSynchronized() {
        // TODO: 添加 BaseList.IsSynchronized getter 实现
        return false;
    }

    public int getCount() {
        return super.size();
    }

    // public final void CopyTo(ArrayList<T> array, int index) {
    // _list.subList(array, index);
    // }

    // public Object getSyncRoot(){ _list.SyncRoot;}

    // endregion

    // region IEnumerable 成员

    // public final Iterable getEnumerator() {
    // return _list.iterator();
    // }

    // endregion

    // /**
    // * 列表的枚举器类
    // *
    // */
    // // C# TO JAVA CONVERTER TODO TASK: The interface type was changed to the
    // closest
    // // equivalent Java type, but the methods implemented will need adjustment:
    // public static class ListEnumerator implements java.util.Iterable {
    // public BaseList _base = null;
    // private int index = -1;

    // public ListEnumerator(BaseList list) {
    // _base = list;
    // }

    // // region IEnumerator 成员

    // /**
    // * 重置
    // *
    // */
    // public final void reset() {
    // index = -1;
    // }

    // /**
    // 获取当前项

    // */
    // public Object Current
    // {
    // return _base._list[index];
    // }

    // /**
    // * 是否有下一个项目
    // *
    // * @return
    // */
    // public final boolean moveNext() {
    // return (++index) < _base.size();
    // }

    // @Override
    // public boolean hasNext() {
    // return false;
    // }

    // @Override
    // public Object next() {
    // return null;
    // }

    // // C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    // // endregion

    // }

    // region Clonenable
    @SuppressWarnings("unchecked")
    public BaseList<T> clone() {
        return (BaseList<T>)super.clone();
    }
    //// endregion
}
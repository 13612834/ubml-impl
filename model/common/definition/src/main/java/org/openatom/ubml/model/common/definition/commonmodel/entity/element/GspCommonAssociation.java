/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.entity.element;

import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonModel;

/**
 * The Definition Of Common Association
 *
 * @ClassName: GspCommonAssociation
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspCommonAssociation extends GspAssociation implements Cloneable {
    private IGspCommonModel refModel;

    /**
     * 创建关联关系
     */
    public GspCommonAssociation() {
    }

    /**
     * 引用的数据模型
     */
    public final IGspCommonModel getRefModel() {
        return refModel;
    }

    public final void setRefModel(IGspCommonModel value) {
        refModel = value;
    }

    // region ICloneable Members

    // endregion
}
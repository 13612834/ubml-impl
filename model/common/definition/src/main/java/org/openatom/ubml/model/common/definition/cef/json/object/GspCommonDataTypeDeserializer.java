/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.object;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import org.openatom.ubml.model.common.definition.cef.IGspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.collection.CommonDtmCollection;
import org.openatom.ubml.model.common.definition.cef.collection.CommonValCollection;
import org.openatom.ubml.model.common.definition.cef.collection.GspFieldCollection;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.common.definition.cef.entity.GspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.json.element.CefFieldDeserializer;
import org.openatom.ubml.model.common.definition.cef.json.operation.CommonDtmDeserializer;
import org.openatom.ubml.model.common.definition.cef.json.operation.CommonValDeserializer;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of CommonDataType
 *
 * @ClassName: GspCommonDataTypeDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspCommonDataTypeDeserializer extends JsonDeserializer<IGspCommonDataType> {
    private GspCommonDataType commonDataType;

    @Override
    public IGspCommonDataType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return deserializeCommonDataType(jsonParser);
    }

    public IGspCommonDataType deserializeCommonDataType(JsonParser jsonParser) {
        commonDataType = createCommonDataType();

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(commonDataType, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);

        afterReadCommonDataType(commonDataType);
        return commonDataType;
    }

    //#region 私有方法

    private void readPropertyValue(GspCommonDataType commonDataType, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.ID:
                commonDataType.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.CODE:
                commonDataType.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.NAME:
                commonDataType.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.BE_LABEL:
                commonDataType.setBeLabel(SerializerUtils.readStringArray(jsonParser));
                break;
            case CefNames.BIZ_TAG_IDS:
                commonDataType.setBizTagIds(SerializerUtils.readStringArray(jsonParser));
                break;
            case CefNames.IS_REF:
                commonDataType.setIsRef(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.DTM_BEFORE_SAVE:
                readDtmBeforeSave(jsonParser, commonDataType);
                break;
            case CefNames.DTM_AFTER_MODIFY:
                readDtmAfterModify(jsonParser, commonDataType);
                break;
            case CefNames.DTM_AFTER_CREATE:
                readDtmAfterCreate(jsonParser, commonDataType);
                break;
            case CefNames.VAL_AFTER_MODIFY:
                readValAfterModify(jsonParser, commonDataType);
                break;
            case CefNames.VAL_BEFORE_SAVE:
                readValBeforeSave(jsonParser, commonDataType);
                break;
            case CefNames.CONTAIN_ELEMENTS:
                readContainElements(jsonParser, commonDataType);
                break;
            case CefNames.ENABLE_DYNAMIC_PROP:
                //CommonDataType上动态属性相关-已废弃
                SerializerUtils.readPropertyValue_boolean(jsonParser);
                break;
            case CefNames.DYNAMIC_PROP_REPOSITORY_COMP:
                //CommonDataType上动态属性相关-已废弃
                SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case CefNames.DYNAMIC_PROP_SERIALIZER_COMPS:
                //CommonDataType上动态属性相关-已废弃
                SerializerUtils.readStringArray(jsonParser);
                break;
            case CefNames.I18N_RESOURCE_INFO_PREFIX:
                commonDataType.setI18nResourceInfoPrefix(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.CUSTOMIZATION_INFO:
                commonDataType.setCustomizationInfo(SerializerUtils.readPropertyValue_Object(CustomizationInfo.class, jsonParser));
                try {
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw new RuntimeException(String.format("GspCommonDataTypeDeserializer反序列化错误：%1$s", propName));
                }
                break;
            default:
                if (!readExtendDataTypeProperty(commonDataType, propName, jsonParser)) {
                    throw new RuntimeException(String.format("GspCommonDataTypeDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    private void readContainElements(JsonParser jsonParser, GspCommonDataType commonDataType) {
        GspFieldCollection collection = createFieldCollection();
        SerializerUtils.readArray(jsonParser, createFieldDeserializer(), collection);
        handleContainElements(collection, commonDataType);
        commonDataType.setContainElements(collection);
    }

    private void handleContainElements(GspFieldCollection collection, GspCommonDataType commonDataType) {
        for (IGspCommonField field : collection) {
            field.setBelongObject(commonDataType);
            if (field.getChildAssociations() != null && field.getChildAssociations().getCount() > 0) {
                for (GspAssociation association : field.getChildAssociations()) {
                    if (association.getRefElementCollection() != null) {
                        for (IGspCommonField assofield : association.getRefElementCollection()) {
                            assofield.setBelongObject(commonDataType);
                        }
                    }
                }
            }
        }
    }

    private CommonDtmCollection readCommonDtmCollection(JsonParser jsonParser) {
        CommonDtmCollection collection = new CommonDtmCollection();
        CommonDtmDeserializer der = new CommonDtmDeserializer();
        SerializerUtils.readArray(jsonParser, der, collection);
        return collection;
    }

    private CommonValCollection readCommonValidationCollection(JsonParser jsonParser) {
        CommonValCollection collection = new CommonValCollection();
        CommonValDeserializer der = new CommonValDeserializer();
        SerializerUtils.readArray(jsonParser, der, collection);
        return collection;
    }

    private void readDtmBeforeSave(JsonParser jsonParser, GspCommonDataType commonDataType) {
        commonDataType.setDtmBeforeSave(readCommonDtmCollection(jsonParser));
    }

    private void readDtmAfterModify(JsonParser jsonParser, GspCommonDataType commonDataType) {
        commonDataType.setDtmAfterModify(readCommonDtmCollection(jsonParser));
    }

    private void readDtmAfterCreate(JsonParser jsonParser, GspCommonDataType commonDataType) {
        commonDataType.setDtmAfterModify(readCommonDtmCollection(jsonParser));
    }

    private void readValAfterModify(JsonParser jsonParser, GspCommonDataType commonDataType) {
        commonDataType.setValAfterModify(readCommonValidationCollection(jsonParser));
    }

    private void readValBeforeSave(JsonParser jsonParser, GspCommonDataType commonDataType) {
        commonDataType.setValBeforeSave(readCommonValidationCollection(jsonParser));
    }

    //#endregion

    protected GspCommonDataType getGspCommonDataType() {
        return commonDataType;
    }

    protected abstract GspCommonDataType createCommonDataType();

    protected GspFieldCollection createFieldCollection() {
        return new GspFieldCollection();
    }

    protected abstract CefFieldDeserializer createFieldDeserializer();

    protected boolean readExtendDataTypeProperty(GspCommonDataType commonDataType, String propName, JsonParser jsonParser) {
        return false;
    }

    protected void afterReadCommonDataType(GspCommonDataType commonDataType) {
    }

}

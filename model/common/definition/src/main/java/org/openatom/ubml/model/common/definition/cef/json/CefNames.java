/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json;

/**
 * The Serializer Names Of Cef Model
 *
 * @ClassName: CefNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CefNames {

    public static final String ID = "ID";
    public static final String CODE = "Code";
    public static final String NAME = "Name";
    public static final String PKG_NAME = "PkgName";
    public static final String LABEL_ID = "LabelID";
    public static final String DATA_TYPE = "DataType";
    public static final String LENGTH = "Length";
    public static final String PRECISION = "Precision";
    public static final String DEFAULT_VALUE = "DefaultValue";
    public static final String DEFAULT_VAULE_TYPE = "DefaultVauleType";
    public static final String DEFAULT_VALUE_TYPE = "DefaultValueType";
    public static final String IS_VIRTUAL = "IsVirtual";
    public static final String IS_REQUIRE = "IsRequire";
    public static final String OBJECT_TYPE = "ObjectType";
    public static final String IS_REF = "IsRef";
    public static final String BELONG_OBJECT = "BelongObject";
    public static final String CHILD_ASSOCIATIONS = "ChildAssociations";
    public static final String CONTAIN_ENUM_VALUES = "ContainEnumValues";
    public static final String CONTAIN_ELEMENTS = "ContainElements";
    public static final String DTM_AFTER_CREATE = "DtmAfterCreate";
    public static final String DTM_AFTER_MODIFY = "DtmAfterModify";
    public static final String DTM_BEFORE_SAVE = "DtmBeforeSave";
    public static final String VAL_AFTER_MODIFY = "ValAfterModify";
    public static final String VAL_BEFORE_SAVE = "ValBeforeSave";
    public static final String IS_MULTI_LANGUAGE = "IsMultiLanguage";
    public static final String COMPONENT_ID = "ComponentId";
    public static final String DESCRIPTION = "Description";
    public static final String COMPONENT_PKG_NAME = "ComponentPkgName";
    public static final String COMPONENT_NAME = "ComponentName";
    public static final String CHILD_ELEMENTS = "ChildElements";
    public static final String MAPPING_RELATION = "MappingRelation";
    public static final String HAS_ASSOCIATION = "HasAssociation";
    public static final String IS_REF_ELEMENT = "IsRefElement";
    public static final String REF_ELEMENT_ID = "RefElementID";
    public static final String PARENT_ASSOCIATION = "ParentAssociation";
    public static final String REF_MODEL_ID1 = "RefModelId";
    public static final String ID1 = "Id";
    public static final String SOURCE_ELEMENT = "SourceElement";
    public static final String SOURCE_ELEMENT_DISPLAY = "SourceElementDisplay";
    public static final String TARGET_ELEMENT_DISPLAY = "TargetElementDisplay";
    public static final String TARGET_ELEMENT = "TargetElement";
    public static final String REF_DATA_MODEL_NAME = "RefDataModelName";

    public static final String REQUEST_ELEMENTS = "RequestElements";
    public static final String GET_EXECUTING_DATA_STATUS = "GetExecutingDataStatus";

    public static final String COLLECTION_TYPE = "CollectionType";

    public static final String BE_LABEL = "BeLabel";
    //Element
    public static final String IS_FROM_ASSO_UDT = "IsFromAssoUdt";
    public static final String BILL_CODE_CONFIG = "BillCodeConfig";
    public static final String CONTAIN_CHILD_OBJECTS = "ContainChildObjects";
    public static final String PRIMAY_KEY_ID = "PrimayKeyID";
    public static final String EXTEND_NODE_LIST = "ExtendNodeList";
    public static final String EXT_PROPERTIES = "ExtProperties";
    public static final String RESOURCE = "Resource";
    public static final String SHOW_MESSAGE = "ShowMessage";
    public static final String TYPE = "Type";
    public static final String TABLE_NAME = "TableName";
    public static final String COLUMN_NAME = "ColumnName";
    public static final String CONTAIN_CONSTRAINTS = "ContainConstraints";
    public static final String KEYS = "Keys";
    public static final String GENERATING_ASSEMBLY = "GeneratingAssembly";
    public static final String MDATA_TYPE = "MDataType";
    public static final String DYNAMIC_PROP_SET_INFO = "DynamicPropSetInfo";
    public static final String I18N_RESOURCE_INFO_PREFIX = "I18nResourceInfoPrefix";
    public static final String CUSTOMIZATION_INFO = "CustomizationInfo";
    // Bug 353424: 需兼容已有元数据，此处不打标签
    public static final String IS_CUSTOMIZED = "Customized";
    public static final String DIMENSION_INFO = "DimensionInfo";
    public static final String FIRST_DIMENSION = "FirstDimension";
    public static final String FIRST_DIMENSION_CODE = "FirstDimensionCode";
    public static final String FIRST_DIMENSION_NAME = "FirstDimensionName";
    public static final String SECOND_DIMENSION = "SecondDimension";
    public static final String SECOND_DIMENSION_CODE = "SecondDimensionCode";
    public static final String SECOND_DIMENSION_NAME = "SecondDimensionName";
    public static final String ASSO_MODEL_INFO = "AssoModelInfo";
    public static final String VALUE = "Value";
    public static final String IS_DEFAULT_ENUM = "IsDefaultEnum";
    public static final String INDEX = "Index";
    public static final String STRING_INDEX = "StringIndex";
    public static final String ENUM_INDEX_TYPE = "EnumIndexType";
    public static final String ENABLE_RTRIM = "EnableRtrim";
    public static final String IS_BIG_NUMBER = "IsBigNumber";
    public static final String BIZ_TAG_IDS = "BizTagIds";


    // Udt相关
    public static final String IS_UDT = "IsUdt";
    public static final String UDT_PKG_NAME = "UdtPkgName";
    public static final String UDT_ID = "UdtID";
    public static final String UDT_NAME = "UdtName";
    public static final String UNIFIED_DATA_TYPE = "UnifiedDataType";

    public static final String MAPPING_INFO = "MappingInfo";
    public static final String KEY_INFO = "KeyInfo";
    public static final String VALUE_INFO = "ValueInfo";
    // 关联
    public static final String REF_MODEL_ID = "RefModelID";
    public static final String REF_MODEL_CODE = "RefModelCode";
    public static final String REF_MODEL_NAME = "RefModelName";
    public static final String REF_MODEL_PKG_NAME = "RefModelPkgName";
    public static final String ASSO_REF_OBJECT_ID = "RefObjectID";
    public static final String ASSO_REF_OBJECT_CODE = "RefObjectCode";
    public static final String ASSO_REF_OBJECT_NAME = "RefObjectName";
    public static final String KEY_COLLECTION = "KeyCollection";
    public static final String ASSOCIATION_KEY = "AssociationKey";
    public static final String WHERE = "Where";
    public static final String ASS_SEND_MESSAGE = "AssSendMessage";
    public static final String FOREIGN_KEY_CONSTRAINT_TYPE = "ForeignKeyConstraintType";
    public static final String DELETE_RULE_TYPE = "DeleteRuleType";
    public static final String REF_ELEMENT_COLLECTION = "RefElementCollection";
    public static final String MODEL_CONFIG_ID = "ModelConfigId";
    public static final String MAIN_OBJ_CODE = "MainObjCode";

    // 变量
    public static final String VARIABLE_SOURCE_TYPE = "VariableSourceType";
    // 操作
    public static final String IS_GENERATE_COMPONENT = "IsGenerateComponent";

    //动态属性集合
    public static final String ENABLE_DYNAMIC_PROP = "EnableDynamicProp";
    public static final String DYNAMIC_PROP_REPOSITORY_COMP = "DynamicPropRepositoryComp";
    public static final String DYNAMIC_PROP_SERIALIZER_COMP = "DynamicPropSerializerComp";
    //原节点上的动态属性-已废弃
    public static final String DYNAMIC_PROP_SERIALIZER_COMPS = "DynamicPropSerializerComps";

    //region Increment
    public static final String INCREMENT_TYPE = "IncrementType";
    public static final String ADDED_DATA_TYPE = "AddedDataType";
    public static final String DELETED_ID = "DeletedId";
    public static final String HAS_INCREMENT = "HasIncrement";
    public static final String PROPERTY_TYPE = "PropertyType";
    public static final String PROPERTY_VALUE = "PropertyValue";
    public static final String OBJECT_DESERIALIZER = "ObjectDeserializer";
    public static final String OBJECT_SERIALIZER = "ObjectSerializer";
    public static final String OBJECT_INCREMENT_TYPE = "ObjectIncrementType";
    public static final String PROPERTY_INCREMENTS = "PropertyIncrements";
    public static final String FIELD_INCREMENTS = "FieldIncrements";
    public static final String CHILD_INCREMENTS = "ChildIncrements";
    public static final String ELEMENT = "Element";
    public static final String CHILD_INCREMENT = "ChildIncrement";
    public static final String CustomizationInfo = "CustomizationInfo";
    //endregion


}

/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.util.EnumSet;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.operation.CommonOperation;
import org.openatom.ubml.model.common.definition.cef.operation.ExecutingDataStatus;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of Common Operation
 *
 * @ClassName: CommonOpDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CommonOpDeserializer<T extends CommonOperation> extends JsonDeserializer<T> {
    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return deserializeCommonOp(jsonParser);
    }

    public T deserializeCommonOp(JsonParser jsonParser) {
        T op = CreateCommonOp();

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(op, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return op;
    }

    private void readPropertyValue(CommonOperation op, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.ID:
                op.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.CODE:
                op.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.NAME:
                op.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.DESCRIPTION:
                op.setDescription(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.COMPONENT_ID:
                op.setComponentId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.COMPONENT_PKG_NAME:
                op.setComponentPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.COMPONENT_NAME:
                op.setComponentName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.IS_REF:
                op.setIsRef(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.IS_GENERATE_COMPONENT:
                op.setIsGenerateComponent(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            default:
                if (!readExtendOpProperty(op, propName, jsonParser)) {
                    throw new RuntimeException(String.format("CommonOpDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    protected boolean readExtendOpProperty(CommonOperation op, String propName, JsonParser jsonParser) {
        return false;
    }

    protected abstract T CreateCommonOp();


    protected EnumSet<ExecutingDataStatus> readGetExecutingDataStatus(JsonParser jsonParser) {
        EnumSet<ExecutingDataStatus> result = EnumSet.noneOf(ExecutingDataStatus.class);
        int intValueSum = SerializerUtils.readPropertyValue_Integer(jsonParser);
        ExecutingDataStatus[] values = ExecutingDataStatus.values();
        for (int i = values.length - 1; i >= 0; i--) {
            ExecutingDataStatus value = values[i];
            if (intValueSum > 0 && intValueSum >= value.getValue()) {
                result.add(value);
                intValueSum -= value.getValue();
            }
        }
        return result;
    }
}

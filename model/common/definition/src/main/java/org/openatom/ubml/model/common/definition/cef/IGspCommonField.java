/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef;

import java.util.List;
import org.openatom.ubml.model.common.definition.cef.collection.GspAssociationCollection;
import org.openatom.ubml.model.common.definition.cef.collection.GspEnumValueCollection;
import org.openatom.ubml.model.common.definition.cef.element.ElementDefaultVauleType;
import org.openatom.ubml.model.common.definition.cef.element.EnumIndexType;
import org.openatom.ubml.model.common.definition.cef.element.FieldCollectionType;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.element.GspElementDataType;
import org.openatom.ubml.model.common.definition.cef.element.GspElementObjectType;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.common.definition.cef.entity.DynamicPropSetInfo;
import org.openatom.ubml.model.common.definition.cef.entity.MappingRelation;

/**
 * The Interface Of Common Field
 *
 * @ClassName: IGspCommonField
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IGspCommonField extends Cloneable {

    /**
     * Get The Id Of Common Field
     *
     * @return The Id Of Common Field
     */
    String getID();

    /**
     * @param value The Id To Set
     */
    void setID(String value);


    /**
     * @return The Code Of Common Field
     */
    String getCode();

    /**
     * @param value The Code To Set
     */
    void setCode(String value);

    /**
     * @return The Code Of Common Field
     */
    String getName();

    /**
     * @param value The Name To Set
     */
    void setName(String value);

    /**
     * @return The Label Of Common Field
     */
    String getLabelID();

    /**
     * @param value The Label To Set
     */
    void setLabelID(String value);

    /**
     * 标签<see cref="string"/>
     */
    List<String> getBeLabel();

    /**
     * @param beLabel The Labels To Set
     */
    void setBeLabel(List<String> beLabel);

    /**
     * @return The Tags On This Field
     */
    List<String> getBizTagIds();

    /**
     * @param bizTagIds The Tags  To Set
     */
    void setBizTagIds(List<String> bizTagIds);

    /**
     * @return The Data Type Of This Field
     */
    GspElementDataType getMDataType();

    /**
     * @param value The Data Type To Set
     */
    void setMDataType(GspElementDataType value);


    /**
     * @return The Default Value Of This Field
     */
    String getDefaultValue();

    /**
     * @param value The Default Value To Set
     */
    void setDefaultValue(String value);


    /**
     * @return The Default Value  Type Of  The Field
     */
    ElementDefaultVauleType getDefaultValueType();

    /**
     * @param value The Default Value Type To Set
     */
    void setDefaultValueType(ElementDefaultVauleType value);


    /**
     * @return The Length Of The Field
     */
    int getLength();

    /**
     * @param value The Length To Set
     */
    void setLength(int value);


    /**
     * @return The Precision Of The Field
     */
    int getPrecision();

    /**
     * @param value The Precision To Set
     */
    void setPrecision(int value);


    /**
     * @return The Field Is Virtual
     */
    boolean getIsVirtual();

    /**
     * @param value The Field Is Virtual
     */
    void setIsVirtual(boolean value);


    /**
     * @return The Field Is Required
     */
    boolean getIsRequire();

    /**
     * @param value The Field Is Required
     */
    void setIsRequire(boolean value);

    /**
     * @return The Field Is Enable RTrim.When it is true,The Framework Will Trim The Right White Spaces.
     */
    boolean isEnableRtrim();

    /**
     * @param value The Field is  Enable RTrim
     */
    void setEnableRtrim(boolean value);


    /**
     * @return The Field Is Multi Language Field.
     */
    boolean getIsMultiLanguage();

    /**
     * @param value The Field Is Multi Language Field
     */
    void setIsMultiLanguage(boolean value);


    /**
     * @return The Fields Is A Ref Field In An Association.
     */
    boolean getIsRef();

    /**
     * @param value The Field Is A Ref Field In An  Association.
     */
    void setIsRef(boolean value);


    /**
     * @return If The Field`s Data Type Is Number,Returns The Number Is A Big Number
     */
    boolean isBigNumber();

    /**
     * @param bigNumber Wether The Number Field Is Big Nuber
     */
    void setIsBigNumber(boolean bigNumber);


    /**
     * @return The Parent Object Of The Field
     */
    IGspCommonDataType getBelongObject();

    /**
     * @param value The Parent Object Of The Field To Set
     */
    void setBelongObject(IGspCommonDataType value);


    /**
     * @return The Child Elements Of The Field,It`s Used When The Field Is An Udt Field.
     */
    IFieldCollection getChildElements();


    /**
     * @return The Mapping Relation Of The Field.
     */
    MappingRelation getMappingRelation();


    /**
     * @return The Child Associations Of The Field,It`s Used When The Field Object Type Is Association
     */
    GspAssociationCollection getChildAssociations();

    /**
     * @param value The Child Associations To Set
     */
    void setChildAssociations(GspAssociationCollection value);

    /**
     * @return The Field Has Association
     */
    boolean getHasAssociation();

    /**
     * @return The Enum Values Of The Field,It`s Used When The Field Object Type Is Enum
     */
    GspEnumValueCollection getContainEnumValues();

    /**
     * @param value The Enum Values Of The Field To Set
     */
    void setContainEnumValues(GspEnumValueCollection value);


    /**
     * @return The Field Is Ref Element
     */
    boolean getIsRefElement();

    /**
     * @param value Wether The Field Is Ref Field To Set
     */
    void setIsRefElement(boolean value);


    /**
     * @return The Ref Field Id Of The Field,It`s Used When The Field  Is Ref Element.
     */
    String getRefElementId();

    /**
     * @param value The  Ref Element Id Of The Field
     */
    void setRefElementId(String value);


    /**
     * @return The Parent Association Of The Field,It`s Used When The Field Is Ref Element
     */
    GspAssociation getParentAssociation();

    /**
     * @param value The Parent Association Of The Field To Set
     */
    void setParentAssociation(GspAssociation value);


    /**
     * @return The Object Type Of The Field:None、Association、Enum、Dynamic
     */
    GspElementObjectType getObjectType();

    /**
     * @param value The Object Type Of The Field To Set
     */
    void setObjectType(GspElementObjectType value);


    /**
     * @return The Field Is An Udt Field
     */
    boolean getIsUdt();

    /**
     * @param value Wether The Field Is  An Udt Field
     */
    void setIsUdt(boolean value);

    /**
     * @return The Udt Package Name Of The Field,It`s Used When The Field Is An Udt Field
     */
    String getUdtPkgName();

    /**
     * @param value The Udt Package Name Of The Field To Set
     */
    void setUdtPkgName(String value);

    /**
     * @return The Udt Id Of The Field WHen The Field Is An Udt Field
     */
    String getUdtID();

    /**
     * @param value The Udt Id To Set
     */
    void setUdtID(String value);

    /**
     * @return The Udt Name Of The Field When The Field Is Udt
     */
    String getUdtName();

    /**
     * @param value The Udt Name To Set
     */
    void setUdtName(String value);

    /**
     * @return The Collection Type Of The Field,It is Can Un Collection、Array  Or List
     */
    FieldCollectionType getCollectionType();

    /**
     * @param value The Collection Type Of  The Field
     */
    void setCollectionType(FieldCollectionType value);

    /**
     * @return The Field Is Form Association Udt
     */
    boolean getIsFromAssoUdt();

    /**
     * @param value Wether The Field Is Form Association Udt
     */
    void setIsFromAssoUdt(boolean value);

    /**
     * @return The I18n Resource Info Key Prefix Of The Field
     */
    String getI18nResourceInfoPrefix();

    /**
     * @param value The I18n Resource Info Key Prefix Of The Field
     */
    void setI18nResourceInfoPrefix(String value);

    /**
     * @return The Dynamic Property Set Info Of The Field
     */
    DynamicPropSetInfo getDynamicPropSetInfo();

    /**
     * @param value The Dynamic Property Set Info To Set Of The Field
     */
    void setDynamicPropSetInfo(DynamicPropSetInfo value);

    /**
     * @return The Customiztion Info Of The Field
     */
    CustomizationInfo getCustomizationInfo();

    /**
     * @param customizationInfo The Customiztion Info To Set
     */
    void setCustomizationInfo(CustomizationInfo customizationInfo);

    /**
     * @return The Enum Index Type To Set When The Field Is An Enum  Field
     */
    EnumIndexType getEnumIndexType();

    /**
     * @param value The Enum Index Type To Set
     */
    void setEnumIndexType(EnumIndexType value);

    /**
     * @return The Field Has None Ref Element In Asooccitiaon
     */
    boolean hasNoneRefElementInAssociation();

    /**
     * @return The Field Donot Contaions Ref Element Form Association Udt
     */
    boolean containRefElementNotFromAssoUdt();

    /**
     * @param absObject         The Parent Object Of The New Field If This Field Is In An Object
     * @param parentAssociation The Parent Association Of The New Field If The Field Is An Association Ref Element
     * @return The New Field Cloned By This Field
     */
    IGspCommonField clone(IGspCommonDataType absObject, GspAssociation parentAssociation);
    // endregion

}

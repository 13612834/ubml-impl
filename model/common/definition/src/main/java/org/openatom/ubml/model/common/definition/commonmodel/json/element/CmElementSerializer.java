/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.json.element.CefFieldSerializer;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonElement;
import org.openatom.ubml.model.common.definition.commonmodel.entity.element.ElementCodeRuleConfig;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;

/**
 * The Json Serializer Of  CmELement
 *
 * @ClassName: VersionControlInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CmElementSerializer extends CefFieldSerializer {

    //region BaseProp
    @Override
    protected void writeExtendFieldBaseProperty(JsonGenerator writer, IGspCommonField value) {

        IGspCommonElement field = (IGspCommonElement)value;
        SerializerUtils.writePropertyValue(writer, CommonModelNames.COLUMN_ID, field.getColumnID());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.BELONG_MODEL_ID, field.getBelongModelID());
        writeExtendElementBaseProperty(writer, field);
    }
    //endregion

    //region SelfProp
    @Override
    protected void writeExtendFieldSelfProperty(JsonGenerator writer, IGspCommonField value) {
        IGspCommonElement field = (IGspCommonElement)value;
        WriteAbstractObjectInfo(writer, field);
    }

    private void WriteAbstractObjectInfo(JsonGenerator writer, IGspCommonElement field) {

        writeBillCodeConfig(writer, field.getBillCodeConfig());

        SerializerUtils.writePropertyValue(writer, CommonModelNames.READONLY, field.getReadonly());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.IS_CUSTOM_ITEM, field.getIsCustomItem());
        //扩展对象属性
        writeExtendElementSelfProperty(writer, field);
    }

    private void writeBillCodeConfig(JsonGenerator writer, ElementCodeRuleConfig config) {
        if (config == null) {
            return;
        }
        SerializerUtils.writePropertyName(writer, CommonModelNames.BILL_CODE_CONFIG);
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.BILL_CODE, config.getCanBillCode());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.CODE_GENERATE_OCCASION, config.getCodeGenerateOccasion().toString());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.CODE_GENERATE_TYPE, config.getCodeGenerateType().toString());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.BILL_CODE_NAME, config.getBillCodeName());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.BILL_CODE_ID, config.getBillCodeID());
        SerializerUtils.writeEndObject(writer);
    }
    //endregion

    //region 抽象方法
    protected abstract void writeExtendElementBaseProperty(JsonGenerator writer, IGspCommonElement element);

    protected abstract void writeExtendElementSelfProperty(JsonGenerator writer, IGspCommonElement element);

    //endregion
}

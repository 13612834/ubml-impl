package org.openatom.ubml.model.vo.definition.action.viewmodelbase;

/**
 * The Definition Of View Object Parameter Actual Value Type
 *
 * @ClassName: ViewModelParActualValueType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ViewModelParActualValueType {
  Constant,//常量
  Expression, //表达式
  Variable;//vo变量

  public int getValue() {
    return this.ordinal();
  }

  public static ViewModelParActualValueType forValue(int value) {
    return values()[value];
  }
}
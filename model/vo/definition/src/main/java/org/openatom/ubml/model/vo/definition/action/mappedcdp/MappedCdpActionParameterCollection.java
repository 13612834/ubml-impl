package org.openatom.ubml.model.vo.definition.action.mappedcdp;

import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameterCollection;

/**
 * The Definition Of Mapped Component Action Parameter Collection
 *
 * @ClassName: MappedCdpActionParameterCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpActionParameterCollection extends ViewModelParameterCollection<MappedCdpActionParameter>
{
	public MappedCdpActionParameterCollection()
	{
	}
}
package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
/**
 * The Definition Of Data Reversal Mapping Action
 *
 * @ClassName: DataReversalMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DataReversalMappingAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "991bf216-f55b-40bf-bb42-1b831b6ef3e9";
	public static final String code = "DataReversalMapping";
	public static final String name = "内置数据反向Mapping操作";
	public DataReversalMappingAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}
package org.openatom.ubml.model.vo.definition.common;

/**
 * The Definition Of View  Model Source`s Metdata Type
 *
 * @ClassName: MetadataType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum MetadataType
{
	/** 
	 BE 
	 
	*/
	BizEntity,

	/** 
	 QO
	 
	*/
	QueryObject;

	public int getValue()
	{
		return this.ordinal();
	}

	public static MetadataType forValue(int value)
	{
		return values()[value];
	}
}
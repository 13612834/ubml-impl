package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
import org.openatom.ubml.model.vo.definition.action.ViewModelActionType;
import org.openatom.ubml.model.vo.definition.collection.VMActionCollection;
import lombok.var;
/**
 * The Josn Serializer Of View Model Action Collection
 *
 * @ClassName: VmActionCollectionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VmActionCollectionSerializer extends JsonSerializer<VMActionCollection> {

    @Override
    public void serialize(VMActionCollection value, JsonGenerator writer, SerializerProvider serializers) {

        if (value.size() == 0) {
            SerializerUtils.WriteStartArray(writer);
            SerializerUtils.WriteEndArray(writer);
            return;
        }
        SerializerUtils.WriteStartArray(writer);
        for (int i = 0; i < value.size(); i++) {
            ViewModelAction action = value.get(i);
            getActionConvertor(action.getType()).serialize(value.get(i), writer, null);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private VmActionSerializer getActionConvertor(ViewModelActionType type) {
        switch (type) {
            case BEAction:
                return new MappedBizActionSerializer();
            case VMAction:
                return new MappedCdpActionSerializer();
            case Custom:
            default:
                throw new RuntimeException("未定义'" + type + "'类型Action的JSON序列化器。");
        }
    }
}

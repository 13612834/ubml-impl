package org.openatom.ubml.model.vo.definition.action.internalexternalaction;
import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;

/**
 * The Definition Of Mutly Delete Action
 *
 * @ClassName: MultiDeleteAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MultiDeleteAction extends MappedCdpAction implements IInternalExtendAction{
    public static final String id = "7b1c3c4l-t1a4-4dyc-b75b-7695hcb3we7e";
    public static final String code= "MultiDelete";
    public static final String name = "内置批量删除操作";
    public MultiDeleteAction()
    {
        setID(id);
        setCode(code);
        setName(name);
    }
}

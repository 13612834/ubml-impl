package org.openatom.ubml.model.vo.definition.common;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.vo.definition.json.model.ValueHelpConfigDeserizlizer;
import org.openatom.ubml.model.vo.definition.json.model.ValueHelpConfigSerizlizer;

/**
 * The Definition Of Value Help Config
 *
 * @ClassName: ValueHelpConfig
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonDeserialize(using = ValueHelpConfigDeserizlizer.class)
@JsonSerialize(using = ValueHelpConfigSerizlizer.class)
public class ValueHelpConfig implements Cloneable, Serializable {

	//region 字段
    /**
     * VM元素ID
     */
    private String elementId;
    /**
     * 允许自定义帮助权限
     */
    private boolean enableCustomHelpAuth;
    /**
     * HelperID
     */
    private String helperId;
    /**
     * 帮助过滤条件表达式
     */
    private String filterExpression;

    /**
     * 帮助扩展动作
     */
    private HelpExtendAction helpExtendAction;

    private CustomizationInfo customizationInfo;

    //endregion


    ///#region 构造函数

    public ValueHelpConfig() {
    }

    /**
     * 构造函数
     *
     * @param elementId        VO元素ID
     * @param helperId         帮助元数据Id
     * @param filterExpression 过滤表达式
     */
    public ValueHelpConfig(String elementId, String helperId, String filterExpression) {
        this();
        setElementId(elementId);
        setHelperId(helperId);
        setFilterExpression(filterExpression);
    }

    ///#endregion

    ///#region 属性

    public final String getElementId() {
        return elementId;
    }

    public final void setElementId(String value) {
        elementId = value;
    }

    public final boolean getEnableCustomHelpAuth() {
        return enableCustomHelpAuth;
    }

    public final void setEnableCustomHelpAuth(boolean value) {
        enableCustomHelpAuth = value;
    }

    public final String getHelperId() {
        return helperId;
    }

    public final void setHelperId(String value) {
        helperId = value;
    }

    public final String getFilterExpression() {
        return filterExpression;
    }

    public final void setFilterExpression(String value) {
        filterExpression = value;
    }

    /**
     * 帮助扩展动作
     */
    public final HelpExtendAction getHelpExtend() {
        if (helpExtendAction == null) {
            helpExtendAction = new HelpExtendAction();
        }
        return helpExtendAction;
    }

    public final void setHelpExtend(HelpExtendAction value) {
        helpExtendAction = value;
    }

    public CustomizationInfo getCustomizationInfo() {
        return this.customizationInfo;
    }

    public void setCustomizationInfo(CustomizationInfo customizationInfo) {
        this.customizationInfo = customizationInfo;
    }

    ///#endregion

    ///#region 方法

    /**
     * 克隆
     *
     * @return 值帮助配置
     */
    public final ValueHelpConfig clone() {
//		return MemberwiseClone();
        Object tempVar = null;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        ValueHelpConfig obj = (ValueHelpConfig) ((tempVar instanceof ValueHelpConfig) ? tempVar : null);
        return obj;
    }

    ///#endregion
}
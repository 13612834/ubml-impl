/*
 *  Copyright 1999-2020 org.openatom.ubml Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.openatom.ubml.model.vo.definition.json.object;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonObject;
import org.openatom.ubml.model.common.definition.commonmodel.json.element.CmElementSerializer;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectSerializer;

/**
 * The type ViewObjectSerializer
 *
 * @author: haozhibei
 */
public class ViewObjectSerializer extends CmObjectSerializer {
    @Override
    protected CmElementSerializer gspCommonDataTypeSerializer() {
        return null;
    }

    @Override
    protected void writeExtendObjectBaseProperty(JsonGenerator writer, IGspCommonObject bizObject) {

    }

    @Override
    protected void writeExtendObjectSelfProperty(JsonGenerator writer, IGspCommonObject bizObject) {

    }
}

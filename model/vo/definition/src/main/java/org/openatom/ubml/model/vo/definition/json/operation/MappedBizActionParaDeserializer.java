package org.openatom.ubml.model.vo.definition.json.operation;

import org.openatom.ubml.model.vo.definition.action.mappedbiz.MappedBizActionParameter;

/**
 * The Json  Deserializer Of The View Model Action Parameter Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionParaDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionParaDeserializer extends VmParameterDeserializer<MappedBizActionParameter> {
    @Override
    protected MappedBizActionParameter createVmPara() {
        return new MappedBizActionParameter();
    }
}

package org.openatom.ubml.model.vo.definition.collection;

import java.io.Serializable;
import org.openatom.ubml.model.common.definition.cef.collection.BaseList;
import org.openatom.ubml.model.vo.definition.common.ValueHelpConfig;

/**
 * The Collection Of The Help Config
 *
 * @ClassName: ValueHelpConfigCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public  class ValueHelpConfigCollection extends BaseList<ValueHelpConfig> implements Cloneable, Serializable
{
		///#region 构造函数

	/** 
	 构造函数
	 
	*/
	public ValueHelpConfigCollection()
	{
		super();
	}

		///#endregion

		///#region 属性

	/** 
	 获取指定ID的值帮助配置
	 
	 @param elementId Element标识
	 @return 值帮助配置
	*/
	public final ValueHelpConfig getItem(String elementId)
	{
		for(ValueHelpConfig item:this){
		if(item.getElementId().equals(elementId)){
			return item;
		}
	}
		return null;
//		return Items.firstOrDefault(i => elementId.equals(i.ElementId));
	}

		///#endregion

		///#region 方法

	/** 
	 批量添加值帮助配置
	 
	 @param items 值帮助配置集合
	*/
	public final void addRange(ValueHelpConfig[] items)
	{
		for (ValueHelpConfig item : items)
		{
			add(item);
		}
	}

	/** 
	 批量添加值帮助配置
	 
	 @param items 值帮助配置集合
	*/
	public final void addRange(Iterable<ValueHelpConfig> items)
	{
		for (ValueHelpConfig item : items)
		{
			add(item);
		}
	}

	/** 
	 克隆
	 
	 @return 值帮助配置集合的副本
	*/
//	public final Object clone()
	@Override
	public final ValueHelpConfigCollection clone()
	{
		ValueHelpConfigCollection newCollection = new ValueHelpConfigCollection();

		for (ValueHelpConfig item : this)
		{
			add(item);
		}

		return newCollection;
	}

		///#endregion
}
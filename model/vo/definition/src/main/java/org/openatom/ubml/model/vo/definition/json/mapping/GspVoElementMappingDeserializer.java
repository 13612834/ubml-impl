package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementSourceType;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The Josn Deserializer Of View Model Element Mapping
 *
 * @ClassName: GspVoElementMappingDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMappingDeserializer extends ViewModelMappingDeserializer {
    @Override
    protected ViewModelMapping createVmMapping() {
        return new GspVoElementMapping();
    }

    @Override
    protected boolean readExtendMappingProperty(JsonParser reader, ViewModelMapping mapping, String propertyName) {
        boolean hasProperty = true;
        GspVoElementMapping voMapping = (GspVoElementMapping) mapping;
        switch (propertyName) {
            case ViewModelJsonConst.SourceType:
                voMapping.setSourceType(SerializerUtils
                    .readPropertyValue_Enum(reader, GspVoElementSourceType.class, GspVoElementSourceType.values(), GspVoElementSourceType.BeElement));
                break;
            case ViewModelJsonConst.TargetObjectId:
                voMapping.setTargetObjectId(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.TargetElementId:
                voMapping.setTargetElementId(SerializerUtils.readPropertyValue_String(reader));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}

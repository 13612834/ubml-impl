package org.openatom.ubml.model.vo.definition.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import org.openatom.ubml.model.vo.definition.action.mappedcdp.MappedCdpActionParameterCollection;
import org.openatom.ubml.model.vo.definition.json.operation.MappedCdpActionDeserializer;
import org.openatom.ubml.model.vo.definition.json.operation.MappedCdpActionSerializer;
import java.io.Serializable;

/**
 * The Definition Of The Parameter With Component
 *
 * @ClassName: MappedCdpAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedCdpActionSerializer.class)
@JsonDeserialize(using = MappedCdpActionDeserializer.class)
public class MappedCdpAction extends MappedCdpActionBase implements Cloneable, Serializable {
  ///#region 属性

  private MappedCdpActionParameterCollection mappedCdpActionParams;

  /**
   * 类型
   */
  @Override
  public ViewModelActionType getType() {
    return ViewModelActionType.VMAction;
  }

  ///#endregion
  public MappedCdpAction() {
    mappedCdpActionParams = new MappedCdpActionParameterCollection();
  }
  ///#region 方法

  /**
   * 克隆
   *
   * @return VO节点映射
   */
  @Override
  public MappedCdpAction clone() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedCdpAction.class);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected IViewModelParameterCollection getParameters() {
    return mappedCdpActionParams;
  }
  ///#endregion
}
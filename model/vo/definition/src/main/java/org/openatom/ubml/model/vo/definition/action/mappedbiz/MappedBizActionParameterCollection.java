package org.openatom.ubml.model.vo.definition.action.mappedbiz;

import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameterCollection;

/**
 * The Definition Of Mapped Be Action Parameter Collection
 *
 * @ClassName: MappedBizActionParameterCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public  class  MappedBizActionParameterCollection extends ViewModelParameterCollection<MappedBizActionParameter>
{
	public  MappedBizActionParameterCollection()
	{

	}
}
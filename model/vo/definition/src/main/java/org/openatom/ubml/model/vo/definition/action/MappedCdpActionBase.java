package org.openatom.ubml.model.vo.definition.action;

import java.io.Serializable;

/**
 * The Definition Of The Parameter With Component
 *
 * @ClassName: MappedCdpActionBase
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class MappedCdpActionBase extends ViewModelAction implements Serializable
{
		///#region 属性
	private String componentEntityId = "";
	/** 
	 对应构件实体Id
			
	*/
	public String getComponentEntityId(){
	 	return componentEntityId;
	 }
	 public void setComponentEntityId(String value){
	 	this.componentEntityId=value;
	 }
	/** 
	 对应构件实体包名
	 
	*/
	private String privateComponentPkgName;
	public String getComponentPkgName()
	{
		return privateComponentPkgName;
	}
	public void setComponentPkgName(String value)
	{
		privateComponentPkgName = value;
	}

	private boolean privateIsGenerateComponent;
	public boolean getIsGenerateComponent()
	{
		return privateIsGenerateComponent;
	}
	public void setIsGenerateComponent(boolean value)
	{
		privateIsGenerateComponent = value;
	}

		///#endregion

		///#region 方法

	/** 
	 当前对象是否等于同一类型的另一个对象。
	 
	 @param other 与此对象进行比较的对象。
	 @return 如果当前对象等于 other 参数，则为 true；否则为 false。
	*/
	@Override
	public boolean equals(ViewModelAction other)
	{

		MappedCdpActionBase cdpAction = (MappedCdpActionBase)((other instanceof MappedCdpActionBase) ? other : null);
		if (cdpAction == null)
		{
			return false;
		}
		if (getID().equals(cdpAction.getComponentEntityId()) && getCode().equals(cdpAction.getCode()) && getName().equals(cdpAction.getName()) && getType() == cdpAction.getType() && getComponentPkgName().equals(cdpAction.getComponentPkgName()) && getComponentName().equals(cdpAction.getComponentName()) && getIsGenerateComponent() == cdpAction.getIsGenerateComponent() && getComponentEntityId().equals(cdpAction.getComponentEntityId()))
		{
			return true;
		}

		return false;
	}

	/** 
	 重载ToString方法
	 
	 @return 描述
	*/
	@Override
	public String toString()
	{
		return String.format("[VM action] ID:%1$s, Code:%2$s, Name:%3$s", getID(), getCode(), getName());
	}

		///#endregion
}
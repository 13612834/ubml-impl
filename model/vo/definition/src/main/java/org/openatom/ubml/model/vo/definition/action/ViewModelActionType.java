package org.openatom.ubml.model.vo.definition.action;

/**
 * The Definition Of The View Model Action Type
 *
 * @ClassName: ViewModelActionType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ViewModelActionType
{
	/** 
	 BE 的业务操作
	 
	*/
	BEAction,

	/** 
	 VM 操作
	 
	*/
	VMAction,

	/** 
	 自定义操作
	 
	*/
	Custom;

	public int getValue()
	{
		return this.ordinal();
	}

	public static ViewModelActionType forValue(int value)
	{
		return values()[value];
	}
}
package org.openatom.ubml.model.vo.definition.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import org.openatom.ubml.model.vo.definition.action.mappedbiz.MappedBizActionParameterCollection;
import org.openatom.ubml.model.vo.definition.json.operation.MappedBizActionDeserializer;
import org.openatom.ubml.model.vo.definition.json.operation.MappedBizActionSerializer;
import java.io.Serializable;

/**
 * The Definition Of The Mapped Biz  Action
 *
 * @ClassName: MappedBizAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedBizActionSerializer.class)
@JsonDeserialize(using = MappedBizActionDeserializer.class)
public class MappedBizAction extends ViewModelAction implements Cloneable, Serializable {

  ///#region 属性
  private MappedBizActionParameterCollection mappedBizActionParams;
  /**
   * 类型
   */
  //@Override
  public ViewModelActionType Type = ViewModelActionType.BEAction;
//	public ViewModelActionType Type => ViewModelActionType.BEAction;

  ///#endregion
  public MappedBizAction() {
    mappedBizActionParams = new MappedBizActionParameterCollection();
  }
  ///#region 方法

  /**
   * 克隆
   *
   * @return VM节点映射
   */
  @Override
  public final MappedBizAction clone() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedBizAction.class);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected IViewModelParameterCollection getParameters() {
    return mappedBizActionParams;
  }
  ///#endregion
}
package org.openatom.ubml.model.vo.definition.collection;

import java.io.Serializable;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.commonmodel.collection.GspElementCollection;
import org.openatom.ubml.model.vo.definition.GspViewModelElement;
import org.openatom.ubml.model.vo.definition.GspViewObject;

/**
 * The Collection Of View Model Element
 *
 * @ClassName: VMElementCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VMElementCollection extends GspElementCollection implements Serializable
{
		///#region 构造函数

	/** 
	 字段集合构造函数
	 
	 @param owner 所属视图对象结点
	*/
//ORIGINAL LINE: public VMElementCollection(GspViewObject owner = null)
	public VMElementCollection(GspViewObject owner)
	{
		super(owner);
	}

		///#endregion

		///#region 属性

	/** 
	 根据ID获取节点元素
	 
	 @param id 节点元素ID
	 @return 节点元素
	*/
	@Override//不能override final的類
	public final GspViewModelElement getItem(String id)
	{
		for(IGspCommonField item:this){
			if(item instanceof GspViewModelElement){
				return (GspViewModelElement)item;
			}
		}
		return null;
//		Object tempVar = this.FirstOrDefault(i => id.equals(i.ID));
//		return (GspViewModelElement)((tempVar instanceof GspViewModelElement) ? tempVar : null);
	}

		///#endregion

		///#region 方法

	/** 
	 重载Equals方法
	 
	 @param obj 要比较的对象
	 @return 如果当前对象等于 other 参数，则为 true；否则为 false。
	*/
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		// if (obj.equals(this))
		//{
			//return true;
		//}
		if (obj.getClass() != getClass())
		{
			return false;
		}

		return equals((VMElementCollection)obj);
	}

	/** 
	 当前对象是否等于同一类型的另一个对象。
	 
	 @param other 与此对象进行比较的对象。
	 @return 如果当前对象等于 other 参数，则为 true；否则为 false。
	*/
	protected boolean equals(VMElementCollection other)
	{
//		if (Count != other.size())
		if (this.size() != other.size())
		{
			return false;
		}
		for (IGspCommonField item : this)
		{
			IGspCommonField otherItem = other.getItem(item.getID());
			if (otherItem == null)
			{
				return false;
			}
			if (!item.equals(otherItem))
			{
				return false;
			}
		}

		return true;
	}

	/** 
	 克隆
	 
	 @return VO节点元素集合
	*/
	public final VMElementCollection clone()
	{
		VMElementCollection collections;
		collections = new VMElementCollection((GspViewObject) getParentObject());

//		collections.addAll(this);
//		(this.Select(element => (GspViewModelElement)((tempVar instanceof GspViewModelElement) ?
//				tempVar : null)));
		for(IGspCommonField item:this){
//			Object item;
			GspViewModelElement obj = ((GspViewModelElement)item).clone();
			GspViewModelElement tempVar = ((obj instanceof GspViewModelElement) ? obj : null);
			collections.add(tempVar);
		}
		return collections;
	}
}
package org.openatom.ubml.model.vo.definition.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import lombok.var;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonModel;
import org.openatom.ubml.model.common.definition.commonmodel.json.model.CommonModelSerializer;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectSerializer;
import org.openatom.ubml.model.vo.definition.GspViewModel;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
import org.openatom.ubml.model.vo.definition.json.mapping.ViewModelMappingSerializer;
import org.openatom.ubml.model.vo.definition.json.object.ViewObjectSerializer;
import org.openatom.ubml.model.vo.definition.json.operation.VmActionCollectionSerializer;

/**
 * The Josn Serializer Of View Model Definition
 *
 * @ClassName: ViewModelSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelSerializer extends CommonModelSerializer {


    //region BaseProp
    @Override
    protected void writeExtendModelProperty(IGspCommonModel commonModel, JsonGenerator writer) {
        GspViewModel vm = (GspViewModel) commonModel;
        writeAutoMergeMessage(writer, vm);
        writeVMActions(writer, vm);
        writeExtendModelProperty(writer, vm);
    }

    private void writeAutoMergeMessage(JsonGenerator writer, GspViewModel vm) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ConvertMsg);
        SerializerUtils.writePropertyValue_boolean(writer, vm.getAutoConvertMessage());
    }

    private void writeVMActions(JsonGenerator writer, GspViewModel vm) {
        if (vm.getActions() == null || vm.getActions().size() < 1)
            return;
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Actions);
        getVMActionCollectionConvertor().serialize(vm.getActions(), writer, null);
    }

    private VmActionCollectionSerializer getVMActionCollectionConvertor() {
        return new VmActionCollectionSerializer();
    }

    private void writeExtendModelProperty(JsonGenerator writer, GspViewModel vm) {
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.EnableStdTimeFormat, vm.getEnableStdTimeFormat());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.Description, vm.getDescription());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ExtendType, vm.getExtendType());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TemplateVoInfo, vm.getTemplateVoInfo());
        writeMapping(writer, vm);
        writeValueHelpConfigs(writer, vm);
        writeExtendProperties(writer, vm);
        writeVoDataExtendInfo(writer, vm);
    }

    private void writeMapping(JsonGenerator writer, GspViewModel vm) {
        if (vm.getMapping() == null) {
            return;
        }
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Mapping);
        ViewModelMappingSerializer convertor = new ViewModelMappingSerializer();
        convertor.serialize(vm.getMapping(), writer, null);
    }

    private void writeValueHelpConfigs(JsonGenerator writer, GspViewModel vm) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ValueHelpConfigs);
        SerializerUtils.writeArray(writer,new ValueHelpConfigSerizlizer(), vm.getValueHelpConfigs());

    }


    private void writeExtendProperties(JsonGenerator writer, GspViewModel vm) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ExtendProperties);
        var dic = vm.getExtendProperties();
        SerializerUtils.writeStartObject(writer);
        if (dic != null && dic.size() > 0) {
            for (var item : dic.entrySet()) {
                SerializerUtils.writePropertyValue(writer, item.getKey(), item.getValue());
            }
        }
        SerializerUtils.writeEndObject(writer);
    }

    private void writeVoDataExtendInfo(JsonGenerator writer, GspViewModel vm) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.DataExtendInfo);
        var converter = new VoDataExtendInfoSerializer();
        converter.serialize(vm.getDataExtendInfo(), writer, null);
    }


    //endregion

    //region SelfProp
    @Override
    protected void writeExtendModelSelfProperty(IGspCommonModel commonModel, JsonGenerator writer) {

    }

    //endregion
    @Override
    protected CmObjectSerializer getCmObjectSerializer() {
        return new ViewObjectSerializer();
    }
}

package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;

/**
 * The Definition Of Changeset Reversal Mapping Action
 *
 * @ClassName: ChangesetReversalMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ChangesetReversalMappingAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "301c5991-a32d-4221-88bf-8c9d07bdd884";
	public static final String code = "ChangesetReversalMapping";
	public static final String name = "内置变更集反向Mapping操作";
	public ChangesetReversalMappingAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}
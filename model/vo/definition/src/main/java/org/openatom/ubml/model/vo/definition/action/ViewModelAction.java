package org.openatom.ubml.model.vo.definition.action;

import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelReturnValue;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import java.io.Serializable;

/**
 * The Definition Of The View Model Action
 *
 * @ClassName: ViewModelAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class ViewModelAction implements Cloneable, Serializable {
  ///#region 属性

  /**
   * 标识
   */
  private String privateID;

  public String getID() {
    return privateID;
  }

  public void setID(String value) {
    privateID = value;
  }

  /**
   * 编号
   */
  private String privateCode;

  public String getCode() {
    return privateCode;
  }

  public void setCode(String value) {
    privateCode = value;
  }

  /**
   * 名称
   */
  private String privateName;

  public String getName() {
    return privateName;
  }

  public void setName(String value) {
    privateName = value;
  }

  /**
   * 类型
   */
  private ViewModelActionType privateType = ViewModelActionType.forValue(0);

  public ViewModelActionType getType() {
    return privateType;
  }

  public void setType(ViewModelActionType value) {
    privateType = value;
  }

  /**
   * 参数列表
   */
  public IViewModelParameterCollection getParameterCollection() {
    return getParameters();
  }

  /**
   * 返回值
   */
  private ViewModelReturnValue privateReturnValue;

  public final ViewModelReturnValue getReturnValue() {
    return privateReturnValue;
  }

  public final void setReturnValue(ViewModelReturnValue value) {
    privateReturnValue = value;
  }

  protected abstract IViewModelParameterCollection getParameters();

  /**
   * 操作映射
   */
  private ViewModelMapping privateMapping;

  public final ViewModelMapping getMapping() {
    return privateMapping;
  }

  public final void setMapping(ViewModelMapping value) {
    privateMapping = value;
  }

  /**
   * 对应构件名
   */
  private String privateComponentName;

  public final String getComponentName() {
    return privateComponentName;
  }

  public final void setComponentName(String value) {
    privateComponentName = value;
  }

  private java.util.HashMap<String, String> extendProperties;

  /**
   * 表单拓展节点
   */
  public final java.util.HashMap<String, String> getExtendProperties() {
    if (extendProperties == null) {
      extendProperties = new java.util.HashMap<String, String>();
    }
    return extendProperties;
  }

  public void setExtendProperties(java.util.HashMap<String, String> value) {
    extendProperties = value;
  }

  private boolean privateIsAutoSave;

  public final boolean getIsAutoSave() {
    return privateIsAutoSave;
  }

  public final void setIsAutoSave(boolean value) {
    privateIsAutoSave = value;
  }

  private CustomizationInfo customizationInfo = new CustomizationInfo();
  public CustomizationInfo getCustomizationInfo() {
    return this.customizationInfo;
  }

  public void setCustomizationInfo(CustomizationInfo customizationInfo) {
    this.customizationInfo = customizationInfo;
  }
  ///#endregion

  ///#region 方法

  /**
   * 重写Clone
   */
  public abstract ViewModelAction clone();

  /**
   * 重写相等判断
   */
  @Override
  public boolean equals(Object obj) {
    if (obj.equals(null)) {
      return false;
    }
    if (obj.equals(this)) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    return equals((ViewModelAction) obj);
  }

  /**
   * 强类型相等判断
   */
  public boolean equals(ViewModelAction other) {

    if (other == null) {
      return false;
    }

    if (getID().equals(other.getID()) && getCode().equals(other.getCode()) && getName()
        .equals(other.getName()) && getType() == other.getType() && getMapping()
        .equals(other.getMapping()) && getComponentName().equals(other.getComponentName())) {
      return true;
    }

    return false;
  }

  /**
   * 重写HashCode
   */
  @Override
  public int hashCode() {
//		unchecked
    {
      int hashCode = (getID() != null ? getID().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (getCode() != null ? getCode().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (getName() != null ? getName().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (getReturnValue() != null ? getReturnValue().hashCode() : 0);
      hashCode = (hashCode * 397) ^ getType().getValue();
      hashCode =
          (hashCode * 397) ^ (getComponentName() != null ? getComponentName().hashCode() : 0);
      hashCode = (hashCode * 397) ^ (getMapping() != null ? getMapping().hashCode() : 0);

      return hashCode;
    }
  }

  /**
   * 重载ToString方法
   *
   * @return 描述
   */
  @Override
  public String toString() {
    return String
        .format("[VM BizAction] ID:%1$s,Code:%2$s,Name:%3$s", getID(), getCode(), getName());
  }
  ///#endregion
}
package org.openatom.ubml.model.vo.definition.action.viewmodelbase;


import org.openatom.ubml.model.vo.definition.action.IViewModelParameter;
import org.openatom.ubml.model.vo.definition.common.VMCollectionParameterType;
import org.openatom.ubml.model.vo.definition.common.VMParameterMode;
import org.openatom.ubml.model.vo.definition.common.VMParameterType;

/**
 * The Definition Of View Model Parameter
 *
 * @ClassName: ViewModelParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class ViewModelParameter implements IViewModelParameter {
    private String privateID;

    public String getID() {
        return privateID;
    }

    public void setID(String value) {
        privateID = value;
    }

    /**
     * 参数名
     */
    private String privateParamCode;

    public String getParamCode() {
        return privateParamCode;
    }

    public void setParamCode(String value) {
        privateParamCode = value;
    }

    /**
     * 参数名
     */
    private String privateParamName;

    public String getParamName() {
        return privateParamName;
    }

    public void setParamName(String value) {
        privateParamName = value;
    }

    /**
     * 参数类型，与Assembly和ClassName关联
     */
    private VMParameterType privateParameterType = VMParameterType.forValue(0);

    public VMParameterType getParameterType() {
        return privateParameterType;
    }

    public void setParameterType(VMParameterType value) {
        privateParameterType = value;
    }

    /**
     * 参数类对应程序集类
     */
    private String privateAssembly;

    public String getAssembly() {
        return privateAssembly;
    }

    public void setAssembly(String value) {
        privateAssembly = value;
    }

    /**
     * 参数类名
     */
    private String privateClassName;

    public String getClassName() {
        return privateClassName;
    }

    public void setClassName(String value) {
        privateClassName = value;
    }

    public String getDotnetClassName() {
        return dotnetClassName;
    }

    public void setDotnetClassName(String dotnetClassName) {
        this.dotnetClassName = dotnetClassName;
    }

    private String dotnetClassName;

    /**
     * 参数模式
     */
    private VMParameterMode privateMode = VMParameterMode.forValue(0);

    public VMParameterMode getMode() {
        return privateMode;
    }

    public void setMode(VMParameterMode value) {
        privateMode = value;
    }

    /**
     * 描述
     */
    private String privateParamDescription;

    public String getParamDescription() {
        return privateParamDescription;
    }

    public void setParamDescription(String value) {
        privateParamDescription = value;
    }

    /**
     * 参数集合类型
     */
    private VMCollectionParameterType privateCollectionParameterType = VMCollectionParameterType.forValue(0);

    public VMCollectionParameterType getCollectionParameterType() {
        return privateCollectionParameterType;
    }

    public void setCollectionParameterType(VMCollectionParameterType value) {
        privateCollectionParameterType = value;
    }

    private ViewModelParActualValue actualValue;
    public ViewModelParActualValue getActualValue(){
        if(actualValue == null){
            actualValue = new ViewModelParActualValue();
        }
        return actualValue;
    }

    public void setActualValue(ViewModelParActualValue value){
        actualValue = value;
    }
}
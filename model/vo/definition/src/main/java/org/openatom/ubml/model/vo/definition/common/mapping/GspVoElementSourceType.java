package org.openatom.ubml.model.vo.definition.common.mapping;

/**
 * The Definition Of View  Model Element Source Type
 *
 * @ClassName: GspVoElementSourceType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspVoElementSourceType
{
	/** 
	 BE 
	 
	*/
	BeElement,

	/** 
	 QO
	 
	*/
	QoElement,

	/** 
	 VO
	 
	*/
	VoElement;

	public int getValue()
	{
		return this.ordinal();
	}

	public static GspVoElementSourceType forValue(int value)
	{
		return values()[value];
	}
}
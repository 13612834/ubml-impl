package org.openatom.ubml.model.vo.definition.action;

import java.io.Serializable;

/**
 * The Definition Of Action Format Parameter
 *
 * @ClassName: ActionFormatParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ActionFormatParameter implements Cloneable, Serializable
{
		///#region 属性

	/** 
	 ID
	 
	*/
	private String privateID;
	public final String getID()
	{

		return privateID;
	}
	public final void setID(String value)
	{

		privateID = value;
	}

	/** 
	 Code
	 
	*/
	private String privateCode;
	public final String getCode()
	{
		return privateCode;
	}
	public final void setCode(String value)
	{
		privateCode = value;
	}

	/** 
	 Name
	 
	*/
	private String privateName;
	public final String getName()
	{
		return privateName;
	}
	public final void setName(String value)
	{
		privateName = value;
	}

		///#endregion

		///#region 方法

	/** 
	 克隆
	 
	 @return Action执行参数
	*/
	public final ActionFormatParameter clone()
	{
		Object tempVar = null;
		try {
			tempVar = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		ActionFormatParameter obj = (ActionFormatParameter)((tempVar instanceof ActionFormatParameter) ? tempVar : null);

		return obj;
	}

		///#endregion
}
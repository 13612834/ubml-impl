package org.openatom.ubml.model.vo.definition.common.mapping;

import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;

/**
 * The Definition Of View Model ELement Mapping
 *
 * @ClassName: GspVoElementMapping
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMapping extends ViewModelMapping
{
	/** 
	 映射节点ID
	 
	*/
	private String targetObjectId;
	public final String getTargetObjectId()
	{
		return targetObjectId;
	}
	public final void setTargetObjectId(String value)
	{
		targetObjectId = value;
	}

	/** 
	 映射字段ID
	 
	*/
	private String targetElementId;
	public final String getTargetElementId()
	{
		return targetElementId;
	}
	public final void setTargetElementId(String value)
	{
		targetElementId = value;
	}

	/** 
	 数据源（be或qo）
	 
	*/
	private GspVoElementSourceType sourceType =
			GspVoElementSourceType.forValue(0);
	public final GspVoElementSourceType getSourceType()
	{
		return sourceType;
	}
	public final void setSourceType(GspVoElementSourceType value)
	{
		sourceType = value;
	}

	public GspVoElementMapping clone() {
		return (GspVoElementMapping)super.clone();
	}

}
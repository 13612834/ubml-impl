package org.openatom.ubml.model.vo.definition.collection;

import java.io.Serializable;
import org.openatom.ubml.model.common.definition.cef.collection.BaseList;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;

/**
 * The Collection Of View Model Action
 *
 * @ClassName: VMActionCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VMActionCollection extends BaseList<ViewModelAction> implements Cloneable,
        Serializable {
    ///#region 属性

    /**
     * 根据ID获取操作
     *
     * @param id 操作ID
     * @return 操作
     */
    public final ViewModelAction getItem(String id) {
//		return this.FirstOrDefault(i => id.equals(i.ID));
        for (ViewModelAction item : this) {
            if (item.getID().equals(id)) {
                return item;
            }
        }
        return null;
    }

    ///#endregion

    ///#region 方法

    /**
     * 克隆
     *
     * @return VO上的操作集合
     */
//	public final Object clone()
    public final VMActionCollection clone() {
        VMActionCollection collections = new VMActionCollection();
        for (ViewModelAction op : this) {
            ViewModelAction tempVar = op.clone();
            collections.add((ViewModelAction) ((tempVar instanceof ViewModelAction) ? tempVar : null));
        }

        return collections;
    }

    /**
     * 重载Equals方法
     *
     * @param obj 要比较的对象
     * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        // if (obj.equals(this))
        //{
        //return true;
        //}
        if (obj.getClass() != getClass()) {
            return false;
        }

        return equals((VMActionCollection) obj);
    }

    /**
     * 当前对象是否等于同一类型的另一个对象。
     *
     * @param other 与此对象进行比较的对象。
     * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    protected boolean equals(VMActionCollection other) {
//		if (Count != other.size())
        if (this.size() != other.size()) {
            return false;
        }
        for (ViewModelAction item : this) {
            ViewModelAction otherItem = other.getItem(item.getID());
            if (otherItem == null) {
                return false;
            }
            if (!item.equals(otherItem)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean remove(Object action) {
        if (action == null) {
            return false;
        }
        String id = ((ViewModelAction) action).getID();
        if (id == null) {
            return false;
        }
        super.removeIf(item ->
                id.equals(item.getID())
        );
        return true;
    }

    public boolean removeById(String actionId) {

        if (actionId == null || "".equals(actionId)) {
            return false;
        }
        super.removeIf(item ->
                actionId.equals(item.getID())
        );
        return true;
    }
    ///// <summary>
    ///// 设置Action集合所属的结点
    ///// </summary>
    ///// <param name="node"></param>
    //public void SetOwner(GspViewObject node)
    //{
    //    foreach (var item in this)
    //    {
    //        item.Owner = node;
    //    }
    //}
    ///#endregion
}
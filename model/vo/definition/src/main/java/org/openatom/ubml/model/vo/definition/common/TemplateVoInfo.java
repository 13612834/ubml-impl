package org.openatom.ubml.model.vo.definition.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The Definition Of View Model Template Info
 *
 * @ClassName: TemplateVoInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class TemplateVoInfo {

	private String templateVoServiceUnit;

	/**
	 * 模板VO信息
	 */
	@JsonProperty(ViewModelJsonConst.TemplateVoServiceUnit)
	public final String getTemplateVoServiceUnit() {
		return templateVoServiceUnit;
	}

	public void setTemplateVoServiceUnit(String value) {
		this.templateVoServiceUnit = value;
	}

	private String templateVoPkgName;

	/**
	 * 模板VO信息
	 */
	@JsonProperty(ViewModelJsonConst.TemplateVoPkgName)
	public final String getTemplateVoPkgName() {
		return templateVoPkgName;
	}

	public void setTemplateVoPkgName(String value) {
		this.templateVoPkgName = value;
	}

	private String templateVoId;

	/**
	 * 模板VO信息
	 */
	@JsonProperty(ViewModelJsonConst.TemplateVoId)
	public final String getTemplateVoId() {
		return templateVoId;
	}

	public void setTemplateVoId(String value) {
		this.templateVoId = value;
	}
}

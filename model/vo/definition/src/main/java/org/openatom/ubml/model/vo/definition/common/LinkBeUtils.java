//package org.openatom.ubml.model.vo.definition.common;
//
//import com.inspur.edp.bef.bizentity.GspBizEntityElement;
//import com.inspur.edp.bef.bizentity.GspBizEntityObject;
//import com.inspur.edp.bef.bizentity.GspBusinessEntity;
//import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
//import com.inspur.edp.cef.designtime.api.IGspCommonField;
//import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
//import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
//import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
//import com.inspur.edp.cef.designtime.api.element.GspAssociation;
//import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
//import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
//import com.inspur.edp.cef.designtime.api.entity.DynamicPropSetInfo;
//import com.inspur.edp.das.commonmodel.IGspCommonElement;
//import com.inspur.edp.das.commonmodel.IGspCommonObject;
//import org.openatom.ubml.model.vo.definition.collection.VMActionCollection;
//import org.openatom.ubml.model.vo.definition.collection.ViewObjectCollection;
//import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementMapping;
//import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementSourceType;
//import org.openatom.ubml.model.vo.definition.common.mapping.GspVoObjectSourceType;
//import org.openatom.ubml.model.vo.definition.GspViewModelElement;
//import org.openatom.ubml.model.vo.definition.action.MappedBizAction;
//import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
//import org.openatom.ubml.model.vo.definition.action.ViewModelActionType;
//import org.openatom.ubml.model.vo.definition.DotNetToJavaStringHelper;
//import org.openatom.ubml.model.vo.definition.GspViewModel;
//import org.openatom.ubml.model.vo.definition.GspViewObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
///**
// * The Tool Of Link Business Entity By View Model
// *
// * @ClassName: LinkBeUtils
// * @Author: Benjamin Gong
// * @Date: 2021/1/11 17:13
// * @Version: V1.0
// */
//public class LinkBeUtils {
//	private final java.util.HashMap<String, GspBusinessEntity> bizEntitiesDic = new java.util.HashMap<String,
//			GspBusinessEntity>();
//	//	private IRefCommonService lcmDtService;
////	private IRefCommonService LcmDtService => (lcmDtService != null) ? lcmDtService : (lcmDtService = ServiceManager.<IRefCommonService>getService());
//	private String errorToken = "#GspBefError# ";
//
//	public final void linkWithBe(GspViewModel vm) {
//		if (vm.getMainObject().getMapping() == null || vm.getMainObject().getMapping().getSourceType() != GspVoObjectSourceType.BeObject) {
//			return;
//		}
//		// 联动前清空缓存
//		bizEntitiesDic.clear();
//		// ① 对象
//		linkMainObj(vm.getMainObject());
//		// ② 操作
//		linkActions(vm);
//		// 带出字段为枚举时，赋值枚举信息
//		linkBeRefElements(vm);
//	}
//
//	/**
//	 * 关联带出字段联动
//	 *
//	 * @param vm
//	 */
//	public final void linkBeRefElements(GspViewModel vm) {
//		ArrayList<IGspCommonElement> eles = vm.getAllElementList(true);
//		for (IGspCommonElement ele : eles) {
//			if (ele.getIsVirtual() || !ele.getIsRefElement()) {
//				continue;
//			}
//			GspViewModelElement viewEle = (GspViewModelElement) ele;
//			if (ele != null) {
//				if (viewEle.getMapping() == null) {
//					throw new RuntimeException(String.format("字段Mapping为空，vo名称=%1$s,vo编号=%2$s,字段名称=%3$s", vm.getName(), vm.getCode()
//							, viewEle.getName()));
//				}
//				if (viewEle.getMapping().getSourceType() != GspVoElementSourceType.BeElement) {
//					continue;
//				}
//				if (viewEle.getParentAssociation() == null) {
//					throw new RuntimeException(String.format("字段所属关联为空，vo名称=%1$s,vo编号=%2$s,字段名称=%3$s", vm.getName(), vm.getCode()
//							, viewEle.getName()));
//				}
//				GspBizEntityElement bizEle = getRefBizElement(viewEle.getParentAssociation().getRefModelID(), viewEle.getRefElementId());
//				// udt
//				viewEle.setIsUdt(bizEle.getIsUdt());
//				viewEle.setUdtID(bizEle.getUdtID());
//				viewEle.setUdtName(bizEle.getUdtName());
//				viewEle.setUdtPkgName(bizEle.getUdtPkgName());
//				if (!ele.getIsRefElement() || ele.getObjectType() != GspElementObjectType.Enum) {
//					continue;
//				}
//
//				if (bizEle.getObjectType() != GspElementObjectType.Enum) {
//					throw new RuntimeException(errorToken + "关联带出字段对象类型为枚举，其对应的关联模型上的该字段不是枚举类型。" + errorToken);
//				}
//				// 赋值枚举信息
//				viewEle.setContainEnumValues(bizEle.getContainEnumValues());
//			}
//		}
//	}
//
////C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
//	//#region 对象
//
//	private void linkMainObj(GspViewObject viewObj) {
//		// 虚拟对象无映射bizObj
//		if (viewObj == null || viewObj.getIsVirtual()) {
//			throw new RuntimeException(errorToken + "主对象不可为空或虚拟对象。" + errorToken);
//		}
//		GspBizEntityObject bizObject = getBizObject(viewObj.getMapping(), viewObj.getCode());
//		if (bizObject == null) {
//			throw new RuntimeException(errorToken + "实体中无vo映射的主对象。" + errorToken);
//		}
//
//		linkBizObject(viewObj, bizObject);
//	}
//
//
//	private void linkBizObject(GspViewObject viewObj, GspBizEntityObject bizObject) {
//		// ① 基本信息
//		linkObjectBasicInfo(viewObj, bizObject);
//		// ② 字段集合
//		linkElements(viewObj);
//		// ③ 字段相关属性
//		linkObjectSelfInfo(viewObj, bizObject, viewObj.getParentObject().getIDElement().getID());
//		// ④ 子对象集合
//		if (viewObj.getContainChildObjects() != null && viewObj.getContainChildObjects().size() != 0) {
//			ViewObjectCollection childObjList = viewObj.getContainChildObjects();
//			for (IGspCommonObject childObj : childObjList) {
//				if (childObj.getIsVirtual()) {
//					continue;
//				}
//				GspBizEntityObject childBizObject = getBizObject(((GspViewObject) childObj).getMapping(), childObj.getCode());
//				if (childBizObject == null) {
//					viewObj.getContainChildObjects().remove(childObj);
//					continue;
//				}
//				linkBizObject((GspViewObject) ((GspViewObject) childObj != null ? childObj : null), childBizObject);
//			}
//		}
//	}
//
//	private void linkObjectBasicInfo(GspViewObject viewObj, GspBizEntityObject bizObject) {
//		// 若vo节点未修改，则联动be节点编号
//		if (DotNetToJavaStringHelper.isNullOrEmpty(viewObj.getCode())) {
//			viewObj.setCode(bizObject.getCode());
//		}
//		viewObj.setIsVirtual(false);
//		viewObj.setIsReadOnly(bizObject.getIsReadOnly());
//	}
//
//	private void linkObjectSelfInfo(GspViewObject viewObj, GspBizEntityObject bizObject, String parentObjectElementId) {
//		ArrayList<IGspCommonField> elementList = viewObj.getContainElements().getAllItems(item -> item.getIsVirtual() == false);
//		HashMap<String, IGspCommonField> viewElements = new HashMap<String, IGspCommonField>();
//		for (IGspCommonField item : elementList) {
//			viewElements.put(item.getID(), (GspViewModelElement) item);
//		}
//		// 字段mapping字典
//		java.util.HashMap<String, String> elementMappings = ConvertUtils.getElementMappingsDic(elementList);
//
//		// ② 分级信息
////        ConvertUtils.UpdateHirarchyInfo(bizObject, viewObj, elementMappings, viewElements);
//		// ③ 时间戳相关
////        ConvertUtils.UpdateTimeStampElements(bizObject, viewObj, elementMappings);
//		// ④ID生成规则
//		ConvertUtils.updateColumnGenerateId(bizObject, viewObj, elementMappings);
//		// ⑤唯一性约束
//		ConvertUtils.updateContainConstraints(bizObject, viewObj, elementMappings, viewElements);
//		//// ⑥关联信息-vo多源后，主子表关联不需要联动
//		//ConvertUtils.UpdateViewObjectKeys(bizObject, viewObj, parentObjectElementId);
//	}
//
//	//#endregion
//
//	//#region 字段
//
//	private void linkElements(GspViewObject viewObj) {
//		ArrayList<IGspCommonField> elementList = viewObj.getContainElements().getAllItems(item -> item.getIsVirtual() == false);
//
//		// 字段mapping字典
//		if (elementList.size() == 0) {
//			throw new RuntimeException(errorToken + String.format("当前对象无字段'%1$s'。", viewObj.getName()) + errorToken);
//		}
//
//		for (IGspCommonField ele : elementList) {
//			GspViewModelElement viewEle = (GspViewModelElement) ele;
//			if (viewEle != null) {
//				GspBizEntityElement bizElement = getBizElement(viewEle.getMapping(), viewEle.getLabelID());
//				if (bizElement == null) {
//					viewObj.getContainElements().remove(ele);
//					continue;
//				}
//				linkBizElement(viewEle, bizElement);
//				// 兼容旧版mapping的be映射字段
//				if (DotNetToJavaStringHelper.isNullOrEmpty(viewEle.getMapping().getTargetElementId())) {
//					viewEle.getMapping().setTargetElementId(bizElement.getID());
//				}
//				if (DotNetToJavaStringHelper.isNullOrEmpty(viewEle.getMapping().getTargetObjectId()) || viewEle.getMapping().getTargetObjectId() != bizElement.getBelongObject().getID()) {
//					viewEle.getMapping().setTargetObjectId(bizElement.getBelongObject().getID());
//				}
//			}
//		}
//	}
//
//	private void linkBizElement(GspViewModelElement viewEle, GspBizEntityElement bizEle) {
//		linkElementBasicInfo(viewEle, bizEle);
//		linkElementObjectType(viewEle, bizEle);
//	}
//
//	private void linkElementBasicInfo(GspViewModelElement ele, GspBizEntityElement bizElement) {
//		boolean canEditLengthAndPrecision = true;
//		ele.setBillCodeConfig(bizElement.getBillCodeConfig().clone());
//		ele.setLabelID(bizElement.getLabelID());
//		if (ele.getMDataType() != bizElement.getMDataType()) {
//			ele.setMDataType(bizElement.getMDataType());
//			canEditLengthAndPrecision = false;
//		}
//		ele.setIsMultiLanguage(bizElement.getIsMultiLanguage());
//
//		if (bizElement.getIsRequire()) {
//			ele.setIsRequire(bizElement.getIsRequire());
//		}
//		if (bizElement.getReadonly()) {
//			ele.setReadonly(bizElement.getReadonly());
//		}
//
//		// udt相关
//		ele.setIsUdt(bizElement.getIsUdt());
//		ele.setUdtID(bizElement.getUdtID());
//		ele.setUdtName(bizElement.getUdtName());
//		ele.setUdtPkgName(bizElement.getUdtPkgName());
//		if (bizElement.getIsUdt()) {
//			canEditLengthAndPrecision = false;
//		} else {
//			canEditLengthAndPrecision = true;
//		}
//
//		// 长度精度
//		linkLengthAndPrecision(ele, bizElement, canEditLengthAndPrecision);
//	}
//
//	/**
//	 * 更新字段长度精度
//	 *
//	 * @param ele
//	 * @param bizElement
//	 * @param canEdit
//	 */
//	private void linkLengthAndPrecision(GspViewModelElement ele, GspBizEntityElement bizElement, boolean canEdit) {
//		if (canEdit) {
//			// 长度精度不可大于be字段
//			if (ele.getLength() > bizElement.getLength()) {
//				ele.setLength(bizElement.getLength());
//			}
//			if (ele.getPrecision() > bizElement.getPrecision()) {
//				ele.setPrecision(bizElement.getPrecision());
//			}
//		} else {
//			ele.setLength(bizElement.getLength());
//			ele.setPrecision(bizElement.getPrecision());
//		}
//	}
//
//	private void linkElementObjectType(GspViewModelElement ele, GspBizEntityElement bizElement) {
//		GspViewModelElement transEle = ConvertUtils.toElement(bizElement, null, ele.getMapping().getTargetMetadataId());
//		ele.setObjectType(bizElement.getObjectType());
//		switch (bizElement.getObjectType()) {
//			case Association:
//				linkElementAssos(ele, transEle);
//				break;
//			case Enum:
//				linkElementEnums(ele, transEle);
//				break;
//			case None:
//				break;
//			case DynamicProp:
//				ele.setDynamicPropSetInfo(bizElement.getDynamicPropSetInfo());
//				break;
//			default:
//				throw new RuntimeException(errorToken + String.format("字段对象类型'{%1$s}'不存在。", bizElement.getObjectType()) + errorToken);
//		}
//		if (ele.getObjectType() != GspElementObjectType.Association) {
//			ele.getChildAssociations().clear();
//		}
//		if (ele.getObjectType() != GspElementObjectType.Enum) {
//			ele.getContainEnumValues().clear();
//		}
//		if (ele.getObjectType() != GspElementObjectType.DynamicProp) {
//			ele.setDynamicPropSetInfo(new DynamicPropSetInfo());
//		}
//
//	}
//
//	private void linkElementEnums(GspViewModelElement ele, GspViewModelElement transElement) {
//		Object tempVar = ele.getContainEnumValues().clone();
//		GspEnumValueCollection voEnumValues = (GspEnumValueCollection) ((GspEnumValueCollection) tempVar != null ? tempVar : null);
//		ele.getContainEnumValues().clear();
//
//		for (GspEnumValue enumItem : transElement.getContainEnumValues()) {
//			Object tempVar2 = enumItem.clone();
//			GspEnumValue enumValue = (GspEnumValue) ((GspEnumValue) tempVar2 != null ? tempVar2 : null);
//			if (voEnumValues != null && voEnumValues.size() > 0) {
//				for (GspEnumValue voEnumValue : voEnumValues) {
//					if (voEnumValue.getValue() == enumValue.getValue()) {
//						enumValue.setI18nResourceInfoPrefix(voEnumValue.getI18nResourceInfoPrefix());
//					}
//				}
//			}
//			ele.getContainEnumValues().add(enumValue);
//		}
//	}
//
//	private void linkElementAssos(GspViewModelElement ele, GspViewModelElement transElement) {
//		GspAssociationCollection currentAssos = ele.getChildAssociations().clone(ele);
//		if (ele.getChildAssociations() != null && ele.getChildAssociations().size() > 0) {
//			ele.getChildAssociations().clear();
//		}
//		if (transElement.getChildAssociations() == null || transElement.getChildAssociations().size() == 0) {
//			throw new RuntimeException(errorToken + String.format("当前字段'%1$s'，对象类型为关联，却无关联信息，请联系管理员。", ele.getName()) + errorToken);
//		}
//		// 目前策略：关联信息全部带出
//		// 若关联模型未改变，则关联只处理带出字段
//		for (GspAssociation transAssociation : transElement.getChildAssociations()) {
//			transAssociation.setBelongElement(ele);
//			GspAssociation currentAsso = getAssociation(currentAssos, transAssociation.getId());
//			if (currentAsso != null && currentAsso.getRefModelID().equals(transAssociation.getRefModelID())) {
//				handleRefElementCollection(transAssociation, currentAsso);
//				ele.getChildAssociations().add(currentAsso);
//			} else {
//				ele.getChildAssociations().add(transAssociation);
//			}
//		}
//
//		//todo:未来可支持单条Asso的更新
//		//foreach (GspAssociation associationItem in ele.getChildAssociations())
//		//{
//		//	GspAssociation transAssociation = getAssociation(transElement.getChildAssociations(),
//		//		associationItem.getID());
//		//	if (transAssociation == null)
//		//		throw new Exception(errorToken+"关联未找到，请联系管理员。"+errorToken);
//		//	associationItem.AssSendMessage = transAssociation.AssSendMessage;
//		//	associationItem.ForeignKeyConstraintType = transAssociation.ForeignKeyConstraintType;
//		//	associationItem.RefModel = transAssociation.RefModel;
//		//	associationItem.RefModelID = transAssociation.RefModelID;
//		//	associationItem.RefMode.getCode() = transAssociation.RefMode.getCode();
//		//	associationItem.RefModelName = transAssociation.RefModelName;
//		//	associationItem.KeyCollection.clear();
//		//	foreach (GspAssociationKey associationKey in transAssociation.KeyCollection)
//		//	{
//		//		associationItem.KeyCollection.add(associationKey);
//		//	}
//
//		//	foreach (var refElement in associationItem.getRefElementCollection())
//		//	{
//		//		GspViewModelElement transRefElement = getElement(transAssociation.getRefElementCollection(),
//		//			refElement.getLabelID());
//		//		if (transRefElement == null)
//		//			throw new Exception(errorToken+"实体中没有找到引用字段【" + refElement.getLabelID() + "】，请联系管理员。"+errorToken);
//		//		//refElement.DefaultVauleType = transRefElement.DefaultVauleType;
//		//		//refElement.DefaultValue = transRefElement.DefaultValue;
//		//		refElement.getLabelID() = transRefElement.getLabelID();
//		//		((GspViewModelElement)refElement).getMapping().TargetObjId = transRefElement.getMapping().TargetObjId;
//		//	}
//
//		//}
//	}
//
//	/**
//	 * 处理已删掉的be关联带出字段
//	 *
//	 * @param transAssociation
//	 * @param currentAsso
//	 */
//	private void handleRefElementCollection(GspAssociation transAssociation, GspAssociation currentAsso) {
//		GspFieldCollection originVoRefElements = currentAsso.getRefElementCollection().clone(currentAsso.getRefElementCollection().getParentObject(), currentAsso);
//		currentAsso.getRefElementCollection().clear();
//		GspFieldCollection transRefElements = transAssociation.getRefElementCollection();
//		for (IGspCommonField refEle : originVoRefElements) {
//			GspViewModelElement transRefElement = (GspViewModelElement) transRefElements.getItem(ele -> ele.getRefElementId() == refEle.getRefElementId());
//			if (transRefElement == null) {
//				continue;
//			}
//			// 兼容旧的带出字段mapping中无TargetElementID及TargetObjectID问题
//			handleEleMapping(transRefElement.getMapping(), ((GspViewModelElement) refEle).getMapping());
//			refEle.setIsFromAssoUdt(transRefElement.getIsFromAssoUdt());
//			currentAsso.getRefElementCollection().add(refEle);
//		}
//	}
//
//	private void handleEleMapping(GspVoElementMapping transMapping, GspVoElementMapping eleMapping) {
//		// 兼容旧的带出字段mapping中无TargetElementID及TargetObjectID问题
//		if (DotNetToJavaStringHelper.isNullOrEmpty(eleMapping.getTargetObjectId())) {
//			eleMapping.setTargetObjectId(transMapping.getTargetObjectId());
//		}
//		if (DotNetToJavaStringHelper.isNullOrEmpty(eleMapping.getTargetElementId())) {
//			eleMapping.setTargetElementId(transMapping.getTargetElementId());
//		}
//		// 若关联带出字段ID已更新
//		if (!eleMapping.getTargetElementId().equals(transMapping.getTargetElementId())) {
//			eleMapping.setTargetElementId(transMapping.getTargetElementId());
//			eleMapping.setTargetObjId(transMapping.getTargetObjId());
//		}
//	}
//
//	private GspAssociation getAssociation(GspAssociationCollection collection, String associationId) {
//		return (GspAssociation) collection.getItem(item -> associationId.equals(item.getId()));
//	}
//
//	private GspViewModelElement getElement(GspFieldCollection collection, String labelId) {
//		for (IGspCommonField item : collection) {
//			if (labelId.equals(item.getLabelID())) {
//				return (GspViewModelElement) ((GspViewModelElement) item != null ? item : null);
//			}
//		}
//		return null;
//	}
//	//#endregion
//
//	//#region 操作
//
//	private void linkActions(GspViewModel vm) {
//		ArrayList<ViewModelAction> list = vm.getActions().getAllItems(item -> item.getType() == ViewModelActionType.BEAction);
//		VMActionCollection mappedBeActions = new VMActionCollection();
//		mappedBeActions.addAll(list);
//		GspBusinessEntity be = getBe(vm.getMapping().getTargetMetadataId());
//		if (mappedBeActions.size() > 0) {
//			for (ViewModelAction action : mappedBeActions) {
//				BizMgrAction mgrAction = (BizMgrAction) be.getBizMgrActions().getItem(item -> action.getMapping().getTargetObjId().equals(item.getID()));
//				if (mgrAction == null) {
//					vm.getActions().remove(action);
//					continue;
//				}
//				MappedBizAction tranAction = ConvertUtils.toMappedAction(mgrAction, action.getMapping().getTargetMetadataId(), null);
//				linkWithMgrAction(action, tranAction);
//			}
//		}
//	}
//
//	private void linkWithMgrAction(ViewModelAction targetAction, MappedBizAction sourceAction) {
//		targetAction.setCode(sourceAction.getCode());
//		targetAction.setName(sourceAction.getName());
//		targetAction.getParameterCollection().clear();
//		if (sourceAction.getParameterCollection().getCount() > 0)
//			for (int i = 0; i < sourceAction.getParameterCollection().getCount(); i++) {
//				targetAction.getParameterCollection().add(sourceAction.getParameterCollection().getItem(i));
//			}
//
//		targetAction.setReturnValue(sourceAction.getReturnValue());
//	}
//
////C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
//	//#endregion
//
////C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
//	//#region 业务实体缓存
//
//	private GspBusinessEntity getBe(String id) {
//		return getBizEntity(id);
//	}
//
//	private GspBizEntityElement getRefBizElement(String refBeId, String refElementId) {
//		return getRefBizElement(refBeId, refElementId, true);
//	}
//
//	private GspBizEntityElement getRefBizElement(String refBeId, String refElementId, boolean containRef) {
//		GspBusinessEntity be = getBizEntity(refBeId);
//		ArrayList<IGspCommonElement> list = be.getAllElementList(true);
//		for (IGspCommonElement ele : list) {
//			if (refElementId.equals(ele.getID())) {
//				return (GspBizEntityElement) ele;
//			}
//		}
//		return null;
//	}
//
//	//ORIGINAL LINE: private GspBizEntityElement getBizElement(ViewModelMapping mapping, string elementLabelId = "")
//	private GspBizEntityElement getBizElement(ViewModelMapping mapping) {
//		return getBizElement(mapping, "");
//	}
//
//	private GspBizEntityElement getBizElement(ViewModelMapping mapping, String elementLabelId) {
//		if (mapping == null) {
//			throw new RuntimeException(errorToken + String.format("当前字段'%1$s'无mapping信息。", elementLabelId) + errorToken);
//		}
//		String metadataId = mapping.getTargetMetadataId();
//		String objId = mapping.getTargetObjId();
//		GspBizEntityElement ele = getRefBizElement(metadataId, objId, false);
//		return ele;
//	}
//
//	private GspBizEntityObject getBizObject(ViewModelMapping mapping, String objCode) {
//		if (mapping == null) {
//			throw new RuntimeException(String.format("%1$s当前对象'%2$s'无mapping信息。%1$s", errorToken, objCode));
//		}
//		String metadataId = mapping.getTargetMetadataId();
//		String objId = mapping.getTargetObjId();
//		GspBusinessEntity be = getBizEntity(metadataId);
//		GspBizEntityObject bizObj = (GspBizEntityObject) be.getNode(node -> node.getID().equals(objId));
//		return bizObj;
//	}
//
//	//todo:依赖lcm加载元数据服务
//	private GspBusinessEntity getBizEntity(String id) {
//		return null;
////        if (bizEntitiesDic.containsKey(id)) {
////            //return bizEntitiesDic.FirstOrDefault(item => item.Key == id).Value;
////            return bizEntitiesDic.get(id);
////        }
////        GspMetadata metadata = LcmDtService.getRefMetadata(id);
////        if (metadata.Content instanceof GspBusinessEntity be)
////        {
////            bizEntitiesDic.put(id, be);
////            return be;
////        }
////        else
////        {
////            throw new RuntimeException(errorToken + $"当前vo的Mapping中记录的元数据不是业务实体，其id={id}。" + errorToken);
////        }
//	}
//
////C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
//	//#endregion
//}
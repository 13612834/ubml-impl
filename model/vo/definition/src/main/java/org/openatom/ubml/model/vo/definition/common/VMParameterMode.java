package org.openatom.ubml.model.vo.definition.common;

/**
 * The Definition Of View Model Parametrer Mode
 *
 * @ClassName: VMParameterMode
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum VMParameterMode
{
	/** 
	 传入参数
	 
	*/
	IN,
	/** 
	 传出参数
	 
	*/
	OUT,
	/** 
	 传入传出参数
	 
	*/
	INOUT;

	public int getValue()
	{
		return this.ordinal();
	}

	public static VMParameterMode forValue(int value)
	{
		return values()[value];
	}
}
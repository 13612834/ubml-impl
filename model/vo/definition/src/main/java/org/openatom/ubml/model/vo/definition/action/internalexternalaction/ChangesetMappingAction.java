package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;

/**
 * The Definition Of Changeset Mapping Action
 *
 * @ClassName: ChangesetMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ChangesetMappingAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "5798f884-c222-47f4-8bbe-685c7013dee4";
	public static final String code = "ChangesetMapping";
	public static final String name = "内置变更集Mapping操作";
	public ChangesetMappingAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}
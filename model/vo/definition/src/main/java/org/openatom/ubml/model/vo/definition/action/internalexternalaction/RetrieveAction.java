package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;

/**
 * The Definition Of Retrieve Action
 *
 * @ClassName: RetrieveAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RetrieveAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "7a02f472-5bbd-424b-9d9e-f82e3f9f448e";
	public static final String code = "Retrieve";
	public static final String name = "内置检索操作";
	public RetrieveAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}
}
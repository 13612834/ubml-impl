package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
import org.openatom.ubml.model.vo.definition.action.mappedcdp.MappedCdpActionParameterCollection;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameterCollection;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
/**
 * The Json Deserializer Of Mapped Component Action Definition
 *
 * @ClassName: MappedCdpActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpActionDeserializer extends VmActionDeserializer<MappedCdpAction> {
    @Override
    protected MappedCdpAction createOp() {
        return new MappedCdpAction();
    }

    @Override
    protected VmParameterDeserializer createPrapDeserializer() {
        return new MappedCdpParaDeserializer();
    }

    @Override
    protected ViewModelParameterCollection createPrapCollection() {
        return new MappedCdpActionParameterCollection();
    }

    @Override
    protected boolean readExtendOpProperty(ViewModelAction op, String propName, JsonParser reader) {
        MappedCdpAction action = (MappedCdpAction)op;
        boolean hasProperty = true;
        switch (propName)
        {
            case ViewModelJsonConst.ComponentPkgName:
                action.setComponentPkgName(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.ComponentEntityId:
                action.setComponentEntityId(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.IsGenerateComponent:
                action.setIsGenerateComponent(SerializerUtils.readPropertyValue_boolean(reader));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}

package org.openatom.ubml.model.vo.definition;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Predicate;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonObject;
import org.openatom.ubml.model.common.definition.commonmodel.entity.GspCommonModel;
import org.openatom.ubml.model.common.definition.entity.IMetadataContent;
import org.openatom.ubml.model.vo.definition.collection.VMActionCollection;
import org.openatom.ubml.model.vo.definition.collection.ValueHelpConfigCollection;
import org.openatom.ubml.model.vo.definition.common.TemplateVoInfo;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.dataextendinfo.VoDataExtendInfo;
import org.openatom.ubml.model.vo.definition.json.model.ViewModelDeserializer;
import org.openatom.ubml.model.vo.definition.json.model.ViewModelSerializer;

/**
 * The Definition Of View Model Metamodel
 *
 * @ClassName: GspViewModel
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonDeserialize(using = ViewModelDeserializer.class)
@JsonSerialize(using = ViewModelSerializer.class)
public class GspViewModel extends GspCommonModel implements IMetadataContent, Cloneable {
	///#region 属性

	/**
	 * 启用标准日期时间格式
	 * <see cref="string"/>
	 */
	private Boolean enableStdTimeFormat = false;

	public Boolean getEnableStdTimeFormat() {
		return enableStdTimeFormat;
	}

	public void setEnableStdTimeFormat(Boolean enableStdTimeFormat) {
		this.enableStdTimeFormat = enableStdTimeFormat;
	}

	/**
	 * 描述
	 * <see cref="string"/>
	 */
	private String privateDescription;

	public final String getDescription() {
		return privateDescription;
	}

	public final void setDescription(String value) {
		privateDescription = value;
	}

	/**
	 * 模型映射：VM可以通过映射规则映射到BE或者其他数据源上
	 * <see cref="ViewModelMapping"/>
	 */
	private ViewModelMapping privateMapping;

	public final ViewModelMapping getMapping() {

		return privateMapping;
	}

	public final void setMapping(ViewModelMapping value) {
		privateMapping = value;
	}

	/**
	 * 根节点
	 * <see cref="GspViewObject"/>
	 */
	public GspViewObject getMainObject() {
		return (GspViewObject) super.getMainObject();
	}

	void setMainObject(GspViewObject value) {
		super.setMainObject(value);
	}

	/**
	 * 获取扩展类型,总是返回为"视图对象"类型
	 * <see cref="string"/>
	 */
	@Override
	public String getExtendType() {
		return "GspViewModel";
	}


	/**
	 * 值帮助配置集合
	 */
	private ValueHelpConfigCollection valueHelpConfigs = new ValueHelpConfigCollection();

	/**
	 * 值帮助配置集合
	 * <see cref="ValueHelpConfigCollection"/>
	 */
	public ValueHelpConfigCollection getValueHelpConfigs() {
		return this.valueHelpConfigs;
	}

	public void setValueHelpConfigs(ValueHelpConfigCollection value) {
		this.valueHelpConfigs = value;
	}

	/**
	 * 操作集合
	 */
	private VMActionCollection actions;

	/**
	 * 操作集合
	 * <see cref="VMActionCollection"/>
	 */
	public final VMActionCollection getActions() {
		if (actions == null) {
			actions = new VMActionCollection();
		}

		return actions;
	}

	public void setActions(VMActionCollection value) {
		this.actions = value;
	}

	private java.util.HashMap<String, String> extendProperties;

	/**
	 * 表单拓展节点
	 */
	public final java.util.HashMap<String, String> getExtendProperties() {
		if (extendProperties == null) {
			extendProperties = new java.util.HashMap<String, String>();
		}
		return extendProperties;
	}

	public void setExtendProperties(java.util.HashMap<String, String> value) {
		this.extendProperties = value;
	}

	private VoDataExtendInfo dataExtendInfo;

	/**
	 * 数据逻辑扩展
	 */
	public final VoDataExtendInfo getDataExtendInfo() {
		if (dataExtendInfo == null) {
			dataExtendInfo = new VoDataExtendInfo();
		}
		return dataExtendInfo;
	}

	public void setDataExtendInfo(VoDataExtendInfo value) {
		this.dataExtendInfo = value;
	}

	private TemplateVoInfo templateVoInfo;

	/**
	 * 模板VO信息
	 */
	public final TemplateVoInfo getTemplateVoInfo() {
		if (templateVoInfo == null) {
			templateVoInfo = new TemplateVoInfo();
		}
		return templateVoInfo;
	}

	public void setTemplateVoInfo(TemplateVoInfo value) {
		this.templateVoInfo = value;
	}

	private boolean autoConvertMessage;
	public boolean getAutoConvertMessage(){
		return autoConvertMessage;
	}

	public void setAutoConvertMessage(boolean value){
		autoConvertMessage = value;
	}
	///#endregion

	///#region 方法

	///#region 复写

	/**
	 * 重载Equals方法
	 *
	 * @param obj 要比较的对象
	 * @return <see cref="bool"/>
	 * 如果当前对象等于 other 参数，则为 true；否则为 false。
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj.equals(this)) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}

		return equals((GspViewModel) obj);
	}

	/**
	 * 当前对象是否等于同一类型的另一个对象。
	 *
	 * @param vo 与此对象进行比较的对象。
	 * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
	 */
	protected boolean equals(GspViewModel vo) {
		if (vo.equals(null)) {
			return false;
		}
		//&& this.DevLevelCode == other.DevLevelCode
		//&& this.ForwardMappingID == other.ForwardMappingID
		if (getID() == vo.getID() && getCode() == vo.getCode() && getName() == vo.getName() && getDescription().equals(vo.getDescription()) && getActions().equals(vo.getActions()) && vo.getMapping().equals(getMapping()) && getMainObject().equals(vo.getMainObject()))
		//&& this.SrcDevLevelCode == other.SrcDevLevelCode)
		{
			return true;
		}


		return false;
	}

	/**
	 * Serves as a hash function for a particular type.
	 *
	 * @return A hash code for the current <see cref="T:System.Object" />.
	 * <filterpriority>2</filterpriority>
	 */
	@Override
	public int hashCode() {
		{
			int hashCode = (getMainObject() != null ? getMainObject().hashCode() : 0);
			hashCode = (hashCode * 397) ^ (getValueHelpConfigs() != null ? getValueHelpConfigs().hashCode() : 0);
			hashCode = (hashCode * 397) ^ (getDescription() != null ? getDescription().hashCode() : 0);
			hashCode = (hashCode * 397) ^ (getMapping() != null ? getMapping().hashCode() : 0);
			hashCode = (hashCode * 397) ^ (actions != null ? actions.hashCode() : 0);
			hashCode = (hashCode * 397) ^ (getMainObject() != null ? getMainObject().hashCode() : 0);
			return hashCode;
		}
	}

	/**
	 * 克隆
	 *
	 * @return <see cref="object"/>
	 */
//ORIGINAL LINE: public new object Clone()
	public final GspViewModel clone() {
		Object tempVar = null;
		tempVar = super.clone();

		GspViewModel vo = (GspViewModel) ((tempVar instanceof GspViewModel) ? tempVar : null);
		if (vo == null) {
			throw new RuntimeException("克隆GSPViewObject失败");
		}
		if (getMainObject() != null) {
			Object tempVar2 = getMainObject().clone();
			vo.setMainObject((GspViewObject) ((tempVar2 instanceof GspViewObject) ? tempVar2 : null));
			ArrayList<IGspCommonObject> objList = vo.getAllObjectList();
			if (objList != null && objList.size() > 0) {
				for (IGspCommonObject obj : objList) {
					obj.setBelongModel(vo);
					obj.setBelongModelID(vo.getID());
				}
			}
		}
		if (getMapping() != null) {
			Object tempVar2 = getMapping().clone();
			vo.setMapping((ViewModelMapping) ((tempVar2 instanceof ViewModelMapping) ? tempVar2 : null));
		}
		return vo;
	}


	/**
	 * 重载ToString方法
	 *
	 * @return <see cref="string"/>
	 */
	@Override
	public String toString() {
		return String.format("ID:%1$s,Code:%2$s,Name:%3$s,RootNode:%4$s", getID(), getCode(), getName(), getMainObject().getCode());
	}

	///#endregion

	///#region 获取节点

	/**
	 * 查找指定的VM节点
	 * <p>
	 * //	 @param viewObjectCode 节点编号
	 *
	 * @return <see cref="GspViewObject"/>
	 */
	public final GspViewObject getNode(String nodeCode) {
		return getNode(pre -> pre.getCode().equals(nodeCode));
	}

	public final GspViewObject getNode(Predicate<GspViewObject> predicate) {
		ArrayList<GspViewObject> result = getAllNodes(predicate);
		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}
//		return GetNodes(node => node.Code.equals(viewObjectCode, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
//GetNodes();
//		ArrayList<GspBizEntityObject> bizObjs = getAllNodes();
//		ArrayList<GspBizEntityObject> result = new ArrayList<GspBizEntityObject>();
//		for (GspBizEntityObject item : bizObjs) {
//			if (predicate.test(item)) {
//				result.add(item);
//			}
//		}
//		return result;
//		for(GspViewObject node:){
//			if(node.getCode().equals(viewObjectCode)){
//				return node;
//			}


//		return GetNodes(node => node.Code.equals(viewObjectCode, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();


	/**
	 * 查询符合条件的节点
	 * <p>
	 * //	 @param predict
	 *
	 * @return <see cref="List{T}"/>
	 * <see cref="GspViewObject"/>
	 */

//	public final ArrayList<GspBizEntityObject> getAllNodes(Predicate<GspBizEntityObject> predicate) {
//
//		ArrayList<GspBizEntityObject> bizObjs = getAllNodes();
//		ArrayList<GspBizEntityObject> result = new ArrayList<GspBizEntityObject>();
//		for (GspBizEntityObject item : bizObjs) {
//			if (predicate.test(item)) {
//				result.add(item);
//			}
//		}
//		return result;
//	}
	public ArrayList<GspViewObject> getAllNodes(Predicate<GspViewObject> predicate) {
		ArrayList<GspViewObject> bizObjs = getAllNodes(null);
		ArrayList<GspViewObject> result = new ArrayList();
		for (GspViewObject item : bizObjs) {
			if (predicate.test(item)) {
				result.add(item);
			}
		}
		return result;
	}

//	public final java.util.List<GspViewObject> GetNodes(HashMap<GspViewObject, Boolean> predict)
//	{
//		if (predict != null)解决方式和gspviewmodel中的一样
//		{
//			return GetAllObjectList().Select(item => (GspViewObject)((item instanceof GspViewObject) ? item : null)).Where(predict).ToList();
//		}
//		return GetAllObjectList().Select(item => (GspViewObject)((item instanceof GspViewObject) ? item : null)).ToList();
//	}

	/**
	 * 查询当前VM所有节点的字典
	 * 其中Key是节点Code
	 *
	 * @return <see cref="Dictionary{TKey, TValue}"/>
	 * <see cref="string"/>
	 * <see cref="GspViewObject"/>
	 */
	public final java.util.HashMap<String, GspViewObject> getNodesDic() {
//		return GetNodes(null).ToDictionary(item => item.Code, item => item);
		HashMap toDic = new HashMap();
		for (IGspCommonObject item : getAllObjectList()) {
			toDic.put(item.getCode(), item);

		}
//		return GetNodes(null).ToDictionary(item => item.Code, item => item);
		return toDic;

	}

	///#endregion

	///#region 联动-弃用
	/**
	 * 与业务实体建立连接
	 * <p>
	 * //	 @param bizEntity
	 *
	 * @return <see cref="GspViewModel"/>
	 */
//	public final GspViewModel LinkWithBizEntity(GspBusinessEntity bizEntity)
//	{
////		java.util.HashMap<String, GspViewModelElement> viewElements = GetAllElementList(true).ToDictionary(item => item.getID(), item => (GspViewModelElement)((item instanceof GspViewModelElement) ? item : null));
//		java.util.HashMap<String, GspViewModelElement> viewElements = getAllElementList(true).ToDictionary(item => item.getID(),
//		item => (GspViewModelElement)((item instanceof GspViewModelElement) ? item : null));
//		java.util.HashMap<String, String> elementMappings = new java.util.HashMap<String, String>();
////		java.util.ArrayList<IGSPCommonElement> elementList = GetAllElementList(true);
//		java.util.ArrayList<IGspCommonElement> elementList = getAllElementList(true);
//		for (IGspCommonElement item : elementList)
//		{
////			if (item instanceof GspViewModelElement && ((GspViewModelElement)item)?.Mapping != null)
//			if (item instanceof GspViewModelElement && ((GspViewModelElement)item).getMapping() != null)
//			{
////				var key = ((GspViewModelElement)((item instanceof GspViewModelElement) ? item : null)).getMapping().TargetObjId;
//				String key =
//						((GspViewModelElement)((item instanceof GspViewModelElement) ? item : null)).getMapping().getTargetObjectId();
//				String value = item.getID();
//				elementMappings.put(key, value);
//			}
//		}
//		//Dictionary<string, string> elementMappings =
//		//this.GetAllElementList(true)
//		//    .ToDictionary(item => (item as GspViewModelElement).Mapping.TargetObjId, item => item.ID);
////		java.util.HashMap<String, GSPBizEntityObject> bizEntityObjects = bizEntity.GetAllObjectList().ToDictionary(item => item.ID, item => (GSPBizEntityObject)((item instanceof GSPBizEntityObject) ? item : null));
//		java.util.HashMap<String, GspBizEntityObject> bizEntityObjects =
//				bizEntity.getAllObjectList().todictory(item.getID(),
//		(GspBizEntityObject)((item instanceof GspBizEntityObject) ? item : null));
////		item => (GSPBizEntityObject)((item instanceof GSPBizEntityObject) ? item : null));
////		java.util.HashMap<String, GSPBizEntityElement> bizEntityElements = bizEntity.GetAllElementList(true).ToDictionary(item => item.ID, item => (GSPBizEntityElement)((item instanceof GSPBizEntityElement) ? item : null));
//		java.util.HashMap<String, GspBizEntityElement> bizEntityElements =
//		bizEntity.getAllElementList(true).todictionary(item => item.ID, item => (GspBizEntityElement)((item instanceof GspBizEntityElement) ? item : null));
//
//		//BizEntitySerializer serializer = new BizEntitySerializer();
//		//string xmlBizContent = serializer.Serialize(bizEntity);
//		//ViewModelParser parser = new ViewModelParser();
//		//GspViewModel transViewModel = parser.Parse(xmlBizContent) as GspViewModel;
////		GspViewModel transViewModel = ConvertUtils.ConvertToViewModel(bizEntity, null, null);
//		GspViewModel transViewModel = ConvertUtils.ConvertToViewModel(bizEntity, null, null);
//
//			///#region 联动对象上的属性
//
//		java.util.List<GspViewObject> tempVar = GetNodes(null);
//		java.util.ArrayList<GspViewObject> viewObjects = (java.util.ArrayList<GspViewObject>)((tempVar instanceof java.util.ArrayList<GspViewObject>) ? tempVar : null);
//		if (viewObjects == null)
//		{
//			throw new RuntimeException("#GSPBefError#实体上没有对象，请联系管理员检查数据#GSPBefError#");
//		}
//		for (GspViewObject objectItem : viewObjects)
//		{
//			if (objectItem.getMapping() == null || !bizEntityObjects.containsKey(objectItem.getMapping().getTargetObjId()))
//			{
//				continue;
//			}
////			GSPBizEntityObject beObject = bizEntityObjects.get(objectItem.getMapping().getTargetObjId());
//			GspBizEntityObject beObject = bizEntityObjects.get(objectItem.getMapping().getTargetObjId());
//
//				///#region 分级信息
////			if (beObject.HirarchyInfo != null)
////			{
////				objectItem.HirarchyInfo = beObject.HirarchyInfo;
////				if (beObject.HirarchyInfo.IsDetailElement != null)
////				{
////					if (elementMappings.containsKey(beObject.HirarchyInfo.IsDetailElement.ID))
////					{
////						String detailElementId = elementMappings.get(beObject.HirarchyInfo.IsDetailElement.ID);
////						objectItem.HirarchyInfo.IsDetailElement = viewElements.get(detailElementId);
////					}
////					else
////					{
////						GspViewModelElement transElement = getTransElement(transViewModel, beObject.HirarchyInfo.IsDetailElement, bizEntity.ID);
////						objectItem.getContainElements().Add(transElement);
////						objectItem.HirarchyInfo.IsDetailElement = transElement;
////					}
////
////				}
////				if (beObject.HirarchyInfo.LayerElement != null)
////				{
////					if (elementMappings.containsKey(beObject.HirarchyInfo.LayerElement.ID))
////					{
////						String layerElementId = elementMappings.get(beObject.HirarchyInfo.LayerElement.ID);
////						objectItem.HirarchyInfo.LayerElement = viewElements.get(layerElementId);
////					}
////					else
////					{
////						GspViewModelElement transElement = getTransElement(transViewModel, beObject.HirarchyInfo.LayerElement, bizEntity.ID);
////						objectItem.getContainElements().Add(transElement);
////						objectItem.HirarchyInfo.LayerElement = transElement;
////					}
////
////				}
////				if (beObject.HirarchyInfo.PathElement != null)
////				{
////					if (elementMappings.containsKey(beObject.HirarchyInfo.PathElement.ID))
////					{
////						String pathElementId = elementMappings.get(beObject.HirarchyInfo.PathElement.ID);
////						objectItem.HirarchyInfo.PathElement = viewElements.get(pathElementId);
////					}
////					else
////					{
////						GspViewModelElement transElement = getTransElement(transViewModel, beObject.HirarchyInfo.PathElement, bizEntity.ID);
////						objectItem.getContainElements().Add(transElement);
////						objectItem.HirarchyInfo.PathElement = transElement;
////					}
////				}
////
////				objectItem.HirarchyInfo.PathLength = beObject.HirarchyInfo.PathLength;
////
////				if (beObject.HirarchyInfo.ParentElement != null)
////				{
////					if (elementMappings.containsKey(beObject.HirarchyInfo.ParentElement.ID))
////					{
////						String parentElementId = elementMappings.get(beObject.HirarchyInfo.ParentElement.ID);
////						objectItem.HirarchyInfo.ParentElement = viewElements.get(parentElementId);
////					}
////					else
////					{
////						GspViewModelElement transElement = getTransElement(transViewModel, beObject.HirarchyInfo.ParentElement, bizEntity.ID);
////						objectItem.getContainElements().add(transElement);
////						objectItem.HirarchyInfo.ParentElement = transElement;
////					}
////				}
////
////				if (beObject.HirarchyInfo.ParentRefElement != null)
////				{
////					if (elementMappings.containsKey(beObject.HirarchyInfo.ParentRefElement.ID))
////					{
////						String pathElementId = elementMappings.get(beObject.HirarchyInfo.ParentRefElement.ID);
////						objectItem.HirarchyInfo.ParentRefElement = viewElements.get(pathElementId);
////					}
////					else
////					{
////						GspViewModelElement transElement = getTransElement(transViewModel, beObject.HirarchyInfo.ParentRefElement, bizEntity.ID);
////						objectItem.getContainElements().add(transElement);
////						objectItem.HirarchyInfo.ParentRefElement = transElement;
////					}
////				}
////			}
//
//				///#endregion
//
//				///#region 创建人等信息
////			if (elementMappings.containsKey(beObject.CreatorElementID))
////			if (elementMappings.containsKey(beObject.getCreateID()))
////			{
////				objectItem.CreatorElementID = elementMappings.get(beObject.CreatorElementID);
////			}
////			if (elementMappings.containsKey(beObject.CreatedDateElementID))
////			{
////				objectItem.CreatedDateElementID = elementMappings.get(beObject.CreatedDateElementID);
////			}
////			if (elementMappings.containsKey(beObject.ModifierElementID))
////			{
////				objectItem.ModifierElementID = elementMappings.get(beObject.ModifierElementID);
////			}
////			if (elementMappings.containsKey(beObject.ModifiedDateElementID))
////			{
////				objectItem.ModifiedDateElementID = elementMappings.get(beObject.ModifiedDateElementID);
////			}
//				///#endregion
////			objectItem.Code = beObject.Code;
//			objectItem.setCode(beObject.getCode());
//			objectItem.setIsVirtual(false); //be带出，IsVirtual=false
//			objectItem.setIsReadOnly(beObject.getIsReadOnly());
//
//				///#region ID生成规则
////			if (elementMappings.containsKey(beObject.ColumnGenerateID.ElementID))
//			if (elementMappings.containsKey(beObject.getColumnGenerateID().getElementID()))
//			{
//				objectItem.ColumnGenerateID.ElementID = elementMappings.get(beObject.ColumnGenerateID.ElementID);
//			}
//			objectItem.ColumnGenerateID.GernerateType = beObject.ColumnGenerateID.GernerateType;
//				///#endregion
//
//				///#region 唯一性约束
//			//objectItem.ContainConstraints = beObject.ContainConstraints;
//			if (objectItem.ContainConstraints != null && objectItem.ContainConstraints.size() > 0)
//			{
//				objectItem.ContainConstraints.Clear();
//			}
//			for (GspUniqueConstraint beConstraint : beObject.ContainConstraints)
//			{
//				Object tempVar2 = beConstraint.clone();
//				GspUniqueConstraint viewModelConstraint = (GspUniqueConstraint)((tempVar2 instanceof GspUniqueConstraint) ? tempVar2 : null);
//				viewModelConstraint.ElementList.Clear();
//				for (IGSPCommonElement element : beConstraint.ElementList)
//				{
//					if (!elementMappings.containsKey(element.ID))
//					{
//						continue;
//					}
//					String vmElementId = elementMappings.get(element.ID);
//					if (DotNetToJavaStringHelper.isNullOrEmpty(vmElementId))
//					{
//						continue;
//					}
//					if (!viewElements.containsKey(vmElementId))
//					{
//						throw new RuntimeException("#GSPBefError#没有找到ID为【" + vmElementId + "】的字段#GSPBefError#");
//					}
//					viewModelConstraint.ElementList.Add(viewElements.get(vmElementId));
//				}
//				objectItem.ContainConstraints.Add(viewModelConstraint);
//			}
//				///#endregion
//
//				///#region 关联信息
//			if (objectItem.getParent() != null)
//			{
//				objectItem.keySet().Clear();
//				for (GSPAssociationKey ass : beObject.keySet())
//				{
//					GSPAssociationKey associationKey = new GSPAssociationKey();
//					associationKey.RefDataModelName = Name;
//					if (elementMappings.containsKey(ass.SourceElement))
//					{
//						associationKey.SourceElement = elementMappings.get(ass.SourceElement);
//					}
//					if (elementMappings.containsKey(ass.SourceElementDisplay))
//					{
//						associationKey.SourceElementDisplay = elementMappings.get(ass.SourceElementDisplay);
//					}
//					if (elementMappings.containsKey(ass.TargetElement))
//					{
//						associationKey.TargetElement = elementMappings.get(ass.TargetElement);
//					}
//					if (elementMappings.containsKey(ass.TargetElementDisplay))
//					{
//						associationKey.TargetElementDisplay = elementMappings.get(ass.TargetElementDisplay);
//					}
//					objectItem.keySet().Add(associationKey);
//				}
//			}
//				///#endregion
//		}
//			///#endregion
//
//			///#region 联动字段上的属性
//
//		for (java.util.Map.Entry<String, GspViewModelElement> item : viewElements.entrySet())
//		{
//			GspViewModelElement ele = item.getValue();
//			if (ele.getMapping() == null || !bizEntityElements.containsKey(ele.getMapping().getTargetObjId()))
//			{
//				continue;
//			}
//			GSPBizEntityElement bizElement = bizEntityElements.get(ele.getMapping().getTargetObjId());
//			ele.BillCodeConfig.CanBillCode = bizElement.BillCodeConfig.CanBillCode;
//			ele.BillCodeConfig.BillCodeID = bizElement.BillCodeConfig.BillCodeID;
//			ele.BillCodeConfig.BillCodeName = bizElement.BillCodeConfig.BillCodeName;
//			ele.BillCodeConfig.CodeGenerateType = bizElement.BillCodeConfig.CodeGenerateType;
//			ele.BillCodeConfig.CodeGenerateOccasion = bizElement.BillCodeConfig.CodeGenerateOccasion;
//			ele.LabelID = bizElement.LabelID;
//			ele.MDataType = bizElement.MDataType;
//			ele.ObjectType = bizElement.ObjectType;
//			if (ele.ObjectType == GSPElementObjectType.Association)
//			{
//				//GspViewModelElement transElement = transViewModel.FindElementById(bizElement.ID) as GspViewModelElement;// transElements[bizElement.ID];
//				GspViewModelElement transElement = transViewModel.GetMappedElementByBizElementId(bizElement.ID);
//				for (GSPAssociation associationItem : ele.ChildAssociations)
//				{
//					GSPAssociation transAssociation = getAssociation(transElement.ChildAssociations, associationItem.Id);
//					if (transAssociation == null)
//					{
//						throw new RuntimeException("#GSPBefError#关联未找到，请联系管理员#GSPBefError#");
//					}
//					associationItem.AssSendMessage = transAssociation.AssSendMessage;
//					associationItem.ForeignKeyConstraintType = transAssociation.ForeignKeyConstraintType;
//					associationItem.RefModel = transAssociation.RefModel;
//					associationItem.RefModelID = transAssociation.RefModelID;
//					associationItem.RefModelCode = transAssociation.RefModelCode;
//					associationItem.RefModelName = transAssociation.RefModelName;
//					associationItem.KeyCollection.Clear();
//					for (GSPAssociationKey associationKey : transAssociation.KeyCollection)
//					{
//						//associationKey.SourceElement = elementMappings[associationKey.SourceElement];
//						//associationKey.SourceElementDisplay = viewElements[associationKey.SourceElement].Name;
//						//associationKey.TargetElement = elementMappings[associationKey.TargetElement];
//						//associationKey.TargetElementDisplay = viewElements[associationKey.TargetElement].Name;
//
//						associationItem.KeyCollection.Add(associationKey);
//					}
//
//					for (var refElement : associationItem.RefElementCollection)
//					{
//						GspViewModelElement transRefElement = getElement(transAssociation.RefElementCollection, refElement.LabelID);
//						if (transRefElement == null)
//						{
//							throw new RuntimeException("#GSPBefError#实体中没有找到引用字段【" + refElement.LabelID + "】，请联系管理员#GSPBefError#");
//						}
//						//refElement.DefaultVauleType = transRefElement.DefaultVauleType;
//						//refElement.DefaultValue = transRefElement.DefaultValue;
//						refElement.LabelID = transRefElement.LabelID;
//						((GspViewModelElement) refElement).getMapping().TargetObjId = transRefElement.getMapping().getTargetObjId();
//					}
//
//				}
//			}
//			else if (ele.ObjectType == GSPElementObjectType.Enum)
//			{
//				ele.ContainEnumValues.Clear();
//				for (GSPEnumValue enumItem : bizElement.ContainEnumValues)
//				{
//					Object tempVar3 = enumItem.clone();
//					GSPEnumValue enumValue = (GSPEnumValue)((tempVar3 instanceof GSPEnumValue) ? tempVar3 : null);
//					ele.ContainEnumValues.Add(enumValue);
//				}
//			}
//			else
//			{
//				ele.ChildAssociations.Clear();
//				ele.ContainEnumValues.Clear();
//			}
//
//			ele.IsVirtual = false; //be带出，IsVirtual=false
//			//ele.CustomExpression = bizElement.CustomExpression;
//			ele.IsMultiLanguage = bizElement.IsMultiLanguage;
//			if (bizElement.IsRequire)
//			{
//				ele.IsRequire = bizElement.IsRequire;
//			}
//			if (bizElement.Readonly)
//			{
//				ele.Readonly = bizElement.Readonly;
//			}
//		}
//
//			///#endregion
//
//			///#region 联动be带出操作
//
//		var mappedBeActions = getActions().Where(item=>item.Type==ViewModel.Action.ViewModelActionType.BEAction).ToList();
//		VMActionCollection tranVmActions = transViewModel.getActions();
//		if (mappedBeActions.size()>0)
//		{
//			for (ViewModelAction action : mappedBeActions)
//			{
//				var tranAction = tranVmActions.Find(item=>action.getMapping().getTargetObjId().equals(item.Mapping.TargetObjId));
//				if (tranAction==null)
//				{
//					throw new RuntimeException(String.format("#GSPBefError#无与id='%1$s'的vm操作对应的。#GSPBefError#", tranAction.ID));
//				}
//				GetMappedBizAction((MappedBizAction)((action instanceof MappedBizAction) ? action : null), (MappedBizAction)((tranAction instanceof MappedBizAction) ? tranAction : null));
//			}
//		}
//
//			///#endregion
//		return this;
//	}
//
//	private void GetMappedBizAction(MappedBizAction targetAction, MappedBizAction sourceAction)
//	{
//		targetAction.setCode(sourceAction.getCode());
//		targetAction.setName(sourceAction.getCode());
//		targetAction.ParameterCollection.clear();
//		if (sourceAction.ParameterCollection.getCount()>0)
//		{
////			for (var item : sourceAction.ParameterCollection)
//			IViewModelParameterCollection list = sourceAction.getParameterCollection();
//			list[0];
//			for (IViewModelParameter item : sourceAction.getParameterCollection())
//			{
//				targetAction.ParameterCollection.add(item);
//			}
//		}
//		targetAction.setReturnValue(sourceAction.getReturnValue());
//	}

//	private GspViewModelElement GetMappedElementByBizElementId(String bizEleId)
//	{
////		var elements = GetAllElementList(true);
//		ArrayList elements = getAllElementList(true);
////		if (!(elements.Find(ele=>bizEleId.equals(((GspViewModelElement) ele).getMapping().TargetObjId)) instanceof GspViewModelElement vmElement))
//		for(GspViewModelElement ele;){
//			if(ele.getMapping().getTargetObjectId().equals(bizEleId)){
//
//			}
//		}
//		{
//			throw new RuntimeException(String.format("#GSPBefError#无与id='%1$s'的be字段对应的vm字段。#GSPBefError#", bizEleId));
//		}
//		return vmElement;
//	}
//
//	private GspViewModelElement getTransElement(GspViewModel transViewModel, IGspCommonElement bizElement, String bizId)
//	{
//		Object tempVar = transViewModel.findElementById(bizElement.getID());
//		GspViewModelElement transElement = (GspViewModelElement)((tempVar instanceof GspViewModelElement) ? tempVar : null);
//		if (transElement != null)
//		{
//			GspVoElementMapping tempVar2 = new GspVoElementMapping();
//			tempVar2.setMapType(MappingType.Element);
//			tempVar2.setTargetObjId(bizElement.getID());
//			tempVar2.setTargetMetadataId(bizId);
//			tempVar2.setTargetMetadataPkgName(transViewModel.getMapping().getTargetMetadataPkgName());
//			transElement.setMapping(tempVar2);
//		}
//		return transElement;
//	}
//
//	private GspAssociation getAssociation(GspAssociationCollection collection, String associationId)
//	{
////		return (GspAssociation)collection.FirstOrDefault(item => associationId.equals(item.Id));
//		for(GspAssociation item:collection){
//			if(item.getId().equals(associationId)){
//				return item;
//			}
//		}
//		return null;
//	}
//
//	private GspViewModelElement getElement(GspFieldCollection collection, String labelId)
//	{
//		for (IGspCommonField item : collection)
//		{
//			if (labelId.equals(item.getLabelID()))
//			{
//				return (GspViewModelElement)((item instanceof GspViewModelElement) ? item : null);
//			}
//		}
//		return null;
//	}
	///#endregion

	///#region manager
	private static final String ItemNameTemplate = "I%1$sManager";

	public final String getMgrInterfaceName() {
		return String.format(ItemNameTemplate, getCode());
	}

	public final String getChangesetClassName() {
		return String.format("%1$s%2$s", getCode(), "ViewModelChange");
	}


	public final String getDefaultValueClassName() {
		return String.format("%1$s%2$s", getCode(), "DefaultValue");
	}
}
package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;

/**
 * The Definition Of Query Action
 *
 * @ClassName: QueryAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class QueryAction extends MappedCdpAction implements IInternalExtendAction
{
	public static final String id = "6fe68bfa-7c1b-4d6b-a7ef-14654168ae75";
	public static final String code = "Query";
	public static final String name = "内置查询操作";
	public QueryAction()
	{
		setID(id);
		setCode(code);
		setName(name);
	}

}
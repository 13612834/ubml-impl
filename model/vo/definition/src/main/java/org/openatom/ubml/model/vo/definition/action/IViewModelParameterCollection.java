//package org.openatom.ubml.model.vo.definition.Interface;
package org.openatom.ubml.model.vo.definition.action;

import java.util.List;

/**
 * The Collection Of The Parameter
 *
 * @ClassName: IViewModelParameterCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IViewModelParameterCollection<T extends IViewModelParameter> extends List<T> {

  /**
   * @return The Parameters Count In The Collection
   */
  int getCount();

  /**
   * Get A Parameter With The Input Index
   * @param index The Parameter To Get
   * @return The Got Parameter
   */
  T getItem(int index);
}
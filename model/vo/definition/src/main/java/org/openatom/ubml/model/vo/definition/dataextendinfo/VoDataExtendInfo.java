package org.openatom.ubml.model.vo.definition.dataextendinfo;

import org.openatom.ubml.model.vo.definition.collection.VMActionCollection;

/**
 * The Definition Of View Object Data Exntend Info
 *
 * @ClassName: VoDataExtendInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VoDataExtendInfo {
    ///#region 私有字段

    private VMActionCollection dataMappingActions;
    private VMActionCollection beforeQueryActions;
    private VMActionCollection queryActions;
    private VMActionCollection afterQueryActions;
    private VMActionCollection retrieveActions;
    private VMActionCollection beforeRetrieveActions;
    private VMActionCollection afterRetrieveActions;
    private VMActionCollection beforeModifyActions;
    private VMActionCollection modifyActions;
    private VMActionCollection afterModifyActions;
    private VMActionCollection changesetMappingActions;
    private VMActionCollection beforeCreateActions;
    private VMActionCollection createActions;
    private VMActionCollection beforeDeleteActions;
    private VMActionCollection deleteActions;
    private VMActionCollection afterSaveActions;
    private VMActionCollection beforeSaveActions;
    private VMActionCollection afterCreateActions;
    private VMActionCollection dataReversalMappingActions;
    private VMActionCollection changesetReversalMappingActions;
    private VMActionCollection afterDeleteActions;
    private VMActionCollection beforeMultiDeleteActions;
    private VMActionCollection multiDeleteActions;
    private VMActionCollection afterMultiDeleteActions;

    ///#endregion
    public final VMActionCollection getBeforeMultiDeleteActions() {
        if (beforeMultiDeleteActions == null) {
            beforeMultiDeleteActions = new VMActionCollection();
        }

        return beforeMultiDeleteActions;
    }

    public void setBeforeMultiDeleteActions(VMActionCollection value) {
        this.beforeMultiDeleteActions = value;
    }

    public final VMActionCollection getMultiDeleteActions() {
        if (multiDeleteActions == null) {
            multiDeleteActions = new VMActionCollection();
        }

        return multiDeleteActions;
    }

    public void setMultiDeleteActions(VMActionCollection value) {
        this.multiDeleteActions = value;
    }

    public final VMActionCollection getAfterMultiDeleteActions() {
        if (afterMultiDeleteActions == null) {
            afterMultiDeleteActions = new VMActionCollection();
        }

        return afterMultiDeleteActions;
    }

    public void setAfterMultiDeleteActions(VMActionCollection value) {
        this.afterMultiDeleteActions = value;
    }

    /**
     * 数据Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getDataMappingActions() {
        if (dataMappingActions == null) {
            dataMappingActions = new VMActionCollection();
        }

        return dataMappingActions;
    }

    public void setDataMappingActions(VMActionCollection value) {
        this.dataMappingActions = value;
    }

    /**
     * 查询数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeQueryActions() {
        if (beforeQueryActions == null) {
            beforeQueryActions = new VMActionCollection();
        }

        return beforeQueryActions;
    }

    public void setBeforeQueryActions(VMActionCollection value) {
        this.beforeQueryActions = value;
    }

    /**
     * 查询数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getQueryActions() {
        if (queryActions == null) {
            queryActions = new VMActionCollection();
        }

        return queryActions;
    }

    public void setQueryActions(VMActionCollection value) {
        this.queryActions = value;
    }

    /**
     * 查询数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterQueryActions() {
        if (afterQueryActions == null) {
            afterQueryActions = new VMActionCollection();
        }

        return afterQueryActions;
    }

    public void setAfterQueryActions(VMActionCollection value) {
        this.afterQueryActions = value;
    }

    /**
     * 检索数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeRetrieveActions() {
        if (beforeRetrieveActions == null) {
            beforeRetrieveActions = new VMActionCollection();
        }

        return beforeRetrieveActions;
    }

    public void setBeforeRetrieveActions(VMActionCollection value) {
        this.beforeRetrieveActions = value;
    }

    /**
     * 检索数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getRetrieveActions() {
        if (retrieveActions == null) {
            retrieveActions = new VMActionCollection();
        }

        return retrieveActions;
    }

    //	public void setRetrieveActions(VMActionCollection value)
    public void setRetrieveActions(VMActionCollection value) {
        this.retrieveActions = value;
    }

    /**
     * 检索数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterRetrieveActions() {
        if (afterRetrieveActions == null) {
            afterRetrieveActions = new VMActionCollection();
        }

        return afterRetrieveActions;
    }

    public void setAfterRetrieveActions(VMActionCollection value) {
        this.afterRetrieveActions = value;
    }

    /**
     * 修改数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeModifyActions() {
        if (beforeModifyActions == null) {
            beforeModifyActions = new VMActionCollection();
        }

        return beforeModifyActions;
    }

    public void setBeforeModifyActions(VMActionCollection value) {
        this.beforeModifyActions = value;
    }

    /**
     * 修改数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getModifyActions() {
        if (modifyActions == null) {
            modifyActions = new VMActionCollection();
        }

        return modifyActions;
    }

    public void setModifyActions(VMActionCollection value) {
        this.modifyActions = value;
    }

    /**
     * 修改数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterModifyActions() {
        if (afterModifyActions == null) {
            afterModifyActions = new VMActionCollection();
        }

        return afterModifyActions;
    }

    public void setAfterModifyActions(VMActionCollection value) {
        this.afterModifyActions = value;
    }

    /**
     * 变更集Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getChangesetMappingActions() {
        if (changesetMappingActions == null) {
            changesetMappingActions = new VMActionCollection();
        }

        return changesetMappingActions;
    }

    //	public void setChangesetMappingActions(VMActionCollection value)
    public void setChangesetMappingActions(VMActionCollection value) {
        this.changesetMappingActions = value;
    }

    /**
     * 新增数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeCreateActions() {
        if (beforeCreateActions == null) {
            beforeCreateActions = new VMActionCollection();
        }

        return beforeCreateActions;
    }

    public void setBeforeCreateActions(VMActionCollection value) {
        this.beforeCreateActions = value;
    }


    /**
     * 新增数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getCreateActions() {
        if (createActions == null) {
            createActions = new VMActionCollection();
        }

        return createActions;
    }

    public void setCreateActions(VMActionCollection value) {
        this.createActions = value;
    }

    /**
     * 新增数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterCreateActions() {
        if (afterCreateActions == null) {
            afterCreateActions = new VMActionCollection();
        }

        return afterCreateActions;
    }

    //	public void setAfterCreateActions(VMActionCollection value)
    public void setAfterCreateActions(VMActionCollection value) {
        this.afterCreateActions = value;
    }

    /**
     * 删除数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeDeleteActions() {
        if (beforeDeleteActions == null) {
            beforeDeleteActions = new VMActionCollection();
        }

        return beforeDeleteActions;
    }

    //	public void setBeforeDeleteActions(VMActionCollection value)
    public void setBeforeDeleteActions(VMActionCollection value) {
        this.beforeDeleteActions = value;
    }


    /**
     * 删除数据
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getDeleteActions() {
        if (deleteActions == null) {
            deleteActions = new VMActionCollection();
        }

        return deleteActions;
    }

    public void setDeleteActions(VMActionCollection value) {
        this.deleteActions = value;
    }

    /**
     * 删除数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterDeleteActions() {
        if (afterDeleteActions == null) {
            afterDeleteActions = new VMActionCollection();
        }

        return afterDeleteActions;
    }

    public void setAfterDeleteActions(VMActionCollection value) {
        this.afterDeleteActions = value;
    }


    /**
     * 保存数据前
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getBeforeSaveActions() {
        if (beforeSaveActions == null) {
            beforeSaveActions = new VMActionCollection();
        }

        return beforeSaveActions;
    }

    public void setBeforeSaveActions(VMActionCollection value) {
        this.beforeSaveActions = value;
    }

    /**
     * 数据反向Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getDataReversalMappingActions() {
        if (dataReversalMappingActions == null) {
            dataReversalMappingActions = new VMActionCollection();
        }

        return dataReversalMappingActions;
    }

    public void setDataReversalMappingActions(VMActionCollection value) {
        this.dataReversalMappingActions = value;
    }

    /**
     * 保存数据后
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getAfterSaveActions() {
        if (afterSaveActions == null) {
            afterSaveActions = new VMActionCollection();
        }

        return afterSaveActions;
    }

    public void setAfterSaveActions(VMActionCollection value) {
        this.afterSaveActions = value;
    }


    /**
     * 变更集反向Mapping
     * <see cref="VMActionCollection"/>
     */
    public final VMActionCollection getChangesetReversalMappingActions() {
        if (changesetReversalMappingActions == null) {
            changesetReversalMappingActions = new VMActionCollection();
        }

        return changesetReversalMappingActions;
    }

    public void setChangesetReversalMappingActions(VMActionCollection value) {
        this.changesetReversalMappingActions = value;
    }
}
package org.openatom.ubml.model.vo.definition.json.operation;

import org.openatom.ubml.model.vo.definition.action.MappedBizAction;
import org.openatom.ubml.model.vo.definition.action.mappedbiz.MappedBizActionParameterCollection;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameterCollection;

/**
 * The Json  Deserializer Of The View Model Action Of  Mapped Be Action
 *
 * @ClassName: MappedBizActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionDeserializer extends VmActionDeserializer<MappedBizAction> {
    @Override
    protected MappedBizAction createOp() {
        return new MappedBizAction();
    }

    @Override
    protected VmParameterDeserializer createPrapDeserializer() {
        return new MappedBizActionParaDeserializer();
    }

    @Override
    protected ViewModelParameterCollection createPrapCollection() {
        return new MappedBizActionParameterCollection();
    }
}

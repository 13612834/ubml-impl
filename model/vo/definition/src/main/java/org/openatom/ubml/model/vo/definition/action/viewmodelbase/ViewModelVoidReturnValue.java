package org.openatom.ubml.model.vo.definition.action.viewmodelbase;



import org.openatom.ubml.model.vo.definition.common.VMParameterType;

/**
 * The Definition Of  Void View Model Return Value
 *
 * @ClassName: ViewModelVoidReturnValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelVoidReturnValue extends ViewModelReturnValue
{
	public static final String assembly = "mscorlib.dll";
	public static final String className = "Void";

	/** 
	 参数类对应程序集类
	 
	*/
	 @Override
	public String getAssembly(){
	 	return assembly;
	 }

	@Override
	public void setAssembly(String value)
	{
	}

	/** 
	 参数类名
	 
	*/
	 @Override
	 public String getClassName(){
		 return className;
	 }

	@Override
	public void setClassName(String value)
	{
	}

	/** 
	 参数类型，与Assembly和ClassName关联
	 
	*/
	 @Override
	 public VMParameterType getParameterType()
	 {
	 	return VMParameterType.Custom;

	 }
	@Override
	public void setParameterType(VMParameterType value)
	{
	}
}
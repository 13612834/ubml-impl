package org.openatom.ubml.model.vo.definition.action.viewmodelbase;

import org.openatom.ubml.model.common.definition.cef.collection.BaseList;
import org.openatom.ubml.model.vo.definition.action.IViewModelParameter;
import org.openatom.ubml.model.vo.definition.action.IViewModelParameterCollection;

/**
 * The Collection Of View Model Parameter
 *
 * @ClassName: ViewModelParameterCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelParameterCollection<T extends IViewModelParameter> extends
    BaseList<T> implements IViewModelParameterCollection<T> {

  public ViewModelParameterCollection() {
  }
}
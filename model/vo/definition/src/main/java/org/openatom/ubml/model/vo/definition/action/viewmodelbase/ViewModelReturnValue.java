package org.openatom.ubml.model.vo.definition.action.viewmodelbase;



import org.openatom.ubml.model.vo.definition.common.VMParameterMode;

/**
 * The Definition Of View Model Return Value
 *
 * @ClassName: ViewModelReturnValue
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelReturnValue extends ViewModelParameter
{
	/** 
	 参数名
	 
	*/
	 @Override
	 public String getParamName(){
	 	return null;
	 }
	@Override
	public void setParamName(String value)
	{

	}

	/** 
	 参数模式
	 
	*/
	 @Override
     public VMParameterMode getMode(){
	 	return VMParameterMode.OUT;
	 }
	@Override
	public void setMode(VMParameterMode value)
	{
	}
}
package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.var;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameter;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
import org.openatom.ubml.model.vo.definition.json.mapping.ViewModelMappingSerializer;
/**
 * The Josn Serializer Of View Model Action
 *
 * @ClassName: VmActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class VmActionSerializer<T extends ViewModelAction> extends JsonSerializer<T> {
    @Override
    public void serialize(T value, JsonGenerator writer, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, value.getID());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.CODE, value.getCode());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.NAME, value.getName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentName, value.getComponentName());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.TYPE, value.getType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsAutoSave, value.getIsAutoSave());
        SerializerUtils.writePropertyValue(writer, CefNames.CustomizationInfo,value.getCustomizationInfo());
        writeParameters(writer, value);
        writeReturnValue(writer, value);
        writeMapping(writer, value);
        writeExtendProperties(writer, value);

        //扩展模型属性
        writeExtendOperationProperty(writer, value);

        SerializerUtils.writeEndObject(writer);
    }

    private void writeParameters(JsonGenerator writer, ViewModelAction action) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ParameterCollection);
        //[
        SerializerUtils.WriteStartArray(writer);
        if (action.getParameterCollection().getCount() > 0) {
            for (var item : action.getParameterCollection()) {
              getParaConvertor().serialize((ViewModelParameter)item, writer, null);
            }
        }
        //]
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeReturnValue(JsonGenerator writer, ViewModelAction action) {
        if (action == null || action.getReturnValue() == null)
            return;

        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ReturnValue);
        VmParameterSerializer convertor = new VmParameterSerializer();
        convertor.serialize(action.getReturnValue(), writer, null);

    }

    private void writeMapping(JsonGenerator writer, ViewModelAction action) {
        if (action.getMapping() == null) {
            return;
        }
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Mapping);
        ViewModelMappingSerializer convertor = new ViewModelMappingSerializer();
        convertor.serialize(action.getMapping(), writer, null);
    }

    private void writeExtendProperties(JsonGenerator writer, ViewModelAction vm) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ExtendProperties);
        var dic = vm.getExtendProperties();
        SerializerUtils.writeStartObject(writer);
        if (dic != null && dic.size() > 0) {
            for (var item : dic.entrySet()) {
                SerializerUtils.writePropertyValue(writer, item.getKey(), item.getValue());
            }
        }
        SerializerUtils.writeEndObject(writer);
    }


    protected abstract VmParameterSerializer getParaConvertor();

    /**
     * 序列化子类扩展信息
     * @param writer
     * @param op
     */
    protected abstract void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op);
}

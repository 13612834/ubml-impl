package org.openatom.ubml.model.vo.definition.action.mappedbiz;

import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameter;

/**
 * The Definition Of Mapped Be Action Parameter
 *
 * @ClassName: MappedBizActionParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedBizActionParameter extends ViewModelParameter
{
	public MappedBizActionParameter()
	{

	}
}
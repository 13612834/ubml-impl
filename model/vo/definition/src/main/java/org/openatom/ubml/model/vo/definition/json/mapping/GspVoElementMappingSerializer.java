package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementMapping;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The Josn Serializer Of View Model Element Mapping
 *
 * @ClassName: GspVoElementMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMappingSerializer extends ViewModelMappingSerializer{

    @Override
    protected final void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
        GspVoElementMapping voMapping =(GspVoElementMapping)mapping;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.SourceType, voMapping.getSourceType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetObjectId, voMapping.getTargetObjectId());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetElementId, voMapping.getTargetElementId());
    }
}

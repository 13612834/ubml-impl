package org.openatom.ubml.model.vo.definition.json.operation;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
import org.openatom.ubml.model.vo.definition.action.ViewModelActionType;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameter;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameterCollection;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.json.ExtendPropertiesDeserializer;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
import org.openatom.ubml.model.vo.definition.json.mapping.ViewModelMappingDeserializer;
/**
 * The Josn Deserializer Of View Model Action
 *
 * @ClassName: VmActionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class VmActionDeserializer<T extends ViewModelAction> extends JsonDeserializer<T> {
    @Override
    public final T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeBizOperation(jsonParser);
    }

    private final T deserializeBizOperation(JsonParser jsonParser) {
        T op = createOp();

        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(op, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);

        return op;
    }

    private void readPropertyValue(ViewModelAction op, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CommonModelNames.ID:
                op.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CommonModelNames.CODE:
                op.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CommonModelNames.NAME:
                op.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case ViewModelJsonConst.Type:
                op.setType(SerializerUtils.readPropertyValue_Enum(jsonParser, ViewModelActionType.class, ViewModelActionType.values(), ViewModelActionType.BEAction));
                break;
            case ViewModelJsonConst.ParameterCollection:
                readParameters(jsonParser, op);
                break;
            case ViewModelJsonConst.ReturnValue:
                readReturnValue(jsonParser, op);
                break;
            case ViewModelJsonConst.Mapping:
                readMapping(jsonParser, op);
                break;
            case ViewModelJsonConst.ComponentName:
                op.setComponentName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case ViewModelJsonConst.ExtendProperties:
                readExtendProperties(jsonParser, op);
                break;
            case ViewModelJsonConst.IsAutoSave:
                op.setIsAutoSave(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.CustomizationInfo:
                op.setCustomizationInfo(SerializerUtils.readPropertyValue_Object(CustomizationInfo.class,jsonParser));
                try {
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw new RuntimeException(String.format("BizOperationDeserializer反序列化错误：%1$s", propName));
                }
                break;
            default:
                if (!readExtendOpProperty(op, propName, jsonParser)) {
                    throw new RuntimeException(String.format("BizOperationDeserializer未识别的属性名：%1$s", propName));
                }
        }
    }

    private void readExtendProperties(JsonParser jsonParser, ViewModelAction action) {
        ExtendPropertiesDeserializer deserializer = new ExtendPropertiesDeserializer();
        action.setExtendProperties(deserializer.deserialize(jsonParser, null));
    }

    private void readParameters(JsonParser jsonParser, ViewModelAction op) {
        VmParameterDeserializer parameterDeserializer = createPrapDeserializer();
        ViewModelParameterCollection<ViewModelParameter> collection = createPrapCollection();
        SerializerUtils.readArray(jsonParser, parameterDeserializer, collection);
        for (ViewModelParameter para : collection) {
            op.getParameterCollection().add(para);
        }
    }

    private void readReturnValue(JsonParser jsonParser, ViewModelAction op) {
        VmReturnValueDeserializer deserializer = new VmReturnValueDeserializer();
        op.setReturnValue(deserializer.deserializePara(jsonParser));
    }

    private void readMapping(JsonParser jsonParser, ViewModelAction op) {
        ViewModelMappingDeserializer deserializer = new ViewModelMappingDeserializer();
        ViewModelMapping mapping = deserializer.deserialize(jsonParser, null);
        op.setMapping(mapping);
    }

    protected boolean readExtendOpProperty(ViewModelAction op, String propName, JsonParser jsonParser) {
        return false;
    }

    protected abstract T createOp();

    protected abstract VmParameterDeserializer createPrapDeserializer();

    protected abstract ViewModelParameterCollection createPrapCollection();
}

package org.openatom.ubml.model.vo.definition.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import lombok.var;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonElement;
import org.openatom.ubml.model.common.definition.commonmodel.json.element.CmElementSerializer;
import org.openatom.ubml.model.vo.definition.GspViewModelElement;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
import org.openatom.ubml.model.vo.definition.json.mapping.GspVoElementMappingSerializer;
import org.openatom.ubml.model.vo.definition.json.operation.VmActionCollectionSerializer;
/**
 * The  Josn Serializer Of View Model Elemnet
 *
 * @ClassName: ViewElementSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewElementSerializer extends CmElementSerializer {

    @Override
    protected void writeExtendElementBaseProperty(JsonGenerator jsonGenerator, IGspCommonElement iGspCommonElement) {

    }

    @Override
    protected void writeExtendElementSelfProperty(JsonGenerator writer, IGspCommonElement commonElement) {
        GspViewModelElement bizElement = (GspViewModelElement) commonElement;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsBeckendOnly, bizElement.getIsBeckendOnly());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ImmediateSubmission, bizElement.getImmediateSubmission());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsVirtualViewElement, bizElement.getIsVirtualViewElement());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.EnableMultiLanguageInput, bizElement.isEnableMultiLanguageInput());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ShowInFilter, bizElement.getShowInFilter());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ShowInSort, bizElement.getShowInSort());
        writeMapping(writer, bizElement);
        writeVMActionCollection(writer, bizElement);
        writeVMHelpConfig(writer, bizElement);
        writeExtendProperties(writer, bizElement);
    }

    private void writeMapping(JsonGenerator writer, GspViewModelElement ve) {
        if (ve.getMapping() == null)
            return;

        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.Mapping);
        GspVoElementMappingSerializer convertor = new GspVoElementMappingSerializer();
        convertor.serialize(ve.getMapping(), writer, null);
    }

    private void writeVMActionCollection(JsonGenerator writer, GspViewModelElement ve) {
        if (ve.getHelpActions() == null)
            return;

        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.HelpActions);
        VmActionCollectionSerializer convertor = new VmActionCollectionSerializer();
        convertor.serialize(ve.getHelpActions(), writer, null);
    }

    private void writeVMHelpConfig(JsonGenerator writer, GspViewModelElement ve) {
        if (ve.getVMHelpConfig() == null)
            return;
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.VMHelpConfig);
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.HelpId, ve.getVMHelpConfig().getHelpId());
        SerializerUtils.writeEndObject(writer);
    }

    private void writeExtendProperties(JsonGenerator writer, GspViewModelElement vm) {
        SerializerUtils.writePropertyName(writer, ViewModelJsonConst.ExtendProperties);
        var dic = vm.getExtendProperties();
        SerializerUtils.writeStartObject(writer);
        if (dic != null && dic.size() > 0) {
            for (var item : dic.entrySet()) {
                SerializerUtils.writePropertyValue(writer, item.getKey(), item.getValue());
            }
        }
        SerializerUtils.writeEndObject(writer);
    }
}

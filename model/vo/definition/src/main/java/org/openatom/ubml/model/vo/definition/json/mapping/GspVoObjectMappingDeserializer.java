package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

import org.openatom.ubml.model.vo.definition.common.mapping.GspVoObjectMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoObjectSourceType;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;


/**
 * The  Josn Deserializer Of View Model Object Mapping
 *
 * @ClassName: GspVoObjectMappingDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoObjectMappingDeserializer extends ViewModelMappingDeserializer {
    @Override
    protected GspVoObjectMapping createVmMapping() {
        return new GspVoObjectMapping();
    }

    @Override
    protected boolean readExtendMappingProperty(JsonParser reader, ViewModelMapping mapping, String propertyName) {
        boolean hasProperty = true;
        GspVoObjectMapping voMapping = (GspVoObjectMapping) mapping;
        switch (propertyName) {
            case ViewModelJsonConst.SourceType:
                voMapping.setSourceType(SerializerUtils
                    .readPropertyValue_Enum(reader, GspVoObjectSourceType.class, GspVoObjectSourceType.values(), GspVoObjectSourceType.VoObject));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}

package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelReturnValue;
import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelVoidReturnValue;
/**
 * The Josn Deserializer Of View Model Action Return Value
 *
 * @ClassName: VmReturnValueDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VmReturnValueDeserializer extends VmParameterDeserializer<ViewModelReturnValue> {
    @Override
    protected ViewModelReturnValue createVmPara() {
        return new ViewModelReturnValue();
    }

    @Override
    public ViewModelReturnValue deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        ViewModelReturnValue rez = (ViewModelReturnValue) super.deserialize(jsonParser, null);
        if (ViewModelVoidReturnValue.assembly.equals(rez.getAssembly())
                && ViewModelVoidReturnValue.className.equals(rez.getClassName())) {
            ViewModelReturnValue newRez = new ViewModelVoidReturnValue();
            newRez.setID(rez.getID());
            newRez.setParamCode(rez.getParamCode());
            newRez.setParamDescription(rez.getParamDescription());
            newRez.setParameterType(rez.getParameterType());
            return newRez;
        }
        return rez;
    }
}

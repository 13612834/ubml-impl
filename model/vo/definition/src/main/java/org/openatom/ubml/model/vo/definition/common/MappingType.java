package org.openatom.ubml.model.vo.definition.common;

/**
 * The Definition Of View Model Mapping Type
 *
 * @ClassName: MappingType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum MappingType
{
	/** 
	 Vo
	 
	*/
	ViewObject,

	/** 
	 BE 
	 
	*/
	BizEntity,

	/** 
	 DM
	 
	*/
	DataModel,

	/** 
	 BE节点
	 
	*/
	BEObject,

	/** 
	 GspDataModel节点
	 
	*/
	DMObject,

	/** 
	 节点元素
	 
	*/
	Element,

	/** 
	 BE  操作
	 
	*/
	BizOperation,

	/** 
	 构件方法
	 
	*/
	ComponentMethod,

	/** 
	 数据对象
	 
	*/
	DataObject,

	/** 
	 数据对象列
	 
	*/
	GSPColumn;


	public int getValue()
	{
		return this.ordinal();
	}

	public static MappingType forValue(int value)
	{
		return values()[value];
	}
}
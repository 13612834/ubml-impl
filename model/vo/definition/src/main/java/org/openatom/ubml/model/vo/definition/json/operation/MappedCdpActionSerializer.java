package org.openatom.ubml.model.vo.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
import org.openatom.ubml.model.vo.definition.action.ViewModelAction;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
/**
 * The Json Serializer Of Mapped Component Action Definition
 *
 * @ClassName: MappedCdpActionSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MappedCdpActionSerializer extends VmActionSerializer<MappedCdpAction> {
    @Override
    protected VmParameterSerializer getParaConvertor() {
        return new MappedCdpParaSerializer();
    }

    @Override
    protected void writeExtendOperationProperty(JsonGenerator writer, ViewModelAction op) {
        MappedCdpAction action = (MappedCdpAction) op;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentEntityId, action.getComponentEntityId());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.ComponentPkgName, action.getComponentPkgName());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.IsGenerateComponent, action.getIsGenerateComponent());
    }
}

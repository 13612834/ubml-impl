package org.openatom.ubml.model.vo.definition.action;

import java.io.Serializable;

/**
 * The Definition Of The View Model Action Result
 *
 * @ClassName: VMActionResult
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VMActionResult implements Cloneable, Serializable
{
		///#region 属性

	/** 
	 类型名称
	 
	*/
	private String privateTypeName;
	public final String getTypeName()
	{
		return privateTypeName;
	}
	public final void setTypeName(String value)
	{
		privateTypeName = value;
	}

		///#endregion

		///#region 方法

	/** 
	 克隆
	 
	 @return Action执行结果
	*/
	public final VMActionResult clone()
	{
//		return MemberwiseClone();
		Object tempVar = null;
		try {
			tempVar = super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		VMActionResult obj = (VMActionResult) ((tempVar instanceof ActionFormatParameter) ? tempVar : null);

		return obj;

		///#endregion
}
}
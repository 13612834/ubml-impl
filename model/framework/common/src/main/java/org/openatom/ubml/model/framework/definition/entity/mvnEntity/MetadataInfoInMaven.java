/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.framework.definition.entity.mvnEntity;

public class MetadataInfoInMaven {

    protected String id;
    protected String metadataId;
    protected String metadataCode;
    protected String metadataName;
    protected String metadataNamespace;
    protected String metadataType;
    protected String mdBizobjectId;
    protected String metadataLanguage;

    protected String metadataIsTranslating;
    protected String metadataPackageCode;
    protected String metadataPackageVersion;
    protected String mavenPackageGroupId;
    protected String mavenPackageArtifactId;
    protected String mavenPackageVersion;
    protected String mavenPackageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetadataId() {
        return metadataId;
    }

    public void setMetadataId(String metadataId) {
        this.metadataId = metadataId;
    }

    public String getMetadataCode() {
        return metadataCode;
    }

    public void setMetadataCode(String metadataCode) {
        this.metadataCode = metadataCode;
    }

    public String getMetadataName() {
        return metadataName;
    }

    public void setMetadataName(String metadataName) {
        this.metadataName = metadataName;
    }

    public String getMetadataNamespace() {
        return metadataNamespace;
    }

    public void setMetadataNamespace(String metadataNamespace) {
        this.metadataNamespace = metadataNamespace;
    }

    public String getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(String metadataType) {
        this.metadataType = metadataType;
    }

    public String getMdBizobjectId() {
        return mdBizobjectId;
    }

    public void setMdBizobjectId(String mdBizobjectId) {
        this.mdBizobjectId = mdBizobjectId;
    }

    public String getMetadataLanguage() {
        return metadataLanguage;
    }

    public void setMetadataLanguage(String metadataLanguage) {
        this.metadataLanguage = metadataLanguage;
    }

    public String getMetadataIsTranslating() {
        return metadataIsTranslating;
    }

    public void setMetadataIsTranslating(String metadataIsTranslating) {
        this.metadataIsTranslating = metadataIsTranslating;
    }

    public String getMetadataPackageCode() {
        return metadataPackageCode;
    }

    public void setMetadataPackageCode(String metadataPackageCode) {
        this.metadataPackageCode = metadataPackageCode;
    }

    public String getMetadataPackageVersion() {
        return metadataPackageVersion;
    }

    public void setMetadataPackageVersion(String metadataPackageVersion) {
        this.metadataPackageVersion = metadataPackageVersion;
    }

    public String getMavenPackageGroupId() {
        return mavenPackageGroupId;
    }

    public void setMavenPackageGroupId(String mavenPackageGroupId) {
        this.mavenPackageGroupId = mavenPackageGroupId;
    }

    public String getMavenPackageArtifactId() {
        return mavenPackageArtifactId;
    }

    public void setMavenPackageArtifactId(String mavenPackageArtifactId) {
        this.mavenPackageArtifactId = mavenPackageArtifactId;
    }

    public String getMavenPackageVersion() {
        return mavenPackageVersion;
    }

    public void setMavenPackageVersion(String mavenPackageVersion) {
        this.mavenPackageVersion = mavenPackageVersion;
    }

    public String getMavenPackageUrl() {
        return mavenPackageUrl;
    }

    public void setMavenPackageUrl(String mavenPackageUrl) {
        this.mavenPackageUrl = mavenPackageUrl;
    }

}

/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.framework.generator.api.common;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import org.openatom.ubml.model.framework.generator.api.entity.JitCompilerConfigration;
import org.openatom.ubml.model.framework.generator.api.temp.EnvironmentUtil;

/**
 * @Classname JitCompilerConfigLoader
 * @Description 反序列化JitCompilerConfigration到实体
 * @Date 2019/7/29 16:09
 * @Created by zhongchq
 * @Version 1.0
 */
public class JitCompilerConfigLoader {

    private static String fileName = "config/platform/common/lcm_gspprojectextend.json";
    private static String sectionName = "JitCompilerConfigration";
    private static List<JitCompilerConfigration> compilerTypeConfigurations;

    ObjectMapper objectMapper = new ObjectMapper();

    public JitCompilerConfigLoader() {

        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static List<JitCompilerConfigration> getCompilerTypeConfigurations() {
        return compilerTypeConfigurations;
    }

    public static void setCompilerTypeConfigurations(
            List<JitCompilerConfigration> compilerTypeConfigurations) {
        JitCompilerConfigLoader.compilerTypeConfigurations = compilerTypeConfigurations;
    }

    protected List<JitCompilerConfigration> getCompilerConfigurations() {
        fileName = Paths.get(EnvironmentUtil.getServerRTPath()).resolve(fileName).toString();

        try {
            if (compilerTypeConfigurations == null || compilerTypeConfigurations.size() <= 0) {
                String fileContents = FileUtils.fileRead(fileName);

                JsonNode jsonNode = objectMapper.readTree(fileContents);
                String metadataConfiguration = jsonNode.findValue(sectionName).toString();
                compilerTypeConfigurations = objectMapper.readValue(metadataConfiguration, new TypeReference<List<JitCompilerConfigration>>() {
                });
                return compilerTypeConfigurations;
            } else {
                return compilerTypeConfigurations;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected JitCompilerConfigration getCompilerConfigurations(String typeName) {
        getCompilerConfigurations();
        if (compilerTypeConfigurations != null || compilerTypeConfigurations.size() > 0) {
            for (JitCompilerConfigration data : compilerTypeConfigurations)
                if (data.getTypeCode().toLowerCase().equals(typeName.toLowerCase())) {
                    return data;
                }
            return null;
        } else {
            return null;
        }
    }

}

/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.externalapi.definition.temp.lcm;

import org.openatom.ubml.model.common.definition.entity.GspMetadata;

/**
 * The type CustomizationService
 *
 * @author: haozhibei
 */
public class CustomizationService {
    public GspMetadata getMetadata(String id) {
        return null;
    }

    public void saveExtMetadataWithDimensions(DimensionExtendEntity entity) {

    }

    public void releaseMetadataToRt(String id, Object o) {

    }

    public void save(GspMetadata metadata) {

    }
}

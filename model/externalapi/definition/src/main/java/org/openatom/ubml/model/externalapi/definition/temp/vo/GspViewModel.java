/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.externalapi.definition.temp.vo;

import java.util.List;

/**
 * The type GspViewModel
 *
 * @author: haozhibei
 */
public class GspViewModel {
    public GspViewObject getMainObject() {
        return null;
    }

    public String getId() {
        return null;
    }

    public List<ViewModelAction> getActions() {
        return null;
    }

    public String getCode() {
        return null;
    }

    public String getID() {
        return null;
    }

    public String getGeneratedConfigID() {
        return null;
    }

    public String getName() {
        return null;
    }
}

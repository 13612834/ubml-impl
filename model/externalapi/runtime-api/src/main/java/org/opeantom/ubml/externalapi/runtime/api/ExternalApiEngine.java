package org.opeantom.ubml.externalapi.runtime.api;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;

/**
 * The unified engine for external api invoking.
 *
 * @Author: Fynn Qi
 * @Date: 2019/7/22 10:33
 * @Version: V1.0
 */
public interface ExternalApiEngine {

    /**
     * Invoke EApi service using JSON payload
     *
     * @param resourceType Type of EApi adaptation resources
     * @param resourceId   resourceId
     * @param resourceOpId resourceOpId
     * @param paramList    list of param(json type)
     * @return the result of invoke
     */
    Object invokeByJsonNode(String resourceType, String resourceId, String resourceOpId, List<JsonNode> paramList);

    /**
     * Invoke EApi service using Object payload
     *
     * @param resourceType Type of EApi adaptation resources
     * @param resourceId   resourceId
     * @param resourceOpId resourceOpId
     * @param paramList    list of param(object type)
     * @return the result of invoke
     */
    Object invokeByObject(String resourceType, String resourceId, String resourceOpId, List<Object> paramList);

}

package org.opeantom.ubml.externalapi.runtime.api;

/**
 * @ClassName: ExternalApiPublishService
 * @Author: Fynn Qi
 * @Date: 2020/6/23 18:07
 * @Version: V1.0
 */
public interface ExternalApiPublishService {

    /**
     * 编译并发布服务
     *
     * @param metadataId
     */
    void publish(String metadataId);

    /**
     * 移除发布的服务
     *
     * @param metadataId
     */
    void unpublish(String metadataId);
}

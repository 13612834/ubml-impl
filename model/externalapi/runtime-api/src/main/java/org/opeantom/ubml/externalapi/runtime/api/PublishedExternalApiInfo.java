package org.opeantom.ubml.externalapi.runtime.api;


/**
 * PublishedServiceInfo
 *
 * @author haozhibei
 */
public class PublishedExternalApiInfo {

    private String id;

    private String version;

    private EndpointInfo endpoint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public EndpointInfo getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(EndpointInfo endpoint) {
        this.endpoint = endpoint;
    }
}

package org.opeantom.ubml.externalapi.runtime.api;

import org.openatom.ubml.model.common.definition.entity.GspMetadata;
import org.openatom.ubml.model.externalapi.definition.entity.ExternalApi;
import org.openatom.ubml.model.externalapi.definition.temp.vo.DimensionInfo;

/**
 * The ExternalApiService
 *
 * @author haozhibei
 */
public interface ExternalApiMetadataService {

    /**
     * 创建运行时定制元数据
     *
     * @param parentEapiId  上一级Eapi元数据ID
     * @param voMetadata    当前层级的VO元数据
     * @param dimensionInfo 维度信息
     * @return 返回值
     */
    ExternalApi create(String parentEapiId, GspMetadata voMetadata, DimensionInfo dimensionInfo);

    /**
     * 删除元数据
     *
     * @param eapiId EApi元数据ID
     */
    void delete(String eapiId);

    /**
     * 更新元数据
     *
     * @param parentEapiId  上一级EApi元数据ID
     * @param eapiId        当前层级的EApi元数据ID
     * @param voMetadata    当前层级的VO元数据
     * @param dimensionInfo 维度信息
     */
    void update(String parentEapiId, String eapiId, GspMetadata voMetadata, DimensionInfo dimensionInfo);

    /**
     * 运行时定制元数据发布：由设计时发布为运行时
     *
     * @param eapiId EApi元数据ID
     */
    void publish(String eapiId);

}

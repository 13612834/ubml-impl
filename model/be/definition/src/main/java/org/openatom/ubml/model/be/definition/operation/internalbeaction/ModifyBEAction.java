/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.internalbeaction;

import org.openatom.ubml.model.be.definition.operation.BizAction;

/**
 * The Internal Modify Action
 *
 * @ClassName: ModifyBEAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ModifyBEAction extends BizAction implements IInternalBEAction {
    public static final String id = "tf9MVINdJ0yhUrsJgfcvBA";
    public static final String code = "Modify";
    public static final String name = "内置修改操作";

    public ModifyBEAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;
import org.openatom.ubml.model.be.definition.GspBusinessEntity;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.json.object.BizObjectSerializer;
import org.openatom.ubml.model.be.definition.json.operation.BizMgrActionCollectionSerializer;
import org.openatom.ubml.model.be.definition.operation.collection.BizMgrActionCollection;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonModel;
import org.openatom.ubml.model.common.definition.commonmodel.json.model.CommonModelSerializer;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectSerializer;

/**
 * The  Josn Serializer Of Business Entity
 *
 * @ClassName: BizEntitySerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizEntitySerializer extends CommonModelSerializer {

    //region BaseProp
    @Override
    protected void writeExtendModelProperty(IGspCommonModel commonModel, JsonGenerator writer) {
        GspBusinessEntity be = (GspBusinessEntity)commonModel;
        writeBizActions(writer, be);
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DependentEntityId, be.getDependentEntityId());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DependentEntityName, be.getDependentEntityName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DependentEntityPackageName, be.getDependentEntityPackageName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CacheConfiguration, be.getCacheConfiguration());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.EnableCaching, be.getEnableCaching());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.EnableTreeDtm, be.getEnableTreeDtm());

        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ComponentAssemblyName, be.getComponentAssemblyName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Category, be.getCategory().getValue());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DataLockType, be.getDataLockType().getValue());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ExtendType, be.getExtendType());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsUsingTimeStamp, be.getIsUsingTimeStamp());
//        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Authorizations, be.getAuthorizations());
    }

    private void writeBizActions(JsonGenerator writer, GspBusinessEntity be) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.BizMgrActions);
        BizMgrActionCollection actionList = be.getBizMgrActions();
        BizMgrActionCollectionSerializer operationsConvertor = getMgrActionsConverter();
        try {
            operationsConvertor.serialize(actionList, writer, null);
        } catch (IOException e) {
            throw new RuntimeException("自定义动作序列化失败", e);
        }
    }

    private BizMgrActionCollectionSerializer getMgrActionsConverter() {
        return new BizMgrActionCollectionSerializer();
    }
    //endregion

    @Override
    protected void writeExtendModelSelfProperty(IGspCommonModel iGspCommonModel, JsonGenerator jsonGenerator) {

    }

    @Override
    protected final CmObjectSerializer getCmObjectSerializer() {
        return new BizObjectSerializer();
    }
}

/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.beenum;

/**
 * The Type Of Business Entity,It Can Be Standard Dependent Or Max.
 *
 * @ClassName: BECategory
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum BECategory {
    /**
     * ��׼BE
     */
    Standard(0),
    /**
     * DBO,����Ԫ�������õ�Ƕ��ʽBE,��Ҫ����HO����
     */
    DependentBusinessEntity(1),
    /**
     * ����BE�Ĺ������ֳ���Ĵ󼯺�
     */
    MaxBusinessEntity(2);

    private static java.util.HashMap<Integer, BECategory> mappings;
    private int intValue;

    private BECategory(int value) {
        intValue = value;
        BECategory.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, BECategory> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, BECategory>();
        }
        return mappings;
    }

    public static BECategory forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
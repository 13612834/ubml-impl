/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.componentbase;

import java.util.ArrayList;
import java.util.Iterator;
import org.openatom.ubml.model.be.definition.operation.componentinterface.IBizParameter;
import org.openatom.ubml.model.be.definition.operation.componentinterface.IBizParameterCollection;

/**
 * The Collectioin Of Biz  Parameter
 *
 * @ClassName: BizParameterCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizParameterCollection<T extends IBizParameter> extends ArrayList<T> implements
        IBizParameterCollection<T> {
    private java.util.ArrayList<T> compParameters = new java.util.ArrayList<T>();

    public BizParameterCollection() {
    }

    // region IList成员
    public final T getItem(int index) {
        if (index > -1 && index < getCount()) {
            return compParameters.get(index);
        }
        return null;
    }

    public final void setItem(int index, T value) {
        compParameters.set(index, value);
    }

    /**
     * 新增参数
     *
     * @param item
     */
    public final boolean add(IBizParameter item) {
        return compParameters.add((T)item);
    }

    public final void insert(int index, IBizParameter item) {
        compParameters.add(index, (T)item);
    }

    public final void removeAt(int index) {
        compParameters.remove(index);
    }

    /**
     * 移除指定的参数
     */
    public final boolean remove(IBizParameter item) {
        boolean flag = false;
        for (int i = 0; i < compParameters.size(); i++) {
            if (compParameters.get(i).equals(item)) {
                compParameters.remove(i);
                flag = true;
                break;
            }
        }
        return flag;
    }

    /**
     * 是否包含指定参数
     *
     * @param item
     * @return
     */
    public final boolean contains(IBizParameter item) {
        return compParameters.contains(item);
    }

    /**
     * 清除所有内容
     */
    public final void clear() {
        compParameters.clear();
    }

    /**
     * 返回指定项目的索引
     *
     * @return 位置
     */
    public final int indexOf(IBizParameter item) {
        for (int i = 0; i < compParameters.size(); i++) {
            if (compParameters.get(i).equals(item)) {
                return i;
            }
        }
        return -1;
    }

    // endregion

    // region ICollection 成员

    public final int getCount() {
        return compParameters.size();
    }

    // endregion
    // region IEnumerator 成员

    // endregion

    // region IEnumerator 成员
    // public final java.util.Iterable<T> GetEnumerator()
    // {
    // for (IBizParameter item : compParameters)
    // {
    // yield return item;
    // }
    // }

    // private java.util.Iterable GetEnumerator()
    // {
    // return GetEnumerator();
    // }

    // //#endregion
    // private IBizParameter getItem(int index)
    // {
    // return getthis()[index];
    // }
    // private void add(IBizParameter item)
    // {
    // getthis().add((T)item);
    // }

    // private void insert(int index, IBizParameter item)
    // {
    // getthis().insert(index, (T)item);
    // }

    // private boolean remove(IBizParameter item)
    // {
    // return getthis().remove((T)item);
    // }

    // private boolean contains(IBizParameter item)
    // {
    // return getthis().contains((T)item);
    // }

    // private int indexOf(IBizParameter item)
    // {
    // return getthis().indexOf((T)item);
    // }

    // private java.util.Iterable<IBizParameter> getEnumerator()
    // {
    // return (java.util.Iterable<IBizParameter>)GetEnumerator();
    // }

    @Override
    public Iterator<T> iterator() {
        return compParameters.iterator();
    }
}
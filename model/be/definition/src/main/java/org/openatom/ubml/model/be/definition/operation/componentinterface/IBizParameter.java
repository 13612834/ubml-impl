/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.componentinterface;

import org.openatom.ubml.model.be.definition.operation.componentenum.BizCollectionParameterType;
import org.openatom.ubml.model.be.definition.operation.componentenum.BizParameterMode;
import org.openatom.ubml.model.be.definition.operation.componentenum.BizParameterType;

/**
 * The Interface Of Biz Parameter Definition
 *
 * @ClassName: IBizParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IBizParameter {
    /**
     * @return The Id  Of BizParameter ,It`s An UUID
     */
    String getID();

    /**
     * @param value The Id  Of BizParameter ,It`s An UUID
     */
    void setID(String value);

    /**
     * @return The Code  Of BizParameter
     */
    String getParamCode();

    /**
     * @param value The Code  Of BizParameter
     */
    void setParamCode(String value);

    /**
     * @return The Name  Of BizParameter
     */
    String getParamName();

    /**
     * @param value The Name  Of BizParameter
     */
    void setParamName(String value);

    /**
     * @return The Type  Of BizParameter
     */
    BizParameterType getParameterType();

    /**
     * @param value The Type  Of BizParameter
     */
    void setParameterType(BizParameterType value);

    /**
     * @return The Collection Type  Of BizParameter
     */
    BizCollectionParameterType getCollectionParameterType();

    /**
     * @param value The Collection Type  Of BizParameter
     */
    void setCollectionParameterType(BizCollectionParameterType value);

    /**
     * @return The Dotnet Assembly Name Of The Parameter Type,But It`s Useless Now
     */
    String getAssembly();

    /**
     * @param value The Dotnet Assembly Name Of The Parameter Type,But It`s Useless Now
     */
    void setAssembly(String value);

    /**
     * @return The Java Class Name Of The Parameter
     */
    String getClassName();

    /**
     * @param value The Java Class Name Of The Parameter
     */
    void setClassName(String value);

    /**
     * @return The Parameter Mode Of The Parameter,The Default Is In
     */
    BizParameterMode getMode();

    /**
     * @param value The Parameter Mode Of The Parameter,The Default Is In
     */
    void setMode(BizParameterMode value);

    /**
     * @return The Description Of The Parameter
     */
    String getParamDescription();

    /**
     * @param value The Description Of The Parameter
     */
    void setParamDescription(String value);

}
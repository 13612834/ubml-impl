/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.beenum;

/**
 * 权限控制类型
 * <p>
 * <p>
 * 控制当前操作使用的权限控制策略
 */
public enum AuthType {
    /**
     * 使用传递的权限，不限制权限，Action没有绑定的业务操作
     */
    TransAuth(0),
    /**
     * 使用传递的权限，并且限制在Action所绑定的业务操作内
     */
    TransAndInsideAuth(1),
    /**
     * 不使用传递的权限，使用Action绑定的业务操作（只绑定一个业务操作）
     */
    MustHasAuth(2);

    private static java.util.HashMap<Integer, AuthType> mappings;
    private int intValue;

    private AuthType(int value) {
        intValue = value;
        AuthType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, AuthType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, AuthType>();
        }
        return mappings;
    }

    public static AuthType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.be.definition.GspBizEntityElement;
import org.openatom.ubml.model.be.definition.common.BizEntityEnumConst;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonElement;
import org.openatom.ubml.model.common.definition.commonmodel.json.element.CmElementSerializer;

/**
 * The  Josn Serializer Of Be Element
 *
 * @ClassName: BizElementDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizElementSerializer extends CmElementSerializer {

    @Override
    protected void writeExtendElementBaseProperty(JsonGenerator jsonGenerator, IGspCommonElement iGspCommonElement) {

    }

    @Override
    protected void writeExtendElementSelfProperty(JsonGenerator writer, IGspCommonElement commonElement) {
        GspBizEntityElement bizElement = (GspBizEntityElement)commonElement;
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CalculationExpress, bizElement.getCalculationExpress());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ValidationExpress, bizElement.getValidationExpress());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.RtElementConfigId, bizElement.getRtElementConfigId());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsDefaultNull, bizElement.getIsDefaultNull());
        writeUnifiedDataTypeDef(writer, bizElement);
        writeReqireCheckOccasion(writer, bizElement);
    }


    //TODO 需要统一加，设计器用
    private void writeUnifiedDataTypeDef(JsonGenerator writer, GspBizEntityElement bizElement) {
//        //udt属性仅前端使用
//        if (Context.Mode == SerializerMode.Content)
//        {
//            return;
//        }

//        if (bizElement.getUnifiedDataType() == null) {
//            return;
//        }
//        // 属性名 UnifiedDataType
//        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.UnifiedDataType);
//
//        //属性值
//        SimpleDataTypeConverter serializer = new SimpleDataTypeConverter();
//        serializer.WriteJson(writer, bizElement.getUnifiedDataType(), null);
    }

    private void writeReqireCheckOccasion(JsonGenerator writer, GspBizEntityElement bizElement) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.RequiredCheckOccasion);
        String value;
        switch (bizElement.getRequiredCheckOccasion()) {
            case All:
                value = BizEntityEnumConst.RequiredCheckOccasionAll;
                break;
            case Save:
                value = BizEntityEnumConst.RequiredCheckOccasionSave;
                break;
            case Modify:
                value = BizEntityEnumConst.RequiredCheckOccasionModify;
                break;
            default:
                throw new RuntimeException("未定义的枚举值：" + bizElement.getRequiredCheckOccasion());
        }
        SerializerUtils.writePropertyValue_String(writer, value);
    }
}

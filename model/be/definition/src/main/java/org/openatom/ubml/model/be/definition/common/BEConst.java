/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.common;

/**
 * The Serializer Property Names Of Business Entity
 *
 * @ClassName: BEConst
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public final class BEConst {

    //#region ElementName
    /**
     * BOEntity
     */
    public static final String BO_ENITITY = "BEEntity";
    /**
     * BO
     */
    public static final String BUSINESS_OBJECT = "BusinessObject";
    /**
     * BO Category
     */
    public static final String GSP_BO_CATEGORY = "Category";

    /**
     * BO Category
     */
    public static final String GSP_BO_DBEID = "DependentEntityId";
    public static final String GSP_BO_DBENAME = "DependentEntityName";
    public static final String GSP_BO_DBEPACKAGE = "DependentEntityPackageName";
    public static final String GSP_BO_ISGENERATINGTIMESTAMP = "IsUsingTimeStamp";
    /**
     * OperationList
     */
    public static final String GSP_BO_OPERATION_LIST = "OperationList";
    /**
     * ActionMgrList
     */
    public static final String GSP_BO_MGRACTION_LIST = "MgrActionList";
    /**
     * ActionList
     */
    public static final String GSP_BO_ACTION_LIST = "ActionList";
    /**
     * DeterminationList
     */
    public static final String GSP_BO_DETERMINATION_LIST = "DeterminationList";
    /**
     * ValidationList
     */
    public static final String GSP_BO_VALIDATION_LIST = "ValidationList";
    /**
     * Operation
     */
    public static final String GSP_BO_OPERATION = "Operation";
    /**
     * InternalOperation
     */
    public static final String GSP_BO_OPERATION_INTERNAL = "InternalOperation";
    /**
     * OperationValidateList
     */
    public static final String GSP_BO_OPERATION_VALIDATE_LIST = "ValidateList";
    /**
     * OperationValidate
     */
    public static final String GSP_BO_OPERATION_VALIDATE = "Validate";
    /**
     * RequestNodeConfigList
     */
    public static final String GSP_BO_REQUEST_NODE_CONFIG_LIST = "RequestNodeConfigList";
    /**
     * RequestNodeConfig
     */
    public static final String GSP_BO_REQUEST_NODE_CONFIG = "RequestNodeConfig";
    /**
     * OperationValidate
     */
    public static final String GSP_SAM_ACTION = "Operation";
    /**
     * SAMActionList
     */
    public static final String GSP_SAM_ACTION_LIST = "ActionList";

    /**
     * BONode
     */
    public static final String BE_NODE = "BENode";
    /**
     * BO Node Collection ElementName
     */
    public static final String BE_CHILD_NODE_LIST = "ChildNodeList";
    /**
     * GspBOConstraint
     */
    public static final String BE_CONSTRAINT = "Constraint";
    /**
     * BOAssociationCollection
     */
    public static final String BE_ASSOCIATION_LIST = "AssociationList";
    /**
     * BO Association
     */
    public static final String BE_ASSOCIATION = "Association";
    /**
     * BO AssociationItem
     */
    public static final String BE_ASSOCIATION_ITEM = "AssociationItem";

    /**
     * BOConstraintCollection
     */
    public static final String BE_CONSTRAINT_LIST = "ConstraintList";
    /**
     * BO ElementCollection
     */
    public static final String BE_ELEMENT_LIST = "ElementList";
    /**
     * GspBasicElement
     */
    public static final String BE_ELEMENT = "Element";

    /**
     * 计算表达式
     */
    public static final String BE_CALCULATE_EXPRESS = "CalculateExpression";

    /**
     * 字段是否只读
     */
    public static final String BE_READONLY = "Readonly";

    /**
     * 必填验证时机
     */
    public static final String BE_REQUIRED_CHECK_OCCASION = "RequiredCheckOccasion";

    /**
     * 验证表达式
     */
    public static final String BE_VALIDATE_EXPRESS = "ValidateExpression";

    /**
     * BOExtionsionCollection
     */
    public static final String BE_EXTIONSION_COLLECTION = "ExtionsionList";
    /**
     * GspBOExtensionEntity
     */
    public static final String GSP_BE_EXTENSION_ENTITY = "Extension";

    /**
     * BOExtionsionItem
     */
    public static final String BE_EXTIONSION_ITEM = "BOExtionsionItem";

    //#endregion


    //#region AttributeNames

    //#region Common
    public static final String ID = "ID";
    public static final String CODE = "Code";
    public static final String NAME = "Name";
    public static final String DESCRIPTION = "Description";
    public static final String VISIBLE = "Visible";
    public static final String ORDER = "Order";
    public static final String GENCOMP = "IsGenerateComponent";

    //#endregion


    //#region BE
    /**
     * BEDataLockType
     */
    public static final String BE_DATALOCKTYPE = "DataLockType";

    /**
     * 程序集名
     */
    public static final String BE_CMP_SSEMBLY_NAME = "ComponentAssemblyName";

    //#endregion


    //#region Element
    /**
     * BEElementExpression
     */
    public static final String BE_EXPRESS_SOURCE_ELEMENT = "ExpressionSourceElement";

    public static final String BE_EXPRESS_TARGET_ELEMENT = "ExpressionTargetElement";

    //#endregion


    //#region Object
    /**
     * BEObjectIsRootNode
     */
    public static final String BE_OBJECT_IS_ROOT_NODE = "IsRootNode";

    //#endregion


    //#region Operation
    /**
     * MethodId
     */
    public static final String BE_OP_METHOD_ID = "MethodId";

    /**
     * ComponentId
     */
    public static final String BE_OP_COMPONENT_ID = "ComponentId";

    /**
     * ComponentName
     */
    public static final String BE_OP_COMPONENT_Name = "ComponentName";

    /**
     * ComponentPkgName
     */
    public static final String BE_OP_COMPONENT_PKG_NAME = "ComponentPkgName";

    /**
     * OpType
     */
    public static final String BE_OP_TYPE = "OpType";

    /**
     * BizActionBase_Parameters
     */
    public static final String BE_ACTION_PARAMETERS = "Parameters";

    public static final String BE_ACTION_PARAM_CODE = "ParamCode";
    public static final String BE_ACTION_PARAM_NAME = "ParamName";
    public static final String BE_ACTION_PARAM_TYPE = "ParameterType";
    public static final String BE_ACTION_PARAM_COLLECTIONTYPE = "CollectionParameterType";
    public static final String BE_ACTION_PARAM_ASSEMBLY = "Assembly";
    public static final String BE_ACTION_PARAM_CLASSNAME = "ClassName";
    public static final String BE_ACTION_PARAM_MODE = "Mode";
    public static final String BE_ACTION_PARAM_DESCRIPTION = "ParamDescription";

    /**
     * BelongModelID
     */
    public static final String BE_OP_MODEL_ID = "BelongModelID";

    /**
     * OpType
     */
    public static final String BE_OP_DETERMINATION_TYPE = "DeterminationType";

    /**
     * BETriggerTimePointType
     */
    public static final String BE_TRIGGER_TIME_POINT_TYPE = "BETriggerTimePointType";

    /**
     * RequestNodeTriggerType
     */
    public static final String REQUEST_NODE_TRIGGER_TYPE = "RequestNodeTriggerType";

    public static final String REQUEST_ELEMENTS = "RequestElements";

    public static final String REQUEST_CHILD_ELEMENTS = "RequestChildElements";
    public static final String REQUEST_CHILD_ELEMENT = "RequestChildElement";
    public static final String REQUEST_CHILD_ELEMENT_KEY = "Key";
    public static final String REQUEST_CHILD_ELEMENT_VALUE = "Value";

    public static final String REQUEST_ELEMENTS_ALL = "RequestElementsAll";

    /**
     * ValidationType
     */
    public static final String BE_OP_VALIDATION_TYPE = "ValidationType";

    /**
     * 后置条件列表
     */
    public static final String BE_OP_SUCCEEDING_IDS = "SucceedingIds";

    /**
     * 前置条件列表
     */
    public static final String BE_OP_PRECEDING_IDS = "PrecedingIds";

    public static final String BE_OP_AUTH_TYPE = "AuthType";

    public static final String BE_OP_OPERATION_IDS = "OperationIds";

    public static final String BE_OP_OPERATION_ID = "OperationId";
    public static final String BE_OP_OPERATION_NAME = "OperationName";

    public static final String BE_OP_RETURNVALUE = "ReturnValue";

    /**
     * 校验规则触发时机
     */
    public static final String BE_OP_VALIDATIONTRIGGERPOINTS = "ValidationTriggerPoints";
    public static final String BE_OP_VALIDATIONTRIGGERPOINT = "ValidationTriggerPoint";
    public static final String BE_OP_VALIDATIONTRIGGERPOINT_KEY = "ValidationTriggerPointKey";
    public static final String BE_OP_VALIDATIONTRIGGERPOINT_VALUE = "ValidationTriggerPointValue";

    //#endregion


    //#region Authority
    public static final String BE_AUTH_INFOS = "AuthorizationInfos";
    public static final String BE_AUTH_INFO = "AuthorizationInfo";

    public static final String BE_AUTH_FIELD_INFOS = "AuthFieldInfos";
    public static final String BE_AUTH_FIELD_INFO = "AuthFieldInfo";
    public static final String BE_AUTH_FIELD_ID = "AuthFieldID";
    public static final String BE_AUTH_FIELD_NAME = "AuthFieldName";
    public static final String BE_AUTH_ID = "AuthID";
    public static final String BE_AUTH_ELEMENT_ID = "ElementID";

    public static final String BE_AUTH_ACTION_INFOS = "AuthActionInfos";
    public static final String BE_AUTH_ACTION_INFO = "AuthActionInfo";

    public static final String BE_AUTH_ACTION_ID = "AuthActionID";
    public static final String BE_AUTH_ACTION_OP_ID = "AuthActionOpID";
    public static final String BE_AUTH_ACTION_OP_NAME = "AuthActionOpName";


    //#endregion


    //#endregion
}
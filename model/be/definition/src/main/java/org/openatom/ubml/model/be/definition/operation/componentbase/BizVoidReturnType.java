/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.componentbase;

import org.openatom.ubml.model.be.definition.operation.componentenum.BizParameterType;

/**
 * The Void Return Value Definition
 *
 * @ClassName: BizVoidReturnType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizVoidReturnType extends BizReturnValue {
    public static final String assembly = "mscorlib.dll";
    public static final String className = "Void";

    /**
     * 参数类对应程序集类
     */
    @Override
    public String getAssembly() {
        return null;
    }

    @Override
    public void setAssembly(String value) {
    }

    /**
     * 参数类名
     */
    @Override
    public String getClassName() {
        return null;
    }

    @Override
    public void setClassName(String value) {
    }

    /**
     * 参数类型，与Assembly和ClassName关联
     */
    @Override
    public BizParameterType getParameterType() {
        return BizParameterType.Custom;
    }

    @Override
    public void setParameterType(BizParameterType value) {
    }
}
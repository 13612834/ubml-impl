/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.collection;

import java.util.ArrayList;
import java.util.function.Predicate;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.BizOperationCollection;
import org.openatom.ubml.model.be.definition.operation.Determination;

/**
 * The Collection Of Determination
 *
 * @ClassName: DeterminationCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DeterminationCollection extends BizOperationCollection {
    private static final long serialVersionUID = 1L;

    // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // ORIGINAL LINE: public new void add(BizOperation operation)
    public final boolean add(BizOperation operation) {
        if (operation instanceof Determination) {
            operation.setOwner(getOwner());
            return super.add(operation);
        }
        throw new RuntimeException("错误的Determination类型");
    }

    // /**
    // 克隆BE Determinations动作集合

    // @param isGenerateId 是否生成Id
    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone(bool isGenerateId)
    // public final DeterminationCollection clone(boolean isGenerateId)
    // {
    // DeterminationCollection col = new DeterminationCollection();
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // col.addRange(this.select(item { return
    // (Determination)item.clone(isGenerateId)));
    // return col;
    // }

    // /**
    // 克隆BE Determinations动作集合

    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone()
    // public final Object clone()
    // {
    // return clone(false);
    // }

    @Override
    public DeterminationCollection clone() {
        return (DeterminationCollection)super.clone();
    }

    @Override
    protected Determination convertOperation(BizOperation op) {
        return (Determination)super.convertOperation(op);
    }

    @Override
    protected DeterminationCollection createOperationCollection() {
        return new DeterminationCollection();
    }

    /**
     * 查询符合条件的Node集合
     *
     * @param predicate 查询条件
     * @return 返回结果按照层次遍历顺序
     */
    public final ArrayList<Determination> getFiltedDtms(Predicate<Determination> predicate) {

        ArrayList<Determination> result = new ArrayList<Determination>();
        for (BizOperation op : this) {
            Determination dtm = (Determination)op;
            if (predicate.test(dtm)) {
                result.add(dtm);
            }
        }
        return result;
    }
}
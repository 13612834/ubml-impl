/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation;

import java.util.EnumSet;
import org.openatom.ubml.model.be.definition.beenum.BEOperationType;
import org.openatom.ubml.model.be.definition.beenum.BETriggerTimePointType;
import org.openatom.ubml.model.be.definition.beenum.BEValidationType;
import org.openatom.ubml.model.be.definition.beenum.RequestNodeTriggerType;
import org.openatom.ubml.model.common.definition.cef.collection.ValElementCollection;

/**
 * The Definition Of Be Validation
 *
 * @ClassName: Validation
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class Validation extends BizOperation implements Cloneable {

    private BEValidationType privateValidationType = BEValidationType.forValue(0);
    private EnumSet<BETriggerTimePointType> privateTriggerTimePointType = EnumSet.of(BETriggerTimePointType.forValue(0));
    private EnumSet<RequestNodeTriggerType> privateRequestNodeTriggerType = EnumSet
            .of(RequestNodeTriggerType.forValue(0));
    private ValElementCollection rqtElements;
    private java.util.HashMap<String, ValElementCollection> requestChildElements;
    private java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> validationTriggerPoints;

    public Validation() {
    }

    public BEValidationType getValidationType() {
        return privateValidationType;
    }

    public void setValidationType(BEValidationType value) {
        privateValidationType = value;
    }

    public final EnumSet<BETriggerTimePointType> getTriggerTimePointType() {
        return privateTriggerTimePointType;
    }

    public final void setTriggerTimePointType(EnumSet<BETriggerTimePointType> value) {
        privateTriggerTimePointType = value;
    }

    public EnumSet<RequestNodeTriggerType> getRequestNodeTriggerType() {
        return privateRequestNodeTriggerType;
    }

    public void setRequestNodeTriggerType(EnumSet<RequestNodeTriggerType> value) {
        privateRequestNodeTriggerType = value;
    }

    public final ValElementCollection getRequestElements() {
        if (rqtElements == null) {
            rqtElements = new ValElementCollection();
        }
        return rqtElements;
    }

    /**
     * ����ʱ���������Ҫ�������ֶ�LabelID����
     */
    public final void setRequestElements(ValElementCollection value) {
        rqtElements = value;
    }

    public final java.util.HashMap<String, ValElementCollection> getRequestChildElements() {
        if (requestChildElements == null) {
            requestChildElements = new java.util.HashMap<String, ValElementCollection>();
        }
        return requestChildElements;
    }

    public final void setRequestChildElements(java.util.HashMap<String, ValElementCollection> value) {
        requestChildElements = value;
    }

    public final java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> getValidationTriggerPoints() {
        if (validationTriggerPoints == null) {
            validationTriggerPoints = new java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>>();
        }
        return validationTriggerPoints;
    }

    public final void setValidationTriggerPoints(java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> value) {
        validationTriggerPoints = value;
    }

    // endregion

    // //#region Private Methods

    // private java.util.List<Validation> InitSucceedings()
    // {

    // succeedings = InitValidationsById(getSucceedingIds());
    // return succeedings;
    // }

    // private java.util.List<Validation> InitPrecedings()
    // {
    // precedings = InitValidationsById(getPrecedingIds());
    // return precedings;
    // }

    // private java.util.List<Validation> InitValidationsById(java.util.List<String>
    // idList)
    // {
    // //�������
    // Object tempVar = getOwner().getValidations().firstOrDefault(item { return
    // item.getID(id.equals()));
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // return idList.select(id { return (Validation)((tempVar instanceof Validation)
    // ? tempVar : null)).toList();
    // }

    // //#endregion

    // region ����

    /**
     * �ж����
     *
     * @param obj �����뵱ǰУ�����ȽϵĶ���
     * @return ��ȷ���true
     */
    @Override
    public boolean equals(Object obj) {
        Validation other = (Validation)((obj instanceof Validation) ? obj : null);
        return other != null && ((this == other) || equals(other));
    }

    /**
     * ��дEquals
     *
     * @param other �����뵱ǰУ�����Ƚϵ�У�������
     * @return ��ȷ���true
     */
    protected final boolean equals(Validation other) {
        // return super.equals(other) && getOrder() == other.getOrder() &&
        // other.getValidationType() == getValidationType()
        return super.equals(other) && other.getValidationType() == getValidationType()
                && other.getRequestNodeTriggerType() == getRequestNodeTriggerType();
    }

    /**
     * ��д GetHashCode
     *
     * @return
     */
    @Override
    public int hashCode() {
        {
            // int hashCode = (super.hashCode() * 397) ^ (new
            // Integer(getOrder())).hashCode();
            int hashCode = (super.hashCode() * 397);
            hashCode = (hashCode * 397) ^ getValidationType().hashCode();
            hashCode = (hashCode * 397) ^ getRequestNodeTriggerType().hashCode();
            return hashCode;
        }
    }

    /**
     * @return
     */
    @Override
    public Validation clone() {
        return clone(false);
    }

    /**
     * Clone����
     *
     * @param isGenerateId
     * @return
     */
    @Override
    public Validation clone(boolean isGenerateId) {
        Object tempVar = super.clone(isGenerateId);
        Validation result = (Validation)((tempVar instanceof Validation) ? tempVar : null);

        // result.PrecedingIds = new java.util.ArrayList<String>(getPrecedingIds());
        // result.SucceedingIds = new java.util.ArrayList<String>(getSucceedingIds());

        return result;
    }

    public final void mergeWithDependentValidation(Validation dependentValidation) {
        if (getIsRef() || dependentValidation == null) {
            return;
        }
        mergeOperationBaseInfo(dependentValidation);
        mergeValidationInfo(dependentValidation);
    }

    private void mergeValidationInfo(Validation dependentValidation) {
        setValidationType(dependentValidation.getValidationType());
        setRequestNodeTriggerType(dependentValidation.getRequestNodeTriggerType());
        // setOrder(dependentValidation.getOrder());
        // setSucceedingIds(dependentValidation.getSucceedingIds());
        // setPrecedingIds(dependentValidation.getPrecedingIds());
    }

    // endregion

    /**
     * ��������
     */
    @Override
    public BEOperationType getOpType() {
        return BEOperationType.Validation;
    }
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.common;

/**
 * The Serializer Property Names Of Entity
 *
 * @ClassName: BizEntityJsonConst
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizEntityJsonConst {
    //业务实体
    public static final String Category = "Category";
    public static final String SourceEntity = "SourceEntity";
    public static final String DataLockType = "DataLockType";
    public static final String ExtendType = "ExtendType";
    public static final String DependentEntityId = "DependentEntityId";
    public static final String DependentEntityName = "DependentEntityName";
    public static final String DependentEntityPackageName = "DependentEntityPackageName";
    public static final String IsUsingTimeStamp = "IsUsingTimeStamp";
    public static final String BizMgrActions = "BizMgrActions";
    public static final String BizMgrAction = "BizMgrAction";
    public static final String ComponentAssemblyName = "ComponentAssemblyName";
    public static final String AssemblyName = "AssemblyName";
    public static final String CacheConfiguration = "CacheConfiguration";
    public static final String EnableCaching = "EnableCaching";
    public static final String EnableTreeDtm = "EnableTreeDtm";
    public static final String IsDefaultNull = "IsDefaultNull";

    //业务实体对象
    public static final String Determinations = "Determinations";
    public static final String Validations = "Validations";
    public static final String BizActions = "BizActions";
    public static final String BelongModel = "BelongModel";
    public static final String ParentObject = "ParentObject";
    public static final String ParentObjectID = "ParentObjectID";
    public static final String LogicDeleteControlInfo = "LogicDeleteControlInfo";

    //业务实体字段
    public static final String CalculationExpress = "CalculationExpress";
    public static final String ValidationExpress = "ValidationExpress";
    public static final String RtElementConfigId = "RtElementConfigId";
    public static final String Readonly = "Readonly";
    public static final String RequiredCheckOccasion = "RequiredCheckOccasion";
    public static final String UnifiedDataType = "UnifiedDataType";

    //操作
    public static final String Description = "Description";
    public static final String ComponentId = "ComponentId";
    public static final String ComponentPkgName = "ComponentPkgName";
    public static final String ComponentName = "ComponentName";
    public static final String IsVisible = "IsVisible";
    public static final String OpType = "OpType";
    public static final String Owner = "Owner";
    public static final String BelongModelId = "BelongModelID";
    public static final String IsRef = "IsRef";
    public static final String IsGenerateComponent = "IsGenerateComponent";

    //BizActionBase
    public static final String CurentAuthType = "CurentAuthType";
    public static final String Parameters = "Parameters";
    public static final String ReturnValue = "ReturnValue";
    public static final String OpIdList = "OpIdList";
    public static final String ParamCode = "ParamCode";
    public static final String ParamName = "ParamName";
    public static final String ParameterType = "ParameterType";
    public static final String CollectionParameterType = "CollectionParameterType";
    public static final String Assembly = "Assembly";
    public static final String ClassName = "ClassName";
    public static final String JavaClassName = "JavaClassName";
    public static final String Mode = "Mode";
    public static final String ParamDescription = "ParamDescription";

    //determination
    public static final String DeterminationType = "DeterminationType";
    public static final String TriggerTimePointType = "TriggerTimePointType";
    public static final String RequestNodeTriggerType = "RequestNodeTriggerType";
    public static final String RequestElements = "RequestElements";
    public static final String RequestChildElements = "RequestChildElements";
    public static final String RequestChildElementKey = "RequestChildElementKey";
    public static final String RequestChildElementValue = "RequestChildElementValue";
    public static final String PreDtmId = "PreDtmId";
    public static final String Determination = "Determination";

    //validation
    public static final String ValidationType = "ValidationType";
    public static final String Order = "Order";
    public static final String Precedings = "Precedings";
    public static final String Succeedings = "Succeedings";
    public static final String PrecedingIds = "PrecedingIds";
    public static final String SucceedingIds = "SucceedingIds";
    public static final String ValidationTriggerPoints = "ValidationTriggerPoints";
    public static final String ValidationTriggerPointKey = "ValidationTriggerPointKey";
    public static final String ValidationTriggerPointValue = "ValidationTriggerPointValue";
    public static final String ValidationAfterSave = "ValAfterSave";
    public static final String Validation = "Validation";
//RequestNodeTriggerType

    //Authority
    public static final String Authorizations = "Authorizations";
    public static final String AuthFieldInfos = "AuthFieldInfos";
    public static final String AuthFieldID = "AuthFieldID";
    public static final String AuthFieldName = "AuthFieldName";
    public static final String AuthorizationID = "AuthorizationID";
    public static final String AuthID = "AuthID";
    public static final String ElementID = "ElementID";

    public static final String ActionInfos = "ActionInfos";

    public static final String AuthActionID = "AuthActionID";
    public static final String AuthActionOpID = "AuthActionOpID";
    public static final String AuthActionOpName = "AuthActionOpName";

    public static final String FuncOperationID = "FuncOperationID";
    public static final String FuncOperationName = "FuncOperationName";

    //Increment
    public static final String AddedAction = "AddedAction";
    public static final String ModifyAction = "ModifyAction";

}
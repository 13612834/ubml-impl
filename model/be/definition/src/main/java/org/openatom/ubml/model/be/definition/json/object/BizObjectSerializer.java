/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.object;


import com.fasterxml.jackson.core.JsonGenerator;
import java.io.IOException;
import org.openatom.ubml.model.be.definition.GspBizEntityObject;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.json.element.BizElementSerializer;
import org.openatom.ubml.model.be.definition.json.operation.BizActionCollectionSeriallizer;
import org.openatom.ubml.model.be.definition.json.operation.DtmCollectionSerializer;
import org.openatom.ubml.model.be.definition.json.operation.ValCollectionSerializer;
import org.openatom.ubml.model.be.definition.operation.collection.BizActionCollection;
import org.openatom.ubml.model.be.definition.operation.collection.DeterminationCollection;
import org.openatom.ubml.model.be.definition.operation.collection.ValidationCollection;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonObject;
import org.openatom.ubml.model.common.definition.commonmodel.json.element.CmElementSerializer;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectSerializer;

/**
 * The  Josn Serializer Of Entity
 *
 * @ClassName: BizObjectSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizObjectSerializer extends CmObjectSerializer {

    private DtmCollectionSerializer dtmCollectionSerializer = new DtmCollectionSerializer();
    private ValCollectionSerializer valCollectionSerializer = new ValCollectionSerializer();
    private BizActionCollectionSeriallizer actionCollectionSeriallizer = new BizActionCollectionSeriallizer();

    //region BaseProp
    @Override
    protected void writeExtendObjectBaseProperty(JsonGenerator writer, IGspCommonObject commonObject) {
        GspBizEntityObject bizObject = (GspBizEntityObject)commonObject;
        writeDeterminations(writer, bizObject);
        writeValidations(writer, bizObject);
        WriteBizActions(writer, bizObject);
        if (bizObject.getLogicDeleteControlInfo() != null) {
            SerializerUtils.writePropertyName(writer, BizEntityJsonConst.LogicDeleteControlInfo);
            SerializerUtils.writePropertyValue_Object(writer, bizObject.getLogicDeleteControlInfo());
        }

//        WriteAuthFieldInfos(writer, bizObject);
    }

    private void writeDeterminations(JsonGenerator writer, GspBizEntityObject bizObject) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Determinations);
        DeterminationCollection dtms = bizObject.getDeterminations();
        try {
            dtmCollectionSerializer.serialize(dtms, writer, null);
        } catch (IOException e) {
            throw new RuntimeException(bizObject.getCode() + "的联动计算序列化失败", e);
        }
    }

    private void writeValidations(JsonGenerator writer, GspBizEntityObject bizObject) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Validations);
        ValidationCollection actionList = bizObject.getValidations();
        try {
            valCollectionSerializer.serialize(actionList, writer, null);
        } catch (IOException e) {
            throw new RuntimeException(bizObject.getCode() + "的校验规则序列化失败", e);
        }
    }

    private void WriteBizActions(JsonGenerator writer, GspBizEntityObject bizObject) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.BizActions);
        BizActionCollection actionList = bizObject.getBizActions();
        try {
            actionCollectionSeriallizer.serialize(actionList, writer, null);
        } catch (IOException e) {
            throw new RuntimeException(bizObject.getCode() + "的实体动作序列化失败", e);
        }
    }

//    private void WriteAuthFieldInfos(JsonGenerator writer, GspBizEntityObject bizObject)
//    {
//        AuthFieldConvertor convertor = new AuthFieldConvertor();
//        //权限字段集合：
//        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.AuthFieldInfos);
//        //[
//        SerializerUtils.WriteStartArray(writer);
//        if (bizObject.AuthFieldInfos.Count > 0)
//        {
//            bizObject.AuthFieldInfos.ForEach(
//                    item =>
//                    {
//                            convertor.WriteJson(writer, item, null);
//					});
//        }
//        //]
//        SerializerUtils.WriteEndArray(writer);
//    }

    //endregion


    @Override
    protected void writeExtendObjectSelfProperty(JsonGenerator jsonGenerator, IGspCommonObject iGspCommonObject) {

    }

    @Override
    protected CmElementSerializer gspCommonDataTypeSerializer() {
        return new BizElementSerializer();
    }


}




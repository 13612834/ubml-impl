/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.collection;

import org.openatom.ubml.model.be.definition.operation.BizMgrAction;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.BizOperationCollection;

/**
 * The Collection Of Biz Manager  Action
 *
 * @ClassName: BizMgrActionCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizMgrActionCollection extends BizOperationCollection {
    private static final long serialVersionUID = 1L;

    // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // ORIGINAL LINE: public new void add(BizOperation operation)
    public final boolean add(BizOperation operation) {
        if (operation instanceof BizMgrAction) {
            return super.add(operation);
        }
        throw new RuntimeException("错误的Action类型");
    }

    // /**
    // 克隆BEMgrAction动作集合

    // @param isGenerateId 是否生成Id
    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone(bool isGenerateId)
    // public final Object clone(boolean isGenerateId)
    // {
    // BizMgrActionCollection col = new BizMgrActionCollection();
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // col.addRange(this.select(item { return
    // (BizMgrAction)item.clone(isGenerateId)));
    // return col;
    // }

    // /**
    // * 克隆BEMgrAction动作集合
    // *
    // * @return 返回动作集合
    // */
    // // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s
    // shadowing
    // // via the 'new' keyword:
    // // ORIGINAL LINE: public new object clone()
    // public final Object clone() {
    // return clone(false);
    // }
    @Override
    public BizMgrActionCollection clone() {
        return (BizMgrActionCollection)super.clone();
    }

    @Override
    protected BizMgrAction convertOperation(BizOperation op) {
        return (BizMgrAction)super.convertOperation(op);
    }

    @Override
    protected BizMgrActionCollection createOperationCollection() {
        return new BizMgrActionCollection();
    }
}
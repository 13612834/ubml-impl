/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.common.InternalActionUtil;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.json.CommonModelNames;

/**
 * The  Josn Serializer Of Biz Operation
 *
 * @ClassName: BizOperationSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizOperationSerializer<T extends BizOperation> extends JsonSerializer<T> {
    private InternalActionUtil util = new InternalActionUtil();

    @Override
    public void serialize(BizOperation value, JsonGenerator gen, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(gen);
        writeBaseProperty(value, gen);
        writeSelfProperty(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    //region BaseProp
    private void writeBaseProperty(BizOperation op, JsonGenerator writer) {

        SerializerUtils.writePropertyValue(writer, CommonModelNames.ID, op.getID());

        // 若为内置操作，则只序列化基本信息
        if (isInternalAction(op.getID()))
            return;
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsRef, op.getIsRef());

        writeExtendOperationBaseProperty(writer, op);
    }

    private boolean isInternalAction(String opID) {
        if (util.InternalMgrActionIDs.contains(opID) || util.InternalBeActionIDs.contains(opID)) {
            return true;
        }
        return false;
    }
    //endregion

    //region SelfProp
    private void writeSelfProperty(BizOperation op, JsonGenerator writer) {
        SerializerUtils.writePropertyValue(writer, CommonModelNames.CODE, op.getCode());
        SerializerUtils.writePropertyValue(writer, CommonModelNames.NAME, op.getName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Description, op.getDescription());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ComponentId, op.getComponentId());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ComponentName, op.getComponentName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ComponentPkgName, op.getComponentPkgName());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsVisible, op.getIsVisible());
        //SerializerUtils.writePropertyValue(writer, BizEntityConst.OpType, op.OpType);//子类赋值
        //Owner不序列化
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.BelongModelId, op.getBelongModelID());
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsGenerateComponent, op.getIsGenerateComponent());
        SerializerUtils.writePropertyValue(writer, CefNames.CUSTOMIZATION_INFO, op.getCustomizationInfo());
        // 若为内置操作，则只序列化基本信息
        if (isInternalAction(op.getID())) {
            return;
        }
        //扩展模型属性
        writeExtendOperationSelfProperty(writer, op);
    }

    //endregion

    //region 抽象方法
    protected abstract void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op);

    protected abstract void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op);
    //endregion
}

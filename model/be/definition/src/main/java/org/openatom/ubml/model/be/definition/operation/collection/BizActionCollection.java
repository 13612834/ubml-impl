/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.collection;

import org.openatom.ubml.model.be.definition.operation.BizAction;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.BizOperationCollection;

/**
 * The Collection Of Biz Action
 *
 * @ClassName: BizActionCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizActionCollection extends BizOperationCollection {
    private static final long serialVersionUID = 1L;

    public final boolean add(BizOperation operation) {
        if (operation instanceof BizAction) {
            operation.setOwner(getOwner());
            return super.add(operation);
        }
        throw new RuntimeException("错误的Action类型");
    }

    // /**
    // 克隆BEAction动作集合

    // @param isGenerateId 是否生成Id
    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone(bool isGenerateId)
    // public final BizActionCollection clone(boolean isGenerateId)
    // {
    // BizActionCollection col = new BizActionCollection();
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // col.addRange(this.select(item { return (BizAction)item.clone(isGenerateId)));
    // return col;
    // }

    // /**
    // * 克隆BEAction动作集合
    // *
    // * @return 返回动作集合
    // */
    // // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s
    // shadowing
    // // via the 'new' keyword:
    // // ORIGINAL LINE: public new object clone()
    // public final Object clone() {
    // return clone(false);
    // }
    @Override
    public BizActionCollection clone() {
        return (BizActionCollection)super.clone();
    }

    @Override
    protected BizAction convertOperation(BizOperation op) {
        return (BizAction)super.convertOperation(op);
    }

    @Override
    protected BizActionCollection createOperationCollection() {
        return new BizActionCollection();
    }
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.BizActionBase;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizParameter;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;


/**
 * The  Josn Serializer Of Entity Action
 *
 * @ClassName: BizActionBaseSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizActionBaseSerializer extends BizOperationSerializer<BizActionBase> {

    @Override
    protected void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op) {
        BizActionBase action = (BizActionBase)op;
        writeExtendActionBaseProperty(writer, action);
    }

    @Override
    protected void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op) {
        BizActionBase action = (BizActionBase)op;
//        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CurentAuthType, action.getCurentAuthType());
        writeOpIdList(writer, action);
        writeParameters(writer, action);
        WriteReturnValue(writer, action);
        WriteExtendActionSelfProperty(writer, action);
    }

    private void writeOpIdList(JsonGenerator writer, BizActionBase action) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.OpIdList);
        //[
        SerializerUtils.WriteStartArray(writer);
        if (action.getOpIdList().size() > 0) {
            for (String item : action.getOpIdList()) {
                SerializerUtils.writePropertyValue_String(writer, item);
            }
        }
        //]
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeParameters(JsonGenerator writer, BizActionBase action) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Parameters);
        //[
        SerializerUtils.WriteStartArray(writer);
        if (action.getParameters().getCount() > 0) {
            for (Object item : action.getParameters()) {
                getParaConvertor().serialize((BizParameter)item, writer, null);
            }
        }
        //]
        SerializerUtils.WriteEndArray(writer);
    }

    private void WriteReturnValue(JsonGenerator writer, BizActionBase action) {
        if (action != null && action.getReturnValue() != null) {
            SerializerUtils.writePropertyName(writer, BizEntityJsonConst.ReturnValue);
            getParaConvertor().serialize(action.getReturnValue(), writer, null);
        }
    }


    protected abstract BizParameterSerializer getParaConvertor();

    protected abstract void writeExtendActionBaseProperty(JsonGenerator writer, BizActionBase action);

    protected abstract void WriteExtendActionSelfProperty(JsonGenerator writer, BizActionBase action);
}

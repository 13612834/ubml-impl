/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation;

import java.util.ArrayList;
import java.util.function.Predicate;
import org.openatom.ubml.model.be.definition.GspBizEntityObject;
import org.openatom.ubml.model.be.definition.common.InternalActionUtil;
import org.openatom.ubml.model.common.definition.cef.collection.BaseList;

/**
 * The Collection Of Biz Operation Definition
 *
 * @ClassName: BizOperationCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizOperationCollection extends BaseList<BizOperation> implements Cloneable {

    private static final long serialVersionUID = 1L;
    private GspBizEntityObject privateOwner;

    public BizOperationCollection() {
    }

    // endregion
    public boolean add(BizOperation op) {
        return super.add(op);
    }

    public final GspBizEntityObject getOwner() {
        return privateOwner;
    }

    public final void setOwner(GspBizEntityObject value) {
        privateOwner = value;
    }

    public final void setOwner(BizOperation item) {
        if (item == null) {
            for (BizOperation opt : this) {
                opt.setOwner(getOwner());
            }
        } else {
            item.setOwner(getOwner());
        }
    }

    public final boolean equals(BizOperationCollection other) {
        if (other == null || getCount() != other.size()) {
            return false;
        }
        for (int index = 0; index < getCount(); index++) {
            if (get(index).equals(other.get(index))) {
                return false;
            }
        }
        return true;
    }

    public BizOperationCollection clone(boolean isGenerateId) {
        BizOperationCollection col = createOperationCollection();
        for (BizOperation op : this) {
            col.add(convertOperation(op.clone(isGenerateId)));
        }
        return col;
    }

    public BizOperationCollection clone() {
        return clone(false);
    }

    protected BizOperationCollection createOperationCollection() {
        return new BizOperationCollection();
    }

    protected BizOperation convertOperation(BizOperation op) {
        return op;
    }

    protected final String getKeyForItem(BizOperation item) {
        return item.getCode();
    }

    public final BizOperation getItem(String id) {
        for (BizOperation op : this) {
            if (op.getID().equals(id)) {
                return op;
            }
        }
        return null;
    }

    public final void mergeDbeOperations(BizOperationCollection dependentOps) {
        BizOperationCollection ops = this.clone();
        if (ops != null && ops.size() > 0) {
            clear();
            for (BizOperation op : ops) {
                if (op.getIsRef()) {
                    BizOperation dbeOp = null;
                    for (BizOperation dbeOp1 : dependentOps) {
                        if (dbeOp1.getID().equals(op.getID())) {
                            dbeOp = dbeOp1;
                        }
                    }
                    if (dbeOp != null) {
                        BizOperation refDtm = dbeOp.clone();
                        refDtm.setID(op.getID());
                        refDtm.setIsRef(true);
                        add(refDtm);
                        dependentOps.remove(dbeOp);
                    }
                } else {
                    add(op);
                }
            }
        }

        if (dependentOps == null || dependentOps.size() < 1) {
            return;
        }
        for (BizOperation op : dependentOps) {
            BizOperation dbeOp = op.clone();
            // �ų����õ�beAction/mgrAction
            InternalActionUtil util = new InternalActionUtil();
            if (util.InternalBeActionIDs.contains(dbeOp.getID()) || util.InternalMgrActionIDs.contains(dbeOp.getID())) {
                continue;
            }

            add(dbeOp);
        }
    }

    public final ArrayList<BizOperation> GetFiltedOps(Predicate<BizOperation> predicate) {

        ArrayList<BizOperation> result = new ArrayList<BizOperation>();
        for (BizOperation op : this) {
            if (predicate.test(op)) {
                result.add(op);
            }
        }
        return result;
    }

    public boolean removeById(String actionId) {

        if (actionId == null || "".equals(actionId)) {
            return false;
        }
        super.removeIf(item ->
                actionId.equals(item.getID())
        );
        return true;
    }
}
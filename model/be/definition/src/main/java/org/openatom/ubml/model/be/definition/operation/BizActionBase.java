/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation;

import org.openatom.ubml.model.be.definition.operation.componentbase.BizReturnValue;
import org.openatom.ubml.model.be.definition.operation.componentinterface.IBizParameterCollection;

/**
 * The Base Definition Of Biz Action
 *
 * @ClassName: BizActionBase
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizActionBase extends BizOperation implements Cloneable {

    // region 属性
    private java.util.ArrayList<String> opIdList = new java.util.ArrayList<String>();
    // private AuthType authType = AuthType.TransAuth;

    // /**
    // 权限类型

    // */
    // public AuthType getCurentAuthType(){}
    // void setCurentAuthType(AuthType value)

    /**
     * 参数列表
     */
    private IBizParameterCollection parameterCollection;
    /**
     * 返回值
     */
    private BizReturnValue privateReturnValue;

    public final IBizParameterCollection getParameters() {
        if (parameterCollection == null) {
            parameterCollection = this.getActionParameters();
        }
        return parameterCollection;
    }

    public final void setParameters(IBizParameterCollection value) {
        parameterCollection = value;
    }

    public final BizReturnValue getReturnValue() {
        return privateReturnValue;
    }

    public final void setReturnValue(BizReturnValue value) {
        privateReturnValue = value;
    }

    // endregion

    // region 方法

    public java.util.ArrayList<String> getOpIdList() {
        return (opIdList != null) ? opIdList : (opIdList = new java.util.ArrayList<String>());
    }

    /**
     * 业务操作ID列表（用于控制动作权限）
     */
    public void setOpIdList(java.util.ArrayList<String> value) {
        opIdList = value;
    }

    protected abstract IBizParameterCollection getActionParameters();

    public final void mergeWithDependentAction(BizActionBase dependentAction) {
        if (getIsRef() || dependentAction == null) {
            return;
        }
        mergeOperationBaseInfo(dependentAction);

        // setCurentAuthType(dependentAction.getCurentAuthType());
        getOpIdList().addAll(dependentAction.getOpIdList());
        for (int i = 0; i < dependentAction.getActionParameters().getCount(); i++) {
            getActionParameters().add(dependentAction.getActionParameters().getItem(i));
        }
    }

    // endregion
}
/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.internalmgraction;

import org.openatom.ubml.model.be.definition.operation.BizMgrAction;

/**
 * The Internal Retrieve Default Action
 *
 * @ClassName: RetrieveDefaultMgrAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RetrieveDefaultMgrAction extends BizMgrAction implements IInternalMgrAction {
    public static final String id = "Tji_dbrLtEKQlOLRpOJj9Q";
    public static final String code = "RetrieveDefault";
    public static final String name = "内置新增操作";

    public RetrieveDefaultMgrAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}
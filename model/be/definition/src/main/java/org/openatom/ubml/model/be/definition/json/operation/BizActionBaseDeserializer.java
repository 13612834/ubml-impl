/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.be.definition.beenum.AuthType;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.BizActionBase;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizParameter;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizParameterCollection;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizReturnValue;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

/**
 * The  Josn Deserializer Of Entity Action
 *
 * @ClassName: BizActionBaseDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizActionBaseDeserializer extends BizOperationDeserializer<BizActionBase> {

    @Override
    protected final BizActionBase createBizOp() {
        return createBizActionBase();
    }

    @Override
    protected final boolean readExtendOpProperty(BizOperation op, String propName, JsonParser jsonParser) {
        BizActionBase action = (BizActionBase)op;
        boolean hasProperty = true;
        switch (propName) {
            case BizEntityJsonConst.CurentAuthType:
                SerializerUtils.readPropertyValue_Enum(jsonParser, AuthType.class, AuthType.values());
                break;
            case BizEntityJsonConst.OpIdList:
                action.setOpIdList(SerializerUtils.readStringArray(jsonParser));
                break;
            case BizEntityJsonConst.Parameters:
                readParameters(jsonParser, action);
                break;
            case BizEntityJsonConst.ReturnValue:
                readReturnValue(jsonParser, action);
                break;
            default:
                if (!readExtendActionProperty(jsonParser, op, propName)) {
                    hasProperty = false;
                }
                break;
        }
        return hasProperty;
    }

    private void readParameters(JsonParser jsonParser, BizActionBase action) {
        BizParaDeserializer paraDeserializer = createPrapConvertor();
        BizParameterCollection<BizParameter> collection = createPrapCollection();
        SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
        action.setParameters(collection);
    }

    private void readReturnValue(JsonParser jsonParser, BizActionBase action) {
        BizReturnValueDeserializer returnValueDeserializer = new BizReturnValueDeserializer();
        BizReturnValue value = (BizReturnValue)returnValueDeserializer.deserialize(jsonParser, null);
        action.setReturnValue(value);
    }

    protected abstract BizActionBase createBizActionBase();

    protected abstract BizParaDeserializer createPrapConvertor();

    protected abstract BizParameterCollection createPrapCollection();

    protected boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op, String propName) {
        return false;
    }
}
